from pyrocko.guts import Object, Float, Int, String, Bool, List, Tuple, Dict


class clusty_config(Object):
    settings = List.T()


class GeneralSettings(Object):
    n_workers = Int.T(default=1)
    work_dir = String.T(default='.')
    catalog_file = String.T()
    station_file = String.T(optional=True)
    station_file_list = List.T(optional=True)
    st_allowlist = List.T(default=[], optional = True)
    st_blocklist = List.T(default=[], optional = True)
    waveform_dir = String.T(optional=True)
    station_subset = Dict.T(optional=True,
        help='Subset of stations, currently only maximum distance'
             + 'between mid of event and stations implemented'
             + '(keyword = ''maxdist_stations'') in [m]')
    plot_format = String.T(default='pdf', help='Choose between pdf and png.')


class cc_comp_settings(Object):
    bp = Tuple.T(3,)
    filtertmin = Float.T(default=120, help='starttime before arrival for filtering')
    filtertmax = Float.T(default=600, help='endtime after arrival for filtering')
    downsample_to = Float.T(optional=True)
    twd = List.T(default=None)
    anticor_mode = String.T(optional=True)
    pick_path = String.T(default='')
    pick_option = String.T(default='pyr_file') # pyr_single, xml, pyr_path
    pick_file_template = String.T(optional=True)
    phase = List.T(default=[])
    components = List.T(default=[])
    use_precalc_arrivals = Bool.T(default=False)
    compute_arrivals = Bool.T(default=True)
    coordinate_system = String.T(default='spherical')
    vmodel = String.T(default='', 
                      help='if not set, crust2x2 model for'
                            +'first event location is used.')
    homogen_model = List.T(default=[])
    use_precalc_ccs = Bool.T(default=False)
    cc_plots = Bool.T(default=False)
    # plot_cc_dist = Bool.T(default=False)
    snr_calc = Bool.T(default=False)
    snr_thresh = Float.T(optional=True)
    max_dist = Float.T(default=40000,
                       help='maximum inter event distance [m]')
    dist_hypocentral = Bool.T(default=False)
    debug_mode = Bool.T(default=False)
    debug_mode_S = Bool.T(default=False)
    plot_npairs_stats = Bool.T(default=False)

class network_similarity_settings(Object):
    get_station_weights = Bool.T(default=False)
    method_similarity_computation = String.T()
    get_bool_sta_use = Bool.T(default=False)
    use_precalc_net_sim = Bool.T(default=False)
    trimm_cut = Float.T(optional=True)
    apply_cc_station_thresh = Bool.T(default=False)
    cc_thresh = Float.T(default=0)
    min_n_stats = Int.T(optional=True)
    az_thresh = Float.T(optional=True)
    combine_components = Bool.T(optional=True)
    weights_components = List.T(default=None)


class clustering_settings(Object):
    method = String.T()
    dbscan_eps = List.T(optional=True, default=[])
    dbscan_min_pts = List.T(optional=True, default=[])
    optics_xi = List.T(optional=True, default=[])
    optics_min_pts = List.T(optional=True, default=[])
    map_faults_path = List.T(default=[])
    plot_hist = Bool.T(default=False)
    plot_netsim_matrix = Bool.T(default=False)
    repr_ev = Bool.T(default=False)
    plot_map = Bool.T(default=True)
    focus_map_plot = Float.T(default=1.0, 
        help='radius relative to largest distance between events, exclude outliers by setting to value below 1.0')
    map_plotlib = String.T(default='gmt', help='\'gmt\' option needs gmt5, chose \'matplotlib\' or \'cartopy\' for cartopy-based maps')
    wf_plot = Tuple.T(optional=[[]])
    wf_plot_stats = List.T(optional=True)
    wf_cl_snuffle = Tuple.T(optional=[[]])
    wf_cluster_choice = List.T(optional=True)
    frucht_k_factor = Float.T(default=2.5)
    frucht_plot_lib = String.T(default='plotly')
    frucht_show = Bool.T(default=False)
    sankey_show = Bool.T(default=False)
    sankey_labels = Bool.T(default=True)
    export_stacked_traces = Bool.T(default=False)
    preprocess_export_stacked_traces = Bool.T(optional=True, default=True)
    prepare_for_grond = Bool.T(default=False)
    debug_stacking = Bool.T(default=False)
    mag_scale_log = Tuple.T(optional=True)
    t_buffer_wfplot = Float.T(default=20.)

class merge_cat_settings(Object):
    cat_config_list = List.T(default=[])
    pref_eps = List.T(default=[])
    pref_minpts = List.T(default=[])
    export_stacked_traces = Bool.T(default=False)
    frucht_show = Bool.T(default=False)
    sankey_show = Bool.T(default=False)

