import sys, logging, os
import numpy as num

logger = logging.getLogger('__name__')

def check_cc_cnf(cc_cnf, error):
    if len(cc_cnf.bp) != 3: 
        logger.error('ERROR: Bandpass filter takes three entries: order, corner_hp [Hz], corner_lp [Hz]')
        error = True
    elif cc_cnf.bp[1] > cc_cnf.bp[2]:
        logger.error('ERROR: Bandpass filter takes three entries: order, corner_hp [Hz], corner_lp [Hz]. hp must be smaller than lp.')
        error = True
    else:
        logger.debug('- Check passed: BP Filter, fmin: %s, fmax: %s, order: %s.'
                     % (cc_cnf.bp[1], cc_cnf.bp[2], cc_cnf.bp[0]))

    if cc_cnf.twd:
        if len(cc_cnf.twd) == 2:
            if cc_cnf.twd[0][0] > cc_cnf.twd[0][1] or cc_cnf.twd[1][0] > cc_cnf.twd[1][1]:
                logger.error('ERROR: twd contains start and end relative to phase arrival. start must be before end.')
                error = True

        elif len(cc_cnf.twd) == 1:
            if cc_cnf.twd[0][0] > cc_cnf.twd[0][1]:
                logger.error('ERROR: twd contains start and end relative to phase arrival. start must be before end.')
                error = True

        if len(cc_cnf.twd) != len(cc_cnf.phase):
            logger.error('ERROR: Number of entries in twd (here: %s) must be same as number of phases (here: %s).'
                         % (len(cc_cnf.twd), len(cc_cnf.phase)))
            error = True

        logger.debug('- Check passed: Time windows')

    if cc_cnf.pick_path != '':
        if os.path.exists(cc_cnf.pick_path):
            logger.debug('- Check passed: Pick path exists.')
        else:
            logger.error('ERROR: Pick path does not exist: %s'
                         % cc_cnf.pick_path)
            error = True

        if cc_cnf.pick_option not in ['pyr_path', 'pyr_file', 'xml']:
            logger.error('ERROR: Please use one of the following pick_options:'\
                         + ' *pyr_path* for a path to a directory with pyrocko pick files,'
                         + ' *pyr_file* for a single pyrocko pick file,'
                         + ' or *xml* for event xml file(s) with picks.')
            error = True
        else:
            logger.debug('- Check passed: Pick option exists.')

    if len(cc_cnf.phase) not in [1,2]:
        logger.error('ERROR: Clusty can only work with one or two phases simultaneously.')
        error = True
    else:
        for p in cc_cnf.phase:
            if p not in ['p','P','s','S','R','r','L','l']:
                logger.error('ERROR: Unknown phase name %s, please use P, S, R or L.' % p)
                error = True
        logger.debug('- Check passed: Phases names okay.')        

    if len(cc_cnf.components) not in [1,2,3]:
        logger.error('ERROR: Clusty can only work with one, two or three components simultaneously.'
                     + 'You can use x as a wild card, e.g. xHZ or xxZ.')
        error = True
    else:
        for c in cc_cnf.components:
            if len(c) != 3:
                logger.error('ERROR: Components must be strings with three characters, E.g. HHZ.'
                             + ' x can be used as a wild card, for example xHZ or xxZ.')

    error_phasecomp = False
    error_compname = False
    for comp in cc_cnf.components:
        if comp.endswith('Z'):
            if 'P' not in cc_cnf.phase and 'R' not in cc_cnf.phase:
                error_phasecomp = True
        if comp.endswith('N') or comp.endswith('E')\
            or comp.endswith('1') or comp.endswith('2') or comp.endswith('3'):
            if 'S' not in cc_cnf.phase and 'L' not in cc_cnf.phase:
                error_phasecomp =True
        if not comp[-1] in ['Z', 'N', 'E', '1', '2', '3']:
            error_compname = True
    
    if error_phasecomp:
        logger.error('ERROR: Currently, clusty only works with P or R phases on Z components and S or L phases on N and E components. Other possibilities will be added soon.')
        error = True

    if error_compname:
        logger.error('ERROR: Currently, clusty only supports component names ending with Z, N or E. Support for other phases will be added soon.')
        error = True

    if cc_cnf.homogen_model:
        if not len(cc_cnf.homogen_model) == len(cc_cnf.phase):
            logger.error('ERROR: Please provide a seismic velocity in homogen_model for each phase.\n')
            error = True

    if not isinstance(cc_cnf.snr_thresh, (float,int)):
        logger.error('ERROR: SNR threshold should be a number.')
        error = True

    if not isinstance(cc_cnf.snr_thresh, (float, int)):
        if cc_cnf.snr_thresh == False:
            logger.debug('- Check passed: SNR threshold not used.')
        else:
            logger.error('ERROR: SNR threshold should be a positive number or False.')
            error = True
    elif cc_cnf.snr_thresh < 0.0:
        logger.error('ERROR: SNR threshold should be a positive number or False.')
        error = True
    else:
        logger.debug('- Check passed: SNR threshold is %s.' % cc_cnf.snr_thresh)

    if not isinstance(cc_cnf.max_dist, (float, int)):
        logger.error('ERROR: Maximum inter-event distance [m] (max_dist) must be a number.')
        error = True

    elif cc_cnf.max_dist < 0.0:
        logger.error('ERROR: Maximum inter-event  distance [m] (max_dist) must be a positive number.')
        error = True

    else:
        logger.debug('- Check passed: Maximum inter-event distance [m] (max_dist) is %s.' % cc_cnf.max_dist)

    return error

def check_netsim_cnf(netsim_cnf, cc_cnf, error):
    if netsim_cnf.get_station_weights:
        logger.debug('- Info: Station weigthing called.')
    else:
        logger.debug('- Info: Station weigthing not called.')

    if netsim_cnf.method_similarity_computation not in ['weighted_sum_c_diff', 'median_ccs', 
    'trimmed_mean_ccs', 'product','max_cc', 'mean_ccs']:

        logger.error('ERROR: Please choose valid Method to compute network similarity: \n'
        + 'weighted_sum_c_diff | median_ccs | trimmed_mean_ccs | product | max_cc |  mean_ccs ')
        error = True

    else:
        logger.debug('- Info: Using method %s for network similarity computation.' 
                     % netsim_cnf.method_similarity_computation
                     )

    if len(netsim_cnf.weights_components) != len(cc_cnf.components):
        logger.error('ERROR: Number of weights for components (weights_components) and number of components not equal.')
        error = True
    else:
        if sum(netsim_cnf.weights_components) != 1.0:
            logger.error('ERROR: Sum of component weights (weights_components) must equal 1.0')
            error = True
        
        logger.debug( '- Check passed: Number of weights.')

    if netsim_cnf.apply_cc_station_thresh:
        if netsim_cnf.cc_thresh >= 1.0 or netsim_cnf.cc_thresh <= 0.0:
            logger.error('ERROR: cc_thresh is the cross-correlation threshold and must be between larger than 0.0 and smaller than 1.0.')
            error = True
    return error


def check_cl_cnf(cl_cnf, error):

    if cl_cnf.method in ['DBSCAN', 'dbscan']:
        #try:
        min_pts = cl_cnf.dbscan_min_pts
        param1 = cl_cnf.dbscan_eps
        param_name = 'cl_cnf.dbscan_eps'
        # except AttributeError:
        #     logger.error('ERROR: Set dbscan_min_pts and dbscan_eps parameters for DBSCAN clustering')
        #     error = True
    elif cl_cnf.method in ['OPTICS', 'optics']:
        #try:
        min_pts = cl_cnf.optics_min_pts
        param1 = cl_cnf.optics_xi
        param_name = 'cl_cnf.optics_xi'
        # except AttributeError:
        #     logger.error('ERROR: Set optics_min_pts and optics_xi parameters for OPTICS clustering')
        #     error = True
    else:
        logger.error('ERROR: Currently only clustering algorithm (*method*) dbscan and optics implemented.')
        error = True

    if len(param1) > 0:
        for param in param1:
            if not isinstance(param, float):
                logger.error('ERROR: %s takes a list of floats as input. Please check %s.' % (param_name,param_name))
                error = True
            if param > 1 or param < 0:
                logger.error('ERROR: %s values must be between 0 and 1. Please check %s.' % (param_name,param_name))
                error = True
    else:
        logger.error('ERROR: List in %s not provided or empty. Please provide a list of clustering parameters' % param_name)
        error = True

    if len(min_pts) > 0:
        for pts in min_pts:
            if not isinstance(pts, int):
                logger.error('ERROR: min_pts takes a list of integers as input.')
                error = True
            if int(pts) < 3:
                logger.warning('ERROR: min_pts should be set to larger or equal to 3.')
                error = True
    else:
        logger.error('ERROR: List of min_pts not provided or empty. Please provide a list of clustering parameters')
        error = True

    if cl_cnf.repr_ev == []:
        logger.error('Representative events are now computed for all eps-mitpts combinations. \
             Please set repr_ev to True or False in the config file.')
        error = True
    
    if not cl_cnf.repr_ev:
        logger.warning('WARNING: Representative events are needed for exporting aligned and stacked traces per cluster. \
            With the current setting, they are not computed. Please set repr_ev to True if you want to export traces.')

    if cl_cnf.wf_plot:
        if len(num.shape(cl_cnf.wf_plot)) == 1:
            logger.error('ERROR: cl_cnf.wf_plot takes a tuple with [eps,min_pts] as input.'
                                 + ' eps is a float and minpts an integer.')
            logger.error('  (A) Check the wf_plot input. Format: [(eps1,min_pts1),(eps2,min_pts2)].')
            logger.error('  (B) Remove wf_plot from the config for default empty sequence')
            error = True
        else:
            for ll in cl_cnf.wf_plot:
                if ll != ():
                    if len(ll) != 2 or not isinstance(ll[0],float)\
                    or not isinstance(ll[1],int):
                        logger.error('ERROR: cl_cnf.wf_plot takes a list of tuples with (eps,min_pts) as input.'
                                     + ' eps is a float and minpts an integer.')
                        error = True

            #for ll in cl_cnf.wf_plot:
            #    if ll[0] not in param1 or ll[1] not in min_pts:
            #        logger.error('ERROR: cl_cnf.wf_plot tuple %s not found in cl_cnf.optics_xi, cl_cnf.dbscan_eps or min_pts.' %ll)
            #        error = True

    if cl_cnf.wf_cl_snuffle:
        if len(num.shape(cl_cnf.wf_cl_snuffle)) == 1:
            logger.error('ERROR: cl_cnf.wf_cl_snuffle takes a tuple with [eps,min_pts] as input.'
                                 + ' eps is a float and minpts an integer.')
            logger.error('  (A) Check the wf_cl_snuffle input. Format: [(eps1,min_pts1),(eps2,min_pts2)].')
            logger.error('  (B) Remove wf_cl_snuffle from the config for default empty sequence')
            error = True
        else:
            for ll in cl_cnf.wf_cl_snuffle:
                if ll != ():
                    if len(ll) != 2 or not isinstance(ll[0],float)\
                    or not isinstance(ll[1],int):
                        logger.error('ERROR: cl_cnf.wf_cl_snuffle takes a list of tuples with (eps,min_pts) as input.'
                                     + ' eps is a float and minpts an integer.')
                        logger.error('  (A) Check the wf_cl_snuffle input. Format: [(eps1,min_pts1),(eps2,min_pts2)].')
                        logger.error('  (B) Remove wf_cl_snuffle from the config for default empty sequence')
                        error = True
            #for ll in cl_cnf.wf_cl_snuffle:
            #    if ll[0] not in param1 or ll[1] not in min_pts:
            #        logger.error('ERROR: cl_cnf.wf_cl_snuffle tuple settings %s not found in cl_cnf.optics_xi, cl_cnf.dbscan_eps or min_pts.' %ll)
            #        error = True

    if cl_cnf.mag_scale_log:
        if len(cl_cnf.mag_scale_log) != 2:
            logger.error('ERROR: config.clustering_settings.mag_scale_log takes a tuple as input.'
                         + 'Format: [a,b], where markersize=a**magnitude *b. Comment out to use automated scaling.')
            error = True

    return error


def check(cc_cnf, netsim_cnf, cl_cnf):
    error = False

    error_cc = check_cc_cnf(cc_cnf, error)
    error_netsim = check_netsim_cnf(netsim_cnf, cc_cnf, error)
    error_cl = check_cl_cnf(cl_cnf, error)

    if error_cc or error_netsim or error_cl:
        logger.info('\n')
        logger.error('--> --> One more more errors in config file found. Please check error messages above.')
        sys.exit()

