import matplotlib as mpl
from matplotlib.colors import LinearSegmentedColormap
import random, logging

logger = logging.getLogger('__name__')

def hex_to_rgb(hex_color, out='std', alpha = 0.1):
    hex_color = hex_color.strip("#")

    r = int(hex_color[0:2], 16)
    g = int(hex_color[2:4], 16)
    b = int(hex_color[4:6], 16)
    
    if out == 'std':
        return r,g,b

    if out == 'plotly_rgb':
        rgb_string = 'rgb(%i,%i,%i,%.2f)' % (r,g,b,alpha)
        return rgb_string

def get_comp_name(cc_settings):
    comps = cc_settings.components
    phases = cc_settings.phase
    for i_comp, comp in enumerate(cc_settings.components):
        logger.info('Processing component %s' % comp)
        c = comp[-1]

        if c == 'Z':
            if 'P' in phases:
                i_ph = num.argwhere(phases=='P')[0,0]
            elif 'R' in phases:
                i_ph = num.argwhere(phases=='R')[0,0]
            else:
                # do not want to use Z trace for S and L phases.
                continue

        elif c in ['N', 'E', '1', '2', '3']:

            if 'S' in phases:
                i_ph = num.argwhere(phases=='S')[0,0]
            elif 'L' in phases:
                i_ph = num.argwhere(phases=='L')[0,0]
            elif 'R' in phases:
                i_ph = num.argwhere(phases=='R')[0,0]
                # L and R have same arrival time, therefore it does not matter which 
                # i_ph is used with N and E component, i_ph is only used to get correct
                # arrival time


            else:
                # do not want to use horizontal traces for P phases.
                continue 

def get_colormap(cmap_name='default'):

    if cmap_name == 'default':
        # plotly's Light24 + Dark24
        # (mint green removed due to lack of visibilty on map, #436B6B replaced a near black color)
        # #B68E100 subsitituted by #be6400, #FB00D1 by #755b71
        # exchanged 2 and 4
        mycolors = ['#000000', '#00A08B', '#FD3216', '#6A76FC', '#FF9616',
                    '#620042', '#328a3e', '#FE00CE', '#0DF9FF', '#F6F926', 
                    '#72f283', '#EEA6FB', '#DC587D', '#D626FF', '#6E899C', 
                    '#00B5F7', '#B68E00', '#FF0092', '#22FFA7', '#E3EE9E',
                    '#86CE00', '#BC7196', '#862A16', '#7E7DCD', '#E48F72',
                    '#DB7093', '#2E91E5', '#E15F99', '#1CA71C', '#FB0D0D', 
                    '#DA16FF', '#436B6B', '#Be6400', '#750D86', '#EB663B', 
                    '#511CFB', '#00A08B', '#755b71', '#FC0080', '#B2828D', 
                    '#6C7C32', '#778AAE', '#FED4C4', '#A777F1', '#FC6955', 
                    '#1616A7', '#DA60CA', '#6C4516', '#0D2A63', '#AF0038']

    elif cmap_name == 'seiscloud_comp':
        mycolors = ['black', 'red', 'blue', 'green', 'darkviolet', 'gold',
                    'darkorange', 'dodgerblue', 'brown', 'lightseagreen', 
                    'plum','lawngreen', 'palevioletred', 'royalblue', 
                    'limegreen','indigo']

    elif cmap_name == 'bluecolors':
        mycolors = ['slategray', 'lightsteelblue', 'cornflowerblue', 
                    'royalblue', 'black', 'blueviolet', 'plum', 'magenta']


    return mycolors

def net_corr_cmap_rdgy():
    '''red whhite black colormaps with broadened white area around zero'''
    cdict1 = {'red':   [(0.0,  1.0, 1.0),
                        (0.3, 1.0, 1.0),
                        (0.7, 1.0, 1.0),
                        (1.0,  0.0, 0.0)],
    
             'green': [(0.0,  0.0, 0.0),
                       (0.3, 1.0, 1.0),
                       (0.7, 1.0, 1.0),
                       (1.0,  0.0, 0.0)],
    
             'blue':  [(0.0,  0.0, 0.0),
                      (0.3,  1.0, 1.0),
                       (0.7,  1.0, 1.0),
                      (1.0,  0.0, 0.0)]}

    col = LinearSegmentedColormap('RedGrey1', cdict1)

    return col

def cluster_to_color(cluster_id, cmap_name = 'default'):
    '''
    Given a cluster id provide the standard color
    ...from simones code...
    '''
    mycolors = get_colormap(cmap_name)

    #my_def_color = 'gainsboro'
    my_def_color = '#606060'
    if cluster_id > (len(mycolors)-2):
        color = my_def_color
    else:
        color = mycolors[cluster_id+1]
    return color


def color2rgb(col):
    '''
    Return a red/green/blue string for GMT for a given color name
    '''
    colarray = mpl.colors.colorConverter.to_rgb(col) #colors.to_rgb(col)
    r, g, b = int(255*colarray[0]), int(255*colarray[1]), int(255*colarray[2])
    rgb = str(r)+'/'+str(g)+'/'+str(b)
    return rgb

