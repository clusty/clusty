import sys
import os
import copy
import glob, time
import numpy as num
import argparse
import pprint
import logging
import itertools
from string import Template
from pyrocko import util, model, orthodrome, guts
from pyrocko import cake
from pyrocko.io import stationxml, quakeml
from pyrocko.dataset import crust2x2
from pyrocko.gui import marker
from shutil import copyfile
from . import cc, merge_freqs, checks
from . import clustering as cl
from .config import GeneralSettings, cc_comp_settings, clusty_config,\
network_similarity_settings, clustering_settings
from .config_settings_default import generate_default_config

import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.use('agg')

np = num

ver = 'V27JAN2023'

def station_selector(stations, catalog, **kwargs):
    '''
    small function to select subset of stations based on
    e.g. maximum distance or other metadata information
    (for example lower corner frequency or sensor
    type could be implemented)
    station allowlists and blocklists are implemented directly in
    main().
    :param stations: list of pyrocko stations
    :param catalog: list of pyrocko events
    '''

    if 'mindist_stations' in kwargs.keys() and 'maxdist_stations' in kwargs.keys():
        mid_events = orthodrome.geographic_midpoint(
            lats=num.asarray([ev.lat for ev in catalog]),
            lons=num.asarray([ev.lon for ev in catalog]),
            weights=None)

        subset_stations = []
        for st in stations:
            inter_sta_dist = orthodrome.distance_accurate50m(
                st.lat, st.lon, mid_events[0], mid_events[1])

            if inter_sta_dist > kwargs['mindist_stations'] and inter_sta_dist < kwargs['maxdist_stations']:
                subset_stations.append(st)


    elif 'maxdist_stations' in kwargs.keys() and 'mindist_stations' not in kwargs.keys():
        mid_events = orthodrome.geographic_midpoint(
            lats=num.asarray([ev.lat for ev in catalog]),
            lons=num.asarray([ev.lon for ev in catalog]),
            weights=None)
        subset_stations = [st for st in stations if orthodrome.distance_accurate50m(
            st.lat, st.lon, mid_events[0], mid_events[1]) < kwargs['maxdist_stations']]
    
    #model.dump_stations(subset_stations, filename='used_stations.yaml')

    return subset_stations


def get_ncomp(netsim_cnf, cc_cnf):
    if netsim_cnf.method_similarity_computation == 'product_combPS' or netsim_cnf.combine_components:
        n_comps = 1
    else:
        n_comps = len(cc_cnf.components)

    return n_comps

#def get_phasename(cc_cnf, netsim_cnf, i_ph):
    
#    n_ph = get_nph(netsim_cnf)

#    if len(cc_cnf.phase) == 2 and n_ph == 1:
#        phase_name = 'PScomb'
#    elif len(cc_cnf.phase) == 1 and n_ph == 1:
#        phase_name = cc_cnf.phase[0]
#    elif len(cc_cnf.phase) == 2 and n_ph == 2:
#        phase_name = cc_cnf.phase[i_ph]

#    return phase_name
def comp_cc(comp,station_list, cat, cc_cnf, waveform_dir,
                arrivals_array, bool_dist, n_workers, work_dir):
    cc.get_similarity_matrix(comp,station_list, cat, cc_cnf, waveform_dir,
                                arrivals_array, bool_dist, n_workers, work_dir)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--init', 
                        help='Generate example config file', required=False,
                         action='store_true')    
    parser.add_argument('--config', 
                        help='Name of config file', required=False)
    parser.add_argument('--run', 
                        help='Start complete clusty run', 
                        required=False, action='store_true')
    parser.add_argument('--cc',
                        help='Run clusty to get cross-correlation matrix, no' +
                            ' clustering started',
                        required=False, action='store_true')
    parser.add_argument('--netsim',
                        help='Compute network similarity from cross-' +
                            'correlation matrix',
                        required=False, action='store_true')
    parser.add_argument('--cluster',
                        help='Run clusty for clustering, network similarity' +
                            ' matrix needed as input',
                        required=False, action='store_true')
    parser.add_argument('--plot_results',
                        help='Plot clusty from result files',
                        required=False, action='store_true')
    parser.add_argument('--eps_analysis',
                        help='Run cluster analysis to select eps',
                        required=False, action='store_true')
    parser.add_argument('--cluster_param_analysis', #compatibility
                        help='Run cluster analysis to select appropriate' +
                            ' cluster parameters',
                        required=False, action='store_true')
    parser.add_argument('--merge_freq_results',
                        help='Harmonize and merge catalogs of different' +
                            ' frequency bands',
                         required=False, action='store_true')
    parser.add_argument('--export_stacked_traces',
                        help='Plot traces stacked by cluster index',
                        required=False, action='store_true')
    parser.add_argument('--log_level', default=logging.INFO,
                        type=lambda x: getattr(logging, x),
                        help='Configure the logging level')
    
    # add generate_config for example config later
    args = parser.parse_args()

    logger = logging.getLogger('__name__')
    logger.setLevel(args.log_level)

    # create console handler and set level
    ch = logging.StreamHandler()
    ch.setLevel(args.log_level)
    logger.addHandler(ch)

    logger.info('\n')
    logger.info('Welcome to clusty! (%s)' % ver)
    logger.info('\n')
    logger.info('IMPORTANT INFO!\nfrom V17DEC2021 Clusty strictly requires SI units: METERS(!) and SECONDS')
    logger.info('\n')
    logger.info('PLEASE ADJUST FROM KM TO METERS IN OLDER CONFIG FILES')
    logger.info('\n')

    if args.init:
        logger.info('Preparing basic config file.')
        fn = 'config_clusty.yaml'
        conf = generate_default_config()
        conf.dump(filename=fn)
        logger.info('Config file name: %s' % fn)

    if not args.init and not args.config:
        logger.error('Clusty needs a config file.')
        print(parser.print_help())

    if args.config and not args.merge_freq_results:
        try:
            cnf_all = clusty_config.load(filename=args.config)
        except guts.ArgumentError as e:
            false_arg = str(e).split(':')[1]
            logger.error('Invalid argument in config file: %s' %false_arg)
            logger.error('--> check config file')
            sys.exit()

        try:
            basic_cnf, cc_cnf, netsim_cnf, cl_cnf = cnf_all.settings
        except ValueError:
            basic_cnf, cc_cnf, netsim_cnf, cl_cnf, mf_cnf = cnf_all.settings

        logger.debug(basic_cnf)
        logger.debug(cc_cnf)

        logger.info('--> Checking input parameters.')

        os.makedirs('%s/results' % basic_cnf.work_dir, exist_ok=True)
        #copyfile(args.config, '%s/results/config_copy.yaml' % basic_cnf.work_dir)

        os.makedirs('%s/precalc_arrays' % basic_cnf.work_dir, exist_ok=True)

        logger.info('- Computed matrices will be saved at: %s/precalc_arrays' % 
                    basic_cnf.work_dir)
        logger.info('- Results will be saved at: %s/results' % 
                    basic_cnf.work_dir)

        # get event catalog
        try:
            cat = model.load_events(filename=basic_cnf.catalog_file)#, format='yaml')
            cat.sort(key=lambda ev: ev.time)
            for ev in cat:
                if ev.name == '' or ev.name == ' ':
                    ev.name = util.time_to_str(ev.time).replace(' ','_')

            filename = os.path.split(basic_cnf.catalog_file)[-1].split('.')[:-1][0]
            model.dump_events(cat, filename='%s/results/%s.yaml' % (basic_cnf.work_dir,filename), format='yaml')

        except FileNotFoundError:
            logger.warning('ERROR: Cannot load event catalog. Aborting clusty run.')
            sys.exit()

        if len(cat) < 5:
            logger.error('ERROR: The catalog contains less than 5 events. Aborting clusty run.')

        # load stations
        station_list = []
        if basic_cnf.station_file:
            if (cc_cnf.coordinate_system == 'plane' and basic_cnf.station_file.endswith('.pf')):
                logger.warning('ERROR: Local coordinates require a station_file in yaml format for correctly parsing north_shift and east_shift')
                sys.exit()

            if not basic_cnf.station_file.endswith('xml'):
                station_list = model.station.load_stations(basic_cnf.station_file)
            else:
                station_list = stationxml.load_xml(filename=basic_cnf.station_file).get_pyrocko_stations()

        elif basic_cnf.station_file_list:
            for file in basic_cnf.station_file_list:
                if (cc_cnf.coordinate_system == 'plane' and basic_cnf.station_file.endswith('.pf')):
                    logger.error('ERROR: Local coordinates require a station_file in yaml format with north_shift and east_shift')
                    sys.exit()

                if file.endswith('.xml'):
                    station_list.extend(stationxml.load_xml(filename=file).get_pyrocko_stations())
                elif file.endswith('.yaml') or file.endswith('.pf'):
                    station_list.extend(model.station.load_stations(file))
        else:
            pass


        if station_list == []:
            logger.warning('No stations found. Please use stationxml with file'
                + 'extension ''xml'''
                + 'or pyrocko station format with file extension'
                + ' ''.pf'' or ''.yaml''.')

        if basic_cnf.st_allowlist != []:
            logger.info('st allow list: %s' % basic_cnf.st_allowlist)
            if '.' in basic_cnf.st_allowlist[0]:
                station_list = [st for st in station_list if '%s.%s' % 
                                (st.network, st.station) 
                                in basic_cnf.st_allowlist]
            else:
                station_list = [st for st in station_list if '%s' % 
                                (st.station) in basic_cnf.st_allowlist]

        if basic_cnf.st_blocklist != []:
            if '.' in basic_cnf.st_blocklist[0]:
                station_list = [st for st in station_list if not '%s.%s' % 
                                (st.network, st.station) 
                                in basic_cnf.st_blocklist]
            else:
                station_list = [st for st in station_list if not '%s' % 
                                (st.station) in basic_cnf.st_blocklist]

        # ignore location codes of the stations! usage of location codes is arbitrary in many
        # cases, often changing over time... (if you do not want to ignore location codes,
        # the trace selection in cc.py needs to be changed as well as uncommenting this:)
        location_code_warning = False
        stats_out = []
        netsta = list(set(['%s.%s' % (st.network, st.station) for st in station_list]))
        netsta.sort()
        for ns in netsta:
            stats_now = [st for st in station_list if '%s.%s' % (st.network, st.station) == ns]
            if len(stats_now) == 1:
                stats_out.append(stats_now[0])
            else:
                location_code_warning = True
                # combine channels
                all_channels = []
                for s in stats_now:
                    chs = s.channels
                    all_channels.extend(chs)

                keep_channels = []
                keep_channels_names = []
                for ch in all_channels:
                    test = ch.name
                    if test not in keep_channels_names:
                        keep_channels_names.append(test)
                        keep_channels.append(ch)
                stats_now[0].channels = keep_channels
                stats_out.append(stats_now[0])

        station_list = stats_out


        if location_code_warning:
            logger.warning('WARNING: One or multiple stations have more than one location code. Clusty is ignoring location codes.')

        if basic_cnf.station_subset:
            kwargs = basic_cnf.station_subset
            station_list = station_selector(station_list, cat, **kwargs)

        logger.info('- Check passed: Number of events: %s' % len(cat))
        logger.info('- Check passed: Number of stations: %s' % len(station_list))
        logger.info('- Used stations: %s' % [st.station for st in station_list])


        if station_list == []:
            logger.warning('No stations found.')
            logger.warning('    (A) Make sure that the PATH to the station file is correct')
            logger.warning('    (B) Provide all distances in METERS (maxdist_stations, mindist_stations)')
            sys.exit()


        if netsim_cnf.method_similarity_computation == 'trimmed_mean_ccs':
            sta_reject = len(station_list)*netsim_cnf.trimm_cut
            floor_val = np.floor(sta_reject)
            round_val = np.round(sta_reject)

            if (floor_val != round_val and len(station_list) < 20):
                logger.warning('\nWARNING: Small number of stations and possibly unappropriate trimm_cut value:\n\n\
        Maximum number of rejected cc values: {%i*%.2f} = %i.\n\
        If you expected %i, please adjust TRIMM_CUT accordingly.' % (len(station_list),netsim_cnf.trimm_cut,floor_val,int(round_val)))


        plformat = basic_cnf.plot_format

        if args.run or args.cc or args.plot_results:
            # only check waveform dir if needed (cc calc or waveform plotting)
            if (not cc_cnf.use_precalc_ccs) or cl_cnf.wf_plot or cl_cnf.wf_cl_snuffle:
                if '*' in basic_cnf.waveform_dir:
                    wfdirs_check = glob.glob(basic_cnf.waveform_dir)
                    if len(wfdirs_check) <1:
                        logger.error('ERROR: Waveform directories not found: %s' % basic_cnf.waveform_dir)
                        sys.exit()
                    for wfd in wfdirs_check:
                        if len(os.listdir(wfd)) < 1:
                            logger.warning('ERROR: One waveform directory is empty: %s' % wfd)

                else:
                    if os.path.exists(basic_cnf.waveform_dir):
                        if len(os.listdir(basic_cnf.waveform_dir)) > 0:
                            logger.info('- Check passed: Waveform directory found and contains data: %s'
                                        % basic_cnf.waveform_dir)
                        else:
                            logger.error('ERROR: Waveform directory is empty: %s' % basic_cnf.waveform_dir)
                            sys.exit()
                    else:
                        logger.error('ERROR: Waveform directory not found: %s' % basic_cnf.waveform_dir)
                        sys.exit()

        logger.info('\n')

        # checking other config file parameters, use debug instead of info
        checks.check(cc_cnf, netsim_cnf, cl_cnf)

        # check if dumping also works...
        cnf_all.dump(filename='%s/results/config_copy.yaml' % basic_cnf.work_dir)



    
    if args.run or args.cc:
        '''
        start complete clusty run or cc-computation with cross-correlation
        matrix computation
        '''

        # save used stations here to be sure that order of station is same as in arrival time and cross-correlation file
        # (not important for network similarity and clustering, but for using cc or tshift matrices outside clusty)
        # (or for aligning waveforms for waveform plots and exporting)

        model.dump_stations_yaml(station_list, '%s/used_stations.yaml' % basic_cnf.work_dir)

        if not cc_cnf.use_precalc_arrivals:

            arrivals_array = num.empty((len(cc_cnf.phase),
                                            len(station_list),
                                            len(cat)))
            arrivals_array.fill(num.nan)
            phases = cc_cnf.phase

            if (cc_cnf.compute_arrivals and cc_cnf.pick_path == ''):
                logger.info('Computing arrival times.')

                if cc_cnf.coordinate_system == 'spherical':
                    st_lats = num.asarray([st.lat for st in station_list])
                    st_lons = num.asarray([st.lon for st in station_list])
                    st_depths = num.asarray([-st.elevation if st.depth == 0 else st.depth for st in station_list])

                    if 'P' in phases or 'p' in phases or 'S' in phases or 's' in phases:

                        if len(cc_cnf.homogen_model) != 0:
                            logger.info('---> Using homogeneous model')

                            # #using cake is an overkill, here
                            # vmodel_base = cake.LayeredModel()
                            # vp = cc_cnf.homogen_model[0] #first phase velocity has to be P

                            # if len(cc_cnf.homogen_model) == 1:
                            #     vs = vp/1.8125 # pyrocko standard vp/vs ratio
                            # elif len(cc_cnf.homogen_model) == 2:
                            #     vs = cc_cnf.homogen_model[1]

                            # mat = cake.Material(vp=vp,vs=vs,rho=2670,qp=200, qs=100)
                            # vmodel_base.append(cake.HomogeneousLayer(0, 100e3, mat))

                        else:
                            if cc_cnf.vmodel == '':
                                vmodels = [cake.load_model(crust2_profile=crust2x2.get_profile(st.lat, st.lon))
                                        for st in station_list]
                                logger.info('---> Using crust2x2 model based on station location')
                            else:
                                vmodel_base = cake.load_model(cc_cnf.vmodel)
                                logger.info('---> Using provided velocity model')

                        for i_st, st in enumerate(station_list):

                            if cc_cnf.vmodel == '' and len(cc_cnf.homogen_model) == 0:
                                vmodel = vmodels[i_st].copy_with_elevation(st.elevation)

                            elif len(cc_cnf.homogen_model) == 0:
                                vmodel = vmodel_base.copy_with_elevation(st.elevation)
                            else:
                                pass
                                # #using cake layer
                                # vmodel = copy.deepcopy(vmodel_base)
                                # vmodel._elements[0].ztop = -st.elevation

                            for i_ev, ev in enumerate(cat):

                                ev_sta_dist = orthodrome.distance_accurate50m_numpy(
                                    ev.lat, ev.lon, st.lat, st.lon)

                                for i_ph, phase_name in enumerate(phases):
                                    if phase_name == 'P' or phase_name == 'p':
                                        phase_names = ['p', 'P']
                                    elif phase_name == 'S' or phase_name == 's':
                                        phase_names = ['s', 'S']
                                    #elif phase_name == 'R' or phase_name == 'L':
                                    #    phase_names = ['p', 'P']
                                        # surface waves hav so long period that
                                        # exact beginning does not matter.
                                        # only true for regional settings, therefore
                                        # new arrival computation added below!

                                    if len(cc_cnf.homogen_model) != 0:
                                        print(st.elevation,st.depth)
                                        hypo_dist = np.sqrt(ev_sta_dist**2 + (ev.depth - st_depths[i_st])**2)
                                        tt = hypo_dist/cc_cnf.homogen_model[i_ph]

                                        arrivals_array[i_ph, i_st, i_ev] = ev.time + tt

                                    else:
                                        arrivals = vmodel.arrivals(distances=[ev_sta_dist*cake.m2d],
                                                                   phases=phase_names,
                                                                  zstart=ev.depth,
                                                                  zstop=st_depths[i_st])

                                        if arrivals:
                                            min_t = min(arrivals, key=lambda x: x.t).t
                                            arrivals_array[i_ph, i_st, i_ev] = ev.time + min_t
                                        else:
                                            arrivals_array[i_ph, i_st, i_ev] = num.nan


                    elif 'R' in phases or 'r' in phases or 'L' in phases or 'l' in phases:
                        v_surface = 4500
                        for i_ev, ev in enumerate(cat):
                            ev_sta_dists = orthodrome.distance_accurate50m_numpy(
                                            ev.lat, ev.lon, st_lats, st_lons)

                            for i_st, st in enumerate(station_list):
                                ev_sta_dist = ev_sta_dists[i_st]
                                for i_ph, ph in enumerate(phases):
                                    arr = ev.time + (ev_sta_dist/v_surface)
                                    arrivals_array[i_ph, i_st, i_ev] = arr

                    else:
                        logger.warning('Phase name not defined, please use phase R, L, P or S.')

                elif cc_cnf.coordinate_system == 'plane':

                    from scipy.spatial.distance import cdist

                    if len(cc_cnf.homogen_model) != 0:

                        if 'P' in phases or 'p' in phases or 'S' in phases or 's' in phases:
                            sta_coords = num.asarray([[st.east_shift,st.north_shift,st.depth] for st in station_list])
                            ev_coords = num.asarray([[ev.east_shift,ev.north_shift,ev.depth] for ev in cat])
                            ev_time = num.asarray([ev.time for ev in cat])
                            ev_sta_dists = cdist(sta_coords, ev_coords)

                            for i_ph, phase_name in enumerate(phases):
                                tt = ev_sta_dists/cc_cnf.homogen_model[i_ph]
                                arrivals_array[i_ph] = num.add(tt, ev_time)

                        else:
                            logger.warning('Phase name not defined or not available in local coordinate mode: Use P or S.')
                    else:
                        logger.warning('Layered models not implemented for local coordinate system')

                num.save(file='%s/precalc_arrays/arrivals_array' % basic_cnf.work_dir, arr=arrivals_array)

            elif cc_cnf.pick_path != '' and cc_cnf.pick_option == 'pyr_path':
                logger.info('Loading picks from pick directory')
                for i_ph, phase_name in enumerate(phases):
                    for i_st, st in enumerate(station_list):
                        for i_ev, ev in enumerate(cat):
                            if not cc_cnf.pick_file_template:
                                try:
                                    picks = marker.load_markers('%s/%s.pf' %
                                        (cc_cnf.pick_path, ev.name))

                                except FileNotFoundError:
                                    arrivals_array[i_ph, i_st, i_ev] = False
                                    continue
                            else:
                                try:
                                    picks = marker.load_markers('%s/%s' %
                                        (cc_cnf.pick_path,
                                        Template(cc_cnf.pick_file_template).substitute(dict(event_name = ev.name))))

                                except FileNotFoundError:
                                    arrivals_array[i_ph, i_st, i_ev] = False
                                    continue

                            # reject phases without phasename
                            picks = [pik for pik in picks if type(pik) == marker.PhaseMarker]

                            try:
                                arr = [pick.tmin for pick in picks if
                                    (pick.match_nsl((st.network, st.station, st.location)))][0] 
                                    # and pick._phasename == phase_name)][0]
                                arrivals_array[i_ph, i_st, i_ev] = arr
    
                            except IndexError:  # if the given phase has no pick
                                arrivals_array[i_ph, i_st, i_ev] = num.nan

                num.save(file='%s/precalc_arrays/arrivals_array' % basic_cnf.work_dir, arr=arrivals_array)


            elif cc_cnf.pick_path != '' and cc_cnf.pick_option == 'pyr_file':
                logger.info('Loading picks from single pick file')
                picks = marker.load_markers(cc_cnf.pick_path)

                picks_read = 0
                for i_ph, phase_name in enumerate(phases):
                    for i_st, st in enumerate(station_list):
                        for i_ev, ev in enumerate(cat):
                            # for pick in picks:
                            #     if pick.match_nsl((st.network, st.station,st.location)):
                            #         if pick._phasename == phase_name:
                            #             if np.abs(pick._event_time-ev.time) <= 1e-3:
                            #                 print(pick._event_time, ev.time, np.abs(pick._event_time-ev.time))
                            # print('------------------------------------')

                            try:
                                arr = [pick.tmin for pick in picks if
                                    (pick.match_nsl((st.network, st.station,st.location))
                                        and pick._phasename == phase_name
                                        and np.abs(pick._event_time-ev.time) <= 1e-3)][0]
                                arrivals_array[i_ph, i_st, i_ev] = arr
                                picks_read +=1
                                #print(arr)

                            except IndexError:  # if the given phase has no pick
                                arrivals_array[i_ph, i_st, i_ev] = num.nan
                                #print('error')

                            # #quick and dirty precision fix for marker loading/saving
                            # for pick in picks:
                            #     if pick.match_nsl((st.network, st.station, st.location)):

                            #         short_str1 = util.tts(ev.time, format='%Y-%m-%d %H%M%S.3FRAC')
                            #         short_str2 = util.tts(pick._event_time, format='%Y-%m-%d %H%M%S.3FRAC')

                            #         if short_str1 == short_str2:
                            #             arrivals_array[i_ph, i_st, i_ev] = pick.tmin
                            #             picks_read +=1

                logger.info('%i picks loaded for %i' % (picks_read,len(cat)))
                num.save(file='%s/precalc_arrays/arrivals_array' % basic_cnf.work_dir, arr=arrivals_array)


            elif cc_cnf.pick_path != '' and cc_cnf.pick_option == 'xml':
                logger.info('Generating arrivals array from picks in xml format.')

                search_me = os.path.join(cc_cnf.pick_path, '*.xml')
                pick_files = glob.glob(search_me)  # find all files
                logger.info('found pick files: %s' % pick_files)

                # for sorting into arrival array matrix (has potential to be improved...)
                event_iev_dict = {}
                for iev, ev in enumerate(cat):
                    event_iev_dict[ev.name] = iev

                nst_ist_dict = {}
                st_ist_dict = {}
                for ist, st in enumerate(station_list):
                    nst_ist_dict['%s.%s' % (st.network, st.station)] = ist
                    st_ist_dict[st.station] = ist

                ph_iph_dict = {}
                for iph, ph in enumerate(phases):
                    ph_iph_dict[ph] = iph

                #logger.info('dicts st: %s, ev: %s, ph: %s' % (len(st_ist_dict.keys()), len(event_iev_dict.keys()),
                #    len(ph_iph_dict.keys())))

                for f in pick_files:
                    try:
                        qml = quakeml.QuakeML.load_xml(filename=f)
                    except:
                        logger.debug('could not load file: %s' % f)
                    for ev in qml.event_parameters.event_list:
                        evname = ev.public_id.split('/')[-1]
                        logger.debug('event. %s' % evname)
                        try:
                            iev = event_iev_dict[evname]
                            logger.debug('%s %s' %(evname, cat[iev].name))
                        except KeyError:
                            logger.warning('Event not found. %s' % evname)
                            continue

                        for pick in ev.pick_list:
                            ph = pick.phase_hint.value
                            if ph == 'AML':
                                continue
                            try:
                                iph = ph_iph_dict[ph]
                                logger.debug('%s %s' %(ph, phases[iph]))
                            except KeyError:
                                logger.warning('Phase not found. %s' % ph)
                                continue

                            stcode = pick.waveform_id.station_code
                            netcode = pick.waveform_id.network_code
                            if len(netcode) < 1:
                                logger.debug('No network code for station %s' % stcode)
                                try:
                                    ist = st_ist_dict[stcode]
                                    logger.debug('%s %s' %(stcode, station_list[ist].station))
                                except KeyError:
                                    logger.info('Station not found. %s' % stcode)
                                    continue
                            else:
                                try:
                                    ist = nst_ist_dict['%s.%s' % (netcode, stcode)]
                                    logger.debug('%s %s' %(stcode, station_list[ist].station))
                                except KeyError:
                                    logger.info('Station not found. %s.%s' % (netcode, stcode))
                                    continue

                            picktime = pick.time.value
                            logger.debug('pick time: %s' % picktime)
                            arrivals_array[iph, ist, iev] = picktime

                            #if stcode == 'ITM' or stcode == 'FSK' or stcode == 'KALE':
                            #logger.info('%s: %s %s %s' %(stcode,ph, cat[iev].name, picktime))
                            #logger.info('%s %s %s %s %s' %(iph, ist,  iev, picktime, arrivals_array[iph, ist, iev]))

                            #if logger.getLogger().isEnabledFor(logger.DEBUG):
                            #    print('pick arr: ', ph, stcode, cat[iev].name, util.tts(picktime))
                            #    vmodel = cake.load_model(crust2_profile=crust2x2.get_profile(st.lat, st.lon))#

                            #    if ph == 'P' or ph == 'p':
                            #        phase_names__ = ['p', 'P']
                            #    elif ph == 'S' or ph == 's':
                            #        phase_names__ = ['s', 'S']
                            #    print(ph, phase_names__)

                            #    ev_sta_dist = orthodrome.distance_accurate50m_numpy(
                            #    cat[iev].lat, cat[iev].lon, station_list[ist].lat, station_list[ist].lon)
                            #    caki = vmodel.arrivals(distances=[ev_sta_dist[0]*cake.m2d],
                            #                               phases=phase_names__,
                            #                              zstart=cat[iev].depth)
                                #for c in caki:
                                #    print(c)

                            #    if caki:
                            #        min_t = min(caki, key=lambda x: x.t).t
                            #        print('cake arr:', util.tts(cat[iev].time + min_t))
                            #    else:
                            #        print('no cake')
                            #    print('-----------------------------')

                #num.set_printoptions(suppress=True,
                #    formatter={'float_kind':'{:16.3f}'.format}, linewidth=130)
                #pprint.pprint(arrivals_array)
                num.save(file='%s/precalc_arrays/arrivals_array' % basic_cnf.work_dir, arr=arrivals_array)
                
                logger.debug('arrival-array size: %s' % str(arrivals_array.shape))
                logger.debug('min arr time: %s' % str(num.nanmin(arrivals_array)))
                logger.debug('max arr time: %s' % str(num.nanmax(arrivals_array)))

            else:
                logger.error('Please choose pick file or use cake computation (Set \'compute_arrivals: true\').')
                sys.exit()

        else:
            arrivals_array = num.load('%s/precalc_arrays/arrivals_array.npy' % basic_cnf.work_dir)
            if arrivals_array.shape[1] != len(station_list):
                logger.error('Arrival time array dimensions does not fit number of stations.')
                sys.exit()
    
        logger.info('Arrival times succesfully calculated/loaded.')
        
        ### cross-correlations ###
        if not cc_cnf.use_precalc_ccs:

            bool_dist, ev_ev_dist_array = cc.check_distance(cat, cc_cnf.max_dist,cc_cnf.dist_hypocentral,cc_cnf.coordinate_system)
            num.save(arr=ev_ev_dist_array, file='%s/precalc_arrays/ev_ev_dist_array'  % basic_cnf.work_dir)

            for i_comp, comp in enumerate(cc_cnf.components):
                comp_cc(comp,station_list, cat, cc_cnf, basic_cnf.waveform_dir,
                               arrivals_array, bool_dist, basic_cnf.n_workers, basic_cnf.work_dir)
                # cc.get_similarity_matrix(comp,station_list, cat, cc_cnf, basic_cnf.waveform_dir,
                #                             arrivals_array, bool_dist, basic_cnf.n_workers)
                # coef_array, tshifts_array, weights_array = cc.get_similarity_matrix(
                #     comp,station_list, cat, cc_cnf, basic_cnf.waveform_dir,
                #     arrivals_array, bool_dist, basic_cnf.n_workers)
                # print(sys.getsizeof(coef_array))
                # print(sys.getsizeof(tshifts_array))
                # print(sys.getsizeof(weights_array))
                # num.save(arr=coef_array, file='./precalc_arrays/cccoef_array_%s' % comp_base)
                # num.save(arr=weights_array, file='./precalc_arrays/ccweights_array_%s' % comp_base)
                # num.save(arr=tshifts_array, file='./precalc_arrays/cctshifts_array_%s' % comp_base)
                
                # del coef_array
                # del tshifts_array
                # del weights_array
        
                #if cc_cnf.plot_npairs_stats:
                #    n_pairs = [num.where(coef_array[0,i] > 0.7).shape[0] for i in range(len(station_list))]
                #    fig, ax = plt.subplots(nrows=2)
                #    ax[0].plot(npairs)
                #    ax[0].set_ylabel('n_pairs')
                #    ax[0].set_xlabel('station index')
                #    plt.savefig('./results/npairs_stats.png')
                #    plt.close()
        else:

            for i_comp, comp in enumerate(cc_cnf.components):
                comp_base = comp[-1]
                logger.info('Using precalculated arrays.')
                logger.debug('Found cccoef: %s' % os.path.exists('%s/precalc_arrays/cccoef_array_%s.npy' %  (basic_cnf.work_dir, comp_base)))
                logger.debug('Found ccweights: %s' % os.path.exists('%s/precalc_arrays/ccweights_array_%s.npy' %  (basic_cnf.work_dir, comp_base)))
                logger.debug('Found cctshifts: %s' % os.path.exists('%s/precalc_arrays/cctshifts_array_%s.npy' %  (basic_cnf.work_dir, comp_base)))
                logger.debug('Found ev_ev_dists: %s' % os.path.exists('%s/precalc_arrays/ev_ev_dist_array.npy' %  basic_cnf.work_dir))
            
        # if cc_cnf.plot_cc_dist:
        #     for i_ph, phase_name in enumerate(cc_cnf.phase):
        #         cc.plot_cc_over_dist(coef_array[i_ph], ev_sta_dist_array, phase_name, cc_cnf.max_dist)
            
        logger.info('Cross-correlation section ready.')

    if args.run or args.netsim:
        '''
        Run network similarity computation from cross-correlation matrix,
        either as part of entire clusty run or of net_sim computation'
        '''

        ### optional: compute station weights based on azimuthal distribution 

        az_stats_weights = None
        if netsim_cnf.get_station_weights:
            logger.info('Starting interactive station weighting selection.')
            az_stats_weights = cc.station_weighting(station_list, cat)
            if not az_stats_weights == []:
                num.save(arr=az_stats_weights, file='az_stats_weights')

        n_comp = len(cc_cnf.components)
        n_ev = len(cat)
        network_similarity =num.empty((n_comp,n_ev, n_ev))
        n_used_stats = num.empty((n_comp,n_ev, n_ev))
        # logger.info('NETSIM allocation:::%.2f' % time.time())
        
        for i_comp, comp in enumerate(cc_cnf.components):
            comp_base = comp[-1]
            logger.info('\n----------- COMPONENT: %s -----------' % comp_base)
            if not netsim_cnf.use_precalc_net_sim:
                logger.info('Loading cc-arrays.')
                
                coef_array = num.load('%s/precalc_arrays/cccoef_array_%s.npy' % (basic_cnf.work_dir,comp_base))
                if coef_array.shape[0] != len(station_list):
                    logger.error('CC coef. array dimensions does not fit number of stations.')
                    sys.exit()
                weights_array = num.load('%s/precalc_arrays/ccweights_array_%s.npy' % (basic_cnf.work_dir, comp_base))
                tshifts_array = num.load('%s/precalc_arrays/cctshifts_array_%s.npy' % (basic_cnf.work_dir, comp_base))
                #ev_ev_dist_array = num.load('./precalc_arrays/ev_ev_dist_array.npy')            
    
                logger.info('Starting computation of network similarity.')
                n_ph = len(cc_cnf.phase)

                if not netsim_cnf.method_similarity_computation == 'trimmed_mean_ccs':

                    network_similarity[i_comp] = cc.stack_ccs_over_stations(
                        coef_array, weights_array, az_stats_weights, netsim_cnf,
                        station_list, cat)

                else:
                    if netsim_cnf.get_bool_sta_use:
                        network_similarity[i_comp], n_used_stats[i_comp], sta_use = cc.stack_ccs_over_stations(
                            coef_array, weights_array, az_stats_weights, netsim_cnf,
                            station_list, cat)

                        num.save(arr=sta_use, file='%s/precalc_arrays/sta_use_%s.npy' % (basic_cnf.work_dir, comp_base))
                        num.save(arr=n_used_stats, file='%s/precalc_arrays/n_used_stats.npy' % basic_cnf.work_dir)
                    else:
                        network_similarity[i_comp], n_used_stats[i_comp] = cc.stack_ccs_over_stations(
                            coef_array, weights_array, az_stats_weights, netsim_cnf,
                            station_list, cat)

                        num.save(arr=n_used_stats, file='%s/precalc_arrays/n_used_stats.npy' % basic_cnf.work_dir)
                        

                num.save(arr=network_similarity, file='%s/precalc_arrays/network_similarity.npy' % basic_cnf.work_dir)
        
            else:
                network_similarity = num.load('%s/precalc_arrays/network_similarity.npy' % basic_cnf.work_dir)
                if netsim_cnf.method_similarity_computation == 'trimmed_mean_ccs':
                    n_used_stats = num.load('%s/precalc_arrays/n_used_stats.npy' % basic_cnf.work_dir)
            
            logger.info('Netsim for %s component finished.' % (comp_base))
    
            title = 'Net-sim \n(%s)' % netsim_cnf.method_similarity_computation

            # imshow plots might have some issues with matrix dimensions ;-)
            #fig_name = netsim_cnf.method_similarity_computation + phase_name
            #cc.small_imshow_plot(network_similarity[i_ph], cat, title,
            #            cbarlabel=None, fig_name=fig_name)
    
            #if netsim_cnf.method_similarity_computation == 'trimmed_mean_ccs':
            #    fig_name = './results/used_stats_%s_%s.png' % (
            #               netsim_cnf.method_similarity_computation, phase_name)
            #    title_s = 'n_st in net-sim comp.'
            #    #cc.small_imshow_plot(n_used_stats[i_ph], cat, title_s,
            #    #                     cbarlabel=None, fig_name=fig_name, 
            #    #                     scale=(0,num.max(n_used_stats[i_ph])))

            if not netsim_cnf.combine_components:
                logger.debug('Computing distance matrix.')
                cl.get_distance_matrix(network_similarity[i_comp], comp, basic_cnf.work_dir)

        if netsim_cnf.combine_components:
            logger.debug('Combining P and S network similarity results.')
            network_similarity = cc.comb_avg(network_similarity, netsim_cnf.weights_components)
            logger.debug('Computing distance matrix.')
            name = 'comb'
            cl.get_distance_matrix(network_similarity[0], name, basic_cnf.work_dir)

        logger.info('Network similarity and distance matrix ready')


    if args.eps_analysis or args.cluster_param_analysis:
        '''
        --> knn -plot for eps/xi setting, use minPts from minPts config list (cl_cnf.dbscan_min_pts)
        --> one plot per minPts value when using dbscan
        '''

        if cl_cnf.method in ['optics','OPTICS','dbscan','DBSCAN']:
            n_comps = get_ncomp(netsim_cnf, cc_cnf)
            for i_comp in range(n_comps):
                if n_comps == 1 and netsim_cnf.combine_components:
                    comp_name = 'comb'
                else:
                    comp_name = cc_cnf.components[i_comp]

                dist_file = '%s/precalc_arrays/dists_%s.npy' % (basic_cnf.work_dir, comp_name)
                dists = num.load(dist_file)
                num.fill_diagonal(dists, 0.0)

                param_list,min_pts_list,param_name = cl.get_cluster_params_cnf(cl_cnf)

                cl.knn_plot(dists, min_pts_list, comp_name, basic_cnf.work_dir, plformat)

                cl.plot_param_analysis(dists, comp_name, cl_cnf, netsim_cnf, basic_cnf.work_dir)

        else:
            logger.error('Clustering method not implemented. Please use ```dbscan``` or ```optics```.')

##########
    if args.cluster or args.run:

        logger.info('Starting clustering')

        n_comps = get_ncomp(netsim_cnf, cc_cnf)

        for i_comp in range(n_comps):
            if n_comps == 1 and netsim_cnf.combine_components:
                comp_name = 'comb'
            else:
                comp_name = cc_cnf.components[i_comp]

            #phase_name = get_phasename(cc_cnf, netsim_cnf, i_ph)
            dist_file = '%s/precalc_arrays/dists_%s.npy' % (basic_cnf.work_dir, comp_name)
            dists = num.load(dist_file)
            num.fill_diagonal(dists, 0.0)
            no_dist_evs = cl.get_list_event_no_dist(dists)

            cluster_labels_list,methodstrings = cl.cluster(dists, comp_name, cl_cnf=cl_cnf, netsim_cnf=netsim_cnf, reach_plot=True)

            for cluster_labels,method_string in zip(cluster_labels_list, methodstrings):
                outfile = '%s/results/cat_%s.yaml' % (basic_cnf.work_dir, method_string)
                cat = cl.catalog_output(cat, cluster_labels, outfile, no_dist_evs)


            result_catalogs = [os.path.join(basic_cnf.work_dir, 'results', 'cat_' + ms+ '.yaml') for ms in methodstrings]
            result_catalogs = sorted(result_catalogs)


            logger.debug('Catalogs entering into harmonization: %s' %result_catalogs)

            if len(result_catalogs) > 1:

                cl.harmonize_cluster_labels(file_list=result_catalogs, work_dir=basic_cnf.work_dir)

            for cat_path in result_catalogs:
                cat = model.load_events(cat_path)
                cluster_labels,cluster_labels_set = cl.get_cluster_labels(cat)
                n_cluster = len(cluster_labels_set)-1
                n_clustered = sum(num.array(cluster_labels) != -1)

                method_string = os.path.split(cat_path)[-1][4:-5]
                param1 = method_string.split('_')[-2]
                min_pts= method_string.split('_')[-1]

                cluster_params = [param1,min_pts]

                if cl_cnf.plot_netsim_matrix:
                    if args.cluster:
                        network_similarity = num.load('%s/precalc_arrays/network_similarity.npy' % basic_cnf.work_dir)
                        cl.netsim_matrix(network_similarity[i_comp], cluster_labels, cat,
                                      method_string, basic_cnf.work_dir, plformat)

                if n_cluster > 0:
                    logger.info('Preparing Fruchtermann plot for %s ' % method_string)
                    if cl_cnf.frucht_plot_lib:
                        cl.fruchtermann(dists=dists, catalog=cat,
                                    cluster_params=cluster_params,
                                    cc_cnf=cc_cnf, netsim_cnf=netsim_cnf, cl_cnf=cl_cnf,
                                    method_string=method_string, work_dir=basic_cnf.work_dir, plformat=plformat)

                else:
                    logger.info('No clusters found for %s. Skip Fruchtermann plot.' % method_string)

                cl.silhouette_plot(dists, cat, method_string, cluster_params, cc_cnf, basic_cnf.work_dir, plformat)
                
            if len(result_catalogs ) > 1:
                logger.info('Preparing sankey plot')
                # only pass work_dir since nothing else needed from basic_cnf
                cl.sankey_multi(result_catalogs, basic_cnf.work_dir, plot_labels=cl_cnf.sankey_labels)


            if cl_cnf.repr_ev:

                eps_minpts_tuple_list = list(itertools.product(set(cl_cnf.dbscan_eps), 
                                                      set(cl_cnf.dbscan_min_pts)))

                for eps, min_pts in eps_minpts_tuple_list:
                    method_string = '%s_%s_%s_%.3f_%s' % (netsim_cnf.method_similarity_computation,
                                              cl_cnf.method, comp_name, eps, min_pts)

                    result_catalog = model.load_events(os.path.join('%s/results/' % basic_cnf.work_dir, 'cat_' + method_string+ '.yaml'))
                    representative_events = cl.get_repr_ev(result_catalog, dists, method_string, basic_cnf.work_dir)

                '''
                freeparam,minpts, param_nme = cl.get_cluster_params(cl_cnf.repr_ev, cl_cnf)
                for i_comp in range(n_comps):
                    #phase_name = get_phasename(cc_cnf, netsim_cnf, i_ph)
                    if n_comps == 1 and netsim_cnf.combine_components:
                        comp_name = 'comb'
                    else:
                        comp_name = cc_cnf.components[i_comp]
                    dists = num.load('%s/precalc_arrays/dists_%s.npy' % (basic_cnf.work_dir, comp_name))
                    try:
                        method_string = [os.path.split(f)[-1][4:-5] for f in result_catalogs 
                                         if float((os.path.split(f)[-1][4:-5]).split('_')[-2]) == freeparam
                                         and float((os.path.split(f)[-1][4:-5]).split('_')[-1]) == minpts
                                         and (os.path.split(f)[-1][4:-5]).split('_')[-3] == comp_name][0]
                        result_catalog = model.load_events(os.path.join('%s/results/' % basic_cnf.work_dir, 'cat_' + method_string + '.yaml'))
                    except:
                        logger.error('Catalog for chosen %s and minpts for saving representative events not found.' % param_name)
                        logger.info(method_string)
                        sys.exit()
                
                representative_events, max_sil_vals = cl.get_repr_ev(result_catalog, dists, method_string, basic_cnf.work_dir)
                '''

    if args.run or args.plot_results:
        error = False

        logger.info('Preparing result plots.')

        param_list,min_pts_list,param_name = cl.get_cluster_params_cnf(cl_cnf)

        freeparam_minpts_tuple_list = list(itertools.product(set(param_list),
                                                      set(min_pts_list)))
        methodstrings = []
        n_comps = get_ncomp(netsim_cnf, cc_cnf)
        for i_comp in range(n_comps):
            #phase_name = get_phasename(cc_cnf, netsim_cnf, i_ph)
            if n_comps == 1 and netsim_cnf.combine_components:
                comp_name = 'comb'
            else:
                comp_name = cc_cnf.components[i_comp]

            for freeparam, min_pts in freeparam_minpts_tuple_list:
                method_string = '%s_%s_%s_%.3f_%s' % (
                                 netsim_cnf.method_similarity_computation,
                                 cl_cnf.method, comp_name, freeparam, min_pts)
                methodstrings.append(method_string)

        result_catalogs = [os.path.join('%s/results/' % basic_cnf.work_dir, 'cat_' + ms+ '.yaml') 
                           for ms in methodstrings]
        result_catalogs = sorted(result_catalogs)

        if cl_cnf.wf_cl_snuffle:

            stpath = os.path.join('%s/used_stations.yaml' % basic_cnf.work_dir)
            logger.info(stpath)
            stats = model.load_stations(stpath)
            
            for cluster_params in cl_cnf.wf_cl_snuffle:
                freeparam = cluster_params[0]
                minpts = cluster_params[1]

                logger.info('Opening waveform snuffle for %s %s and minpts %s.' % (param_name, freeparam, minpts))
                
                if (not cl_cnf.wf_plot_stats) or cl_cnf.wf_plot_stats == []:
                    subset_stations_and_indices = [(i_st, st) for i_st, st in enumerate(stats)]
                else:
                    subset_stations_and_indices = [(i_st, st) for i_st, st in enumerate(stats)
                                                    if '%s.%s' % (st.network, st.station) in cl_cnf.wf_plot_stats]

                arrivals_array = num.load('%s/precalc_arrays/arrivals_array.npy' % basic_cnf.work_dir)

                #try:
                #    tshifts_array = num.load('./precalc_arrays/cctshifts_array.npy')
                #except:
                #    tshifts_array = None

                for i_comp in range(n_comps):
                    #phase_name = get_phasename(cc_cnf, netsim_cnf, i_ph)
                    if n_comps == 1 and netsim_cnf.combine_components:
                        comp_name = 'comb'
                    else:
                        comp_name = cc_cnf.components[i_comp]
                    dists = num.load('%s/precalc_arrays/dists_%s.npy' % (basic_cnf.work_dir, comp_name))
                    
                    
                    method_string = [os.path.split(f)[-1][4:-5] for f in result_catalogs 
                                     if float((os.path.split(f)[-1][4:-5]).split('_')[-2]) == freeparam
                                     and float((os.path.split(f)[-1][4:-5]).split('_')[-1]) == minpts
                                     and (os.path.split(f)[-1][4:-5]).split('_')[-3] == comp_name][0]
                    try:
                        result_catalog = model.load_events(os.path.join('%s/results/' % basic_cnf.work_dir, 'cat_' + method_string + '.yaml'))
                    except:
                        logger.error('ERROR: Clustering results for chosen %s and minpts for wf_cl_snuffle not found. ' % param_name + 
                            ' Clustering for chosen parameters must be computed first: %s, %s' % (freeparam,minpts))
                        error = True
                        continue
                    try:
                        repr_file = os.path.join('%s/results/' % basic_cnf.work_dir, 
                                        'representative_events_' + method_string + '.yaml')
                        representative_events = model.load_events(repr_file, format='yaml')
                    except FileNotFoundError:
                        logger.error('ERROR: Representative event file for waveform snuffling not found: %s' % repr_file)
                        error = True
                        continue

                    cl.wf_cluster_snuffle(result_catalog, subset_stations_and_indices, 
                                   basic_cnf.waveform_dir, arrivals_array, cc_cnf, method_string,
                                   representative_events, cl_cnf, basic_cnf.work_dir)

        #i_ph = 1
        #phase_name = get_phasename(cc_cnf, netsim_cnf, i_ph)
        for f in result_catalogs:
            result_catalog = model.load_events(f)
            if len([ev for ev in result_catalog if ev.extras['cluster_number'] != -1]) == 0:
                continue
            method_string = os.path.split(f)[-1][4:-5]
            comp_name = method_string.split('_')[-3]

            if cl_cnf.plot_map:
                if cc_cnf.coordinate_system == 'spherical':
                    if cl_cnf.map_plotlib in ['gmt','GMT']:
                        cl.map_plot(result_catalog, method_string, cc_cnf=cc_cnf, cl_cnf=cl_cnf, work_dir=basic_cnf.work_dir, plformat=plformat)
                    elif cl_cnf.map_plotlib in ['matplotlib','cartopy']:
                        fig_carto,ax_carto = cl.map_plot_carto(result_catalog, method_string, cc_cnf=cc_cnf, cl_cnf=cl_cnf, work_dir=basic_cnf.work_dir, plformat=plformat)
                elif cc_cnf.coordinate_system in ['cartesian','plane','local']:
                    fig_carto,ax_carto = cl.map_plot_carto_local(result_catalog, method_string, cc_cnf=cc_cnf, cl_cnf=cl_cnf, work_dir=basic_cnf.work_dir, plformat=plformat)

            cl.plot_cluster_timing(result_catalog, method_string, basic_cnf.work_dir, cl_cnf,plformat) # vielleicht noch spaeter?! final plots...

        if cl_cnf.plot_hist:
            cl.nev_cluster_hist(result_catalogs, basic_cnf.work_dir, plformat)

        if len(result_catalogs) > 1:
            cl.calc_adjusted_rand(result_catalogs, basic_cnf.work_dir, plformat)

        if cl_cnf.wf_plot:
            stpath = os.path.join('%s/used_stations.yaml' % basic_cnf.work_dir)
            logger.info(stpath)
            stats = model.load_stations(stpath)

            for cluster_params in cl_cnf.wf_plot:
                freeparam,minpts,param_name = cl.get_cluster_params(cluster_params,cl_cnf)

                logger.info('Preparing waveform plot(s) for %s %s and minpts %s.' % (param_name,freeparam, minpts))

                if (not cl_cnf.wf_plot_stats) or cl_cnf.wf_plot_stats == []:
                    subset_stations_and_indices = [(i_st, st) for i_st, st in enumerate(stats)]
                else:
                    subset_stations_and_indices = [(i_st, st) for i_st, st in enumerate(stats)
                                                    if '%s.%s' % (st.network, st.station) in cl_cnf.wf_plot_stats]
                
                arrivals_array = num.load('%s/precalc_arrays/arrivals_array.npy' % basic_cnf.work_dir)
                #try:
                #    tshifts_array = num.load('./precalc_arrays/cctshifts_array.npy')
                #except:
                #    tshifts_array = None
                method_string = ''

                for i_comp in range(n_comps):
                    #phase_name = get_phasename(cc_cnf, netsim_cnf, i_ph)
                    if n_comps == 1 and netsim_cnf.combine_components:
                        comp_name = 'comb'
                    else:
                        comp_name = cc_cnf.components[i_comp]
                    dists = num.load('%s/precalc_arrays/dists_%s.npy' % (basic_cnf.work_dir, comp_name))
                    try:
                        method_string = [os.path.split(f)[-1][4:-5] for f in result_catalogs 
                                         if float((os.path.split(f)[-1][4:-5]).split('_')[-2]) == freeparam
                                         and float((os.path.split(f)[-1][4:-5]).split('_')[-1]) == minpts
                                         and (os.path.split(f)[-1][4:-5]).split('_')[-3] == comp_name][0]
                        logger.info(method_string)
                        logger.info(os.path.join('%s/results/' % basic_cnf.work_dir, 'cat_' + method_string + '.yaml'))
                        result_catalog = model.load_events(os.path.join('%s/results/' % basic_cnf.work_dir, 'cat_' + method_string + '.yaml'))
                    except:
                        logger.error('ERROR: Clustering results for chosen %s and minpts for wf_plot not found. ' % param_name + 
                            ' Clustering for chosen parameters must be computed first: %s, %s' % (freeparam,minpts))
                        error = True
                        continue
                    try:
                        repr_file = os.path.join('%s/results/' % basic_cnf.work_dir, 
                                        'representative_events_' + method_string + '.yaml')
                        representative_events = model.load_events(repr_file)
                    except:
                        logger.error('ERROR: Representative event file not found: ' % repr_file)
                        error = True
                        continue                    
                    #representative_events, max_sil_vals = cl.get_repr_ev(result_catalog, dists, method_string, basic_cnf.work_dir)


                    cl.wf_cluster_plot(result_catalog, subset_stations_and_indices, 
                                   basic_cnf.waveform_dir, arrivals_array, cc_cnf, method_string,
                                   representative_events, cl_cnf, basic_cnf.work_dir, plformat)

        logger.info('Finished result plots.')
        if error:
            logger.info('WARNING: !!! Plotting error occurred. See above messages for more information.')


    if args.merge_freq_results:
        '''
        input: list of config files with different frequency ranges

        output: save harmonized catalogs
        '''

        result_catalogs = []
        methodstrings =[]

        basic_cnf, cc_cnf, netsim_cnf, cl_cnf, mergecat_cnf = clusty_config.load(filename=args.config).settings

        faults_path = cl_cnf.map_faults_path
        work_dir = basic_cnf.work_dir

        cat = model.load_events(basic_cnf.catalog_file)

        eps_minpts_tuple_list = list(zip(mergecat_cnf.pref_eps,mergecat_cnf.pref_minpts))

        for i_cat, cat_config_file in enumerate(mergecat_cnf.cat_config_list):
            freq_dir_name = os.path.split(cat_config_file)[0]

            basic_cnf_temp, cc_cnf_temp, netsim_cnf_temp, cl_cnf_temp = clusty_config.load(filename=cat_config_file).settings
            os.makedirs('%s/input/%s' % (work_dir,freq_dir_name), exist_ok=True)
            copyfile(cat_config_file, '%s/input/%s' % (work_dir, cat_config_file))

            logger.info('adding %s/input/%s' % (work_dir, cat_config_file))

            n_comps = get_ncomp(netsim_cnf_temp, cc_cnf_temp)

            for i_comp in range(n_comps):
                if n_comps == 1 and netsim_cnf_temp.combine_components:
                    comp_name = 'comb'
                else:
                    comp_name = cc_cnf_temp.components[i_comp]


                dists = num.load('%s/precalc_arrays/dists_%s.npy' % (freq_dir_name,comp_name))
                num.fill_diagonal(dists, 0.0)
                no_dist_evs = cl.get_list_event_no_dist(dists)

                logger.info('Starting clustering for final results')

                if cl_cnf_temp.method == 'dbscan':
                    eps, min_pts = eps_minpts_tuple_list[i_cat]
                    cluster_labels_list = cl.cluster_dbscan(eps_minpts_tuples=[eps_minpts_tuple_list[i_cat]],
                                                        dist_array=dists)
                else:
                    logger.error('Clustering method not implemented. Please use ```dbscan```.')

                method_string = '%s_%s_%s_%.3f_%s' % (netsim_cnf_temp.method_similarity_computation,
                                  cl_cnf_temp.method, comp_name, eps, min_pts)
                os.makedirs('%s/input/%s' % (work_dir,freq_dir_name), exist_ok=True)

                outfile = '%s/input/%s/cat_%s.yaml' % (work_dir, freq_dir_name,method_string)
                result_catalogs.append(outfile)
                cat = cl.catalog_output(cat, cluster_labels_list[0], outfile, no_dist_evs)

        logger.info('Starting harmonization.')

        if len(result_catalogs) > 1:
            method = '%s_%s_%s_' % (netsim_cnf.method_similarity_computation,
                              cl_cnf.method, comp_name)

            cl.harmonize_cluster_labels(file_list=result_catalogs, work_dir=basic_cnf.work_dir, single_freq=False)
            # only pass work_dir since nothing else needed from basic_cnf
            cl.sankey_multi(result_catalogs, basic_cnf.work_dir, fn='%s/sankey.html' % work_dir,
                            single_freq=False, plot_labels=cl_cnf.sankey_labels)

            #what about a new silhouette plot
            #cl.silhouette_plot(dists, cat, method_string, eps, min_pts, cc_cnf)

            # probably omitted
            #cl.calc_adjusted_rand(result_catalogs, basic_cnf.work_dir)

            logger.info('Start merging catalogs of different frequency bands.')

            cat_merged = merge_freqs.merge_harmonized_catalogs(result_catalogs)

            os.makedirs('%s/results' % work_dir, exist_ok=True)
            model.dump_events(cat_merged, '%s/results/cat_merged_%s.yaml' % (work_dir,method), 
                              format='yaml')

            fn = '%s/results/cluster_map_%s.%s' % (work_dir,method,plformat)
            if cl_cnf.plot_map:
                if cc_cnf.coordinate_system == 'spherical':
                    if cl_cnf.map_plotlib in ['gmt','GMT']:
                        cl.map_plot_freqmerge(cat_merged, method_string, cc_cnf=False, cl_cnf=cl_cnf, work_dir=basic_cnf.work_dir, plformat=plformat)
                    elif cl_cnf.map_plotlib in ['matplotlib','cartopy']:
                        logger.info('Multiple catalogs not implemented in matplotlib/cartopy')
                        #fig_carto,ax_carto = cl.map_plot_carto(cat_merged, method_string, cc_cnf=False, cl_cnf=cl_cnf, work_dir=basic_cnf.work_dir)
                elif cc_cnf.coordinate_system in ['cartesian','plane','local']:
                    logger.info('Multiple catalogs not implemented in matplotlib/cartopy')
                    #fig_carto,ax_carto = cl.map_plot_carto_local(cat_merged, method_string, cc_cnf=False, cl_cnf=cl_cnf, work_dir=basic_cnf.work_dir)

    if args.export_stacked_traces:
        logger.info('Preparing waveform stacking, reading prepared arrays.')

        stpath = os.path.join('%s/used_stations.yaml' % basic_cnf.work_dir)
        logger.info(stpath)
        stats = model.load_stations(stpath)


        eps_minpts_tuple_list = list(itertools.product(set(cl_cnf.dbscan_eps), 
                                                      set(cl_cnf.dbscan_min_pts)))
        n_comps = get_ncomp(netsim_cnf, cc_cnf)
        methodstrings = []
        for i_comp in range(n_comps):
            if n_comps == 1 and netsim_cnf.combine_components:
                comp_name = 'comb'
            else:
                comp_name = cc_cnf.components[i_comp]

            for eps, min_pts in eps_minpts_tuple_list:
                method_string = '%s_%s_%s_%.3f_%s' % (netsim_cnf.method_similarity_computation,
                                          cl_cnf.method, comp_name, eps, min_pts)
                methodstrings.append(method_string)

        result_catalogs = [os.path.join('%s/results/' % basic_cnf.work_dir, 'cat_' + ms+ '.yaml') for ms in methodstrings]
        arrivals_array = num.load('%s/precalc_arrays/arrivals_array.npy' % basic_cnf.work_dir)
        #tshifts_array = num.load('./precalc_arrays/cctshifts_array.npy')
        #coef_array = num.load('./precalc_arrays/cccoef_array.npy')

        representative_event_catalogs = [os.path.join('%s/results/' % basic_cnf.work_dir, 
            'representative_events_' + ms+ '.yaml') for ms in methodstrings]

        if not representative_event_catalogs:
            logger.error('Export of aligned and stacked traces requires computation of representative \
                events during clustering step. Please rerun --cluster after adding argument\
                ```repr_ev: True``` in the clustering_settings of the config file.')

        logger.info('Starting waveform stacking.')

        #logger.info('STA start: %s' % stats[0].station)
        #sys.exit()

        for f,re in zip(result_catalogs,representative_event_catalogs):
            logger.debug(f)
            logger.debug(re)
            result_catalog = model.load_events(f)
            representative_events = model.load_events(re)
            method_string = os.path.split(f)[-1][4:-5]

            cl.export_stacked_traces(result_catalog, basic_cnf.waveform_dir, 
                                     arrivals_array, #coef_array,
                                     #tshifts_array, 
                                     stats,
                                     cl_cnf.prepare_for_grond,
                                     cl_cnf.debug_stacking,
                                     cc_cnf.components,
                                     method_string, cc_cnf, cl_cnf, basic_cnf.work_dir,
                                     cl_cnf.preprocess_export_stacked_traces,
                                     representative_events)

    logger.info('Clusty run completed.')

if __name__ == '__main__':
    main()
