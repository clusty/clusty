import numpy as num
import logging, sys
import glob, os
import matplotlib.pyplot as plt
import matplotlib as mpl
from sklearn.cluster import DBSCAN, OPTICS
from .cc import small_imshow_plot, comp_snr
from . import util_clusty
from pyrocko.guts import Object, String, List, Int, Tuple, Float
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pyrocko import util, pile, trace, io, model
from pyrocko.plot.automap import Map
from pyrocko import orthodrome as od
from pyrocko import moment_tensor as pmt
from pyrocko.guts import load
import datetime
from matplotlib.ticker import MultipleLocator

import plotly.graph_objects as go
import plotly
import networkx as nx
from sklearn.metrics import silhouette_samples, silhouette_score
from sklearn.metrics import adjusted_rand_score

import matplotlib.ticker as ticker
import string
import math
import itertools
import warnings

from pyrocko.plot import beachball

if sys.version_info.minor <= 5:
    err_class = ImportError
else:
    err_class = ModuleNotFoundError

try:
    import cartopy
    import cartopy.crs as ccrs
    from cartopy.mpl.ticker import LongitudeFormatter, LatitudeFormatter
    from shapely.errors import ShapelyDeprecationWarning
    warnings.filterwarnings("ignore", category=ShapelyDeprecationWarning)

except err_class:
    pass

np = num

mpl.use('agg')

logger = logging.getLogger('__name__')

def get_mag_scale_method(cl_cnf):
    if cl_cnf.mag_scale_log:
        log_tuple = cl_cnf.mag_scale_log
        scale_meth = 'manual'
    else:
        log_tuple = None
        scale_meth = 'auto'

    return log_tuple, scale_meth

def nev_cluster_hist(result_catalogs, work_dir, plformat):
    '''
    plot histogram of clusters
    :param result_catalog: List of catalog files
    :param method_string: String describing method. Hardcoded position
                          of eps and minpts values. For legend and outfilename.
    '''
    bluecolors = util_clusty.get_colormap(cmap_name='bluecolors')

    fig, ax = plt.subplots(figsize=(3.486*2,(3.486*2)/1.618))
    barwidth = 1/(len(result_catalogs)+1)
    positions = [barwidth*i for i in range(-len(result_catalogs)//2, len(result_catalogs)//2)]
    max_n_cluster = 0

    for i_f, f in enumerate(result_catalogs):
        catalog = model.load_events(f)
        ff = f.split('cat_')[1]
        method_string = ff[:-5]
        eps = method_string[-6:-2]
        min_pts = method_string[-1]
        result_catalog = model.load_events(f)

        cluster_list = [int(ev.extras['cluster_number']) for ev in catalog]
        cluster_list = [ncl for ncl in cluster_list if ncl !=-1]
        n_ev_total = len(cluster_list) 
        cl_number_set = set(list(cluster_list))
        n_cluster = len(cl_number_set)
        n_ev_cl = [(ncl, cluster_list.count(ncl)) for ncl in cl_number_set]
        n_ev_sorted = sorted([x[1] for x in n_ev_cl], reverse=True)

        r = [i + positions[i_f] for i in range(n_cluster)]
        bars = n_ev_sorted
        try:
            c = bluecolors[i_f]
        except IndexError:
            c = 'grey'
        ax.bar(r, bars, 
                label='%s, %s'% (eps, min_pts), #alpha=0.5,
                width=barwidth, edgecolor='white',
                color=c)

        if n_cluster > max_n_cluster:
            max_n_cluster = n_cluster

    ax.legend(prop={'size': 8})
    ax.set_title('Cluster size histogram', fontsize=10)
    ax.set_ylabel('Number of events in cluster', fontsize=10)
    ax.set_xlabel('Cluster number', fontsize=10)
    ax.xaxis.set_major_locator(mpl.ticker.MultipleLocator(1))
    ax.set_xlim(-1, max_n_cluster+1)
    ax.set_yscale('log')
    ax.tick_params(axis='both', which='major', labelsize=8)
    fig.savefig('%s/results/nev_cluster_hist_%s.%s' % (work_dir, method_string[:-7],plformat))
    plt.close()


def get_distance_matrix(net_sim_array, comp_name, work_dir):
    '''
    compute distance matrix needed for clustering

    '''
    net_sim_array[num.isnan(net_sim_array)] = 0
    net_sim_array = net_sim_array.T + net_sim_array
    net_sim_array[num.where(net_sim_array == 0.0)] = num.nan
    dists = 1-net_sim_array
    num.fill_diagonal(dists, 0.0)
    dists[num.isnan(dists)] = 1.
    dists = num.where(abs(dists) < 0.0001, 0., dists)  # numerical artefacts in distance computation...

    num.save(arr=dists, file='%s/precalc_arrays/dists_%s.npy' % (work_dir, comp_name))

    return dists


def get_list_event_no_dist(dists):
    '''
    get indices of events that do not have any entry in the distance matrix.
    these are events without data or which do not pass the thresholds.
    --> these will get an entry in result catalog and a separate plotting
    symbol in map plot.
    '''

    clustered_k = 0
    list_ev_no_dist = []
    n_ev = dists.shape[0]

    for i_ev in range(n_ev):
        ddist = dists[i_ev][num.argwhere(dists[i_ev] < 1.)]

        if num.sum(ddist) < 0.001:
            list_ev_no_dist.append(False)
        else:
            list_ev_no_dist.append(True)
            clustered_k += 1

    unclustered_k = n_ev - clustered_k
    logger.info('Events entering into clustering stage: %i (%i excluded before)' % (clustered_k, unclustered_k))

    return(list_ev_no_dist)

def get_cluster_params(cluster_params,cl_cnf, name_only=False):

    if cl_cnf.method in  ['dbscan','DBSCAN']:
        param1 = cluster_params[0]
        pts_min = cluster_params[1]
        param_name = 'Eps'

    elif cl_cnf.method in  ['optics','OPTICS']:
        param1 = cluster_params[0]
        pts_min = cluster_params[1]
        param_name = 'Xi'

    if name_only:
        return param_name
    else:
        return param1, pts_min, param_name

def get_cluster_params_cnf(cl_cnf, name_only=False):

    if cl_cnf.method in ['optics','OPTICS']:
        min_pts_list = cl_cnf.optics_min_pts
        param_list = cl_cnf.optics_xi
        param_name = 'Xi'

    elif cl_cnf.method in ['dbscan','DBSCAN']:
        min_pts_list = cl_cnf.dbscan_min_pts
        param_list = cl_cnf.dbscan_eps
        param_name = 'Eps'

    if name_only:
        return param_name
    else:
        return  param_list,min_pts_list,param_name

def cluster(dists, comp_name,cl_cnf,netsim_cnf,**kwargs):

    if cl_cnf.method == 'dbscan':
        if 'freeparam_list' in kwargs.keys():
            eps_minpts_tuple_list = list(itertools.product(set(kwargs['freeparam_list']),
                                                    set(cl_cnf.dbscan_min_pts)))
        else:
            eps_minpts_tuple_list = list(itertools.product(set(cl_cnf.dbscan_eps),
                                                    set(cl_cnf.dbscan_min_pts)))
        eps_minpts_tuple_list.sort(key=lambda x: x[0])

        cluster_labels_list = cluster_dbscan(eps_minpts_tuples=eps_minpts_tuple_list,
                                                        dist_array=dists)

        methodstrings = []
        for eps_minpts_tuple, cluster_labels in zip(eps_minpts_tuple_list,
                                                    cluster_labels_list):
            eps, min_pts = eps_minpts_tuple

            method_string = '%s_%s_%s_%.3f_%s' % (netsim_cnf.method_similarity_computation,
                                      cl_cnf.method, comp_name, eps, min_pts)
            methodstrings.append(method_string)

    elif cl_cnf.method == 'optics':
        if 'freeparam_list' in kwargs.keys():
            xi_minpts_tuple_list = list(itertools.product(set(kwargs['freeparam_list']),
                                                          set(cl_cnf.optics_min_pts)))
        else:
            xi_minpts_tuple_list = list(itertools.product(set(cl_cnf.optics_xi),
                                                          set(cl_cnf.optics_min_pts)))
        xi_minpts_tuple_list.sort(key=lambda x: x[0])

        if 'reach_plot' in kwargs.keys():
            reach_plot = kwargs['reach_plot']
        else:
            reach_plot = False

        cluster_labels_list = cluster_optics(xi_minpts_tuples=xi_minpts_tuple_list,
                                                 dist_array=dists,reach_plot=reach_plot)
        methodstrings = []

        for xi_minpts_tuple, cluster_labels in zip(xi_minpts_tuple_list,
                                                    cluster_labels_list):

            xi, min_pts = xi_minpts_tuple

            method_string = '%s_%s_%s_%.3f_%s' % (netsim_cnf.method_similarity_computation,
                                      cl_cnf.method, comp_name, xi, min_pts)
            methodstrings.append(method_string)

    else:
        logger.error('Clustering method not implemented. Please use ```dbscan``` or ```optics```.')


    return cluster_labels_list, methodstrings



def cluster_dbscan(eps_minpts_tuples, dist_array):
    '''
    apply dbscan from scikit learn for clustering

    :param eps_list: list of allowed distances to belong to one cluster
    :param min_pts: list of minimum number of events that can set up one \
                        cluster, in same order as eps_list
    :param metrics: network similarity matrix

    :returns clustering: list of list containing integers as cluster indentifiers
    '''

    cluster_labels_list = []
    for eps, min_pts in eps_minpts_tuples: #zip(eps_list, min_pts_list):
        cluster_labels_list.append(DBSCAN(eps=eps, min_samples=min_pts,
                                 metric='precomputed').fit_predict(dist_array))
        logger.info('DBSCAN clustering with eps=%.2f and MinPts=%i finished' % (eps,min_pts))

    return cluster_labels_list


def cluster_optics(xi_minpts_tuples, dist_array,reach_plot=False, n_jobs=4):
    '''
    apply optics from scikit learn for clustering

    :param min_pts: list of minimum number of events that can set up one \
                        cluster, in same order as eps_list
    :param metrics: network similarity matrix

    :returns clustering: list of list containing integers as cluster indentifiers
    '''

    cluster_labels_list = []
    for xi, min_pts in xi_minpts_tuples: #zip(eps_list, min_pts_list):
        fit_result = OPTICS(min_samples=min_pts,metric='precomputed', cluster_method = 'xi',
                            xi=xi,n_jobs=n_jobs,max_eps=0.4).fit(dist_array)
        logger.info("OPTICS clustering with Xi=%.3f and MinPts=%i finished" % (xi,min_pts))

        #plotting
        n_cluster = len(set(fit_result.labels_))-1
        n_clustered = sum(num.array(fit_result.labels_) != -1)

        logger.info("--> assigned %i evens to %i clusters" % (n_clustered,n_cluster))

        labels = fit_result.labels_
        cluster_labels_list.append(labels)

        if reach_plot:
            plot_reach_optics(fit_result.reachability_,labels,fit_result.ordering_,xi,min_pts)

    return cluster_labels_list

def plot_reach_optics(reachbility,labels,ordering,xi,min_pts,plformat='pdf'):

    fig,ax = plt.subplots()

    space = num.arange(len(labels))

    reachability = reachbility[ordering]
    labels = labels[ordering]

    for i, clust in enumerate(sorted(set((labels)))):
        color = util_clusty.cluster_to_color(clust)

        order = space[labels == clust]
        reach = reachability[labels == clust]
        ax.bar(order, reach, facecolor=color, width=1)
        ax.scatter(order, reach, c=color, s=1)

    ax.set_ylabel('Reachability (epsilon distance)')
    ax.set_title('Reachability Plot    Xi: %.3f  MinPts: %i' %(xi, min_pts))

    fig.savefig('./results/reachability_%.3f_%i.%s' % (xi,min_pts,plformat))
    plt.close()



def catalog_output(catalog, cluster_labels, outfile, no_dist_evs=False):
    cluster_cat = []
    if not no_dist_evs:
        #try:
        no_dist_evs = [ev.extras['clustered'] for ev in catalog]

    for ev, cluster_label, bool_dist in zip(catalog, cluster_labels, 
                                            no_dist_evs):

        ev.extras['cluster_number'] = cluster_label
        ev.extras['color'] = util_clusty.cluster_to_color(cluster_label)
        ev.extras['clustered'] = bool_dist

        cluster_cat.append(ev)

    model.dump_events(cluster_cat, outfile, format='yaml')

    return cluster_cat


#def mag_scale_log(mag, log_tuple, m_max):
#    #return 2**mag/2
#    #return 1.5**mag*1.2
#    return 1.75**mag/1.8 #*5 #castor (max 4)
#    s_max = 20
#    factor = s_max/(log_tuple[0]**m_max/log_tuple[1])
#    return (log_tuple[0]**mag/log_tuple[1])*factor

# def mag_scale_log(mag, log_tuple, m_max, m_min, method='manual'):
#     if method == 'manual':
#         #s_max = 24
#         #factor = s_max/(log_factor**m_max)
#         #markersize = log_factor**mag*factor
#         markersize = (log_tuple[0]**mag)*log_tuple[1]

#     elif method == 'auto':
#         log_tuple = get_mag_tuple_auto(m_min,m_max)
#         markersize = (log_tuple[0]**mag)*log_tuple[1]

#     return markersize

def mag_scale_log(mag, log_tuple, m_max, m_min, plotlib, method='manual'):

    if method == 'manual':
        #s_max = 24
        #factor = s_max/(log_factor**m_max)
        #markersize = log_factor**mag*factor
        markersize = (log_tuple[0]**mag)*log_tuple[1]

    elif method == 'auto':
        log_tuple = get_mag_tuple_auto(m_min,m_max,plotlib)
        markersize = (log_tuple[0]**mag)*log_tuple[1]

    return markersize


def get_mag_tuple_auto(mag_min,mag_max,plotlib):
    if plotlib in ['GMT','gmt','plotly']:
        markersize_max = 24
        markersize_min = 2
    elif plotlib in ['matplotlib', 'cartopy','plt']:
        markersize_max = 24**2
        markersize_min = 2**2
    else:
        logger.warning('ERROR: plotting library unknown, please chose gmt or matplotlib and make sure that both are installed properly')
        sys.exit()

    if mag_min != mag_max:
        a = (markersize_max/markersize_min)**(1/(mag_max-mag_min))
        scale_fac = markersize_min/a**mag_min
    else:
        a = 1
        scale_fac = np.sqrt((markersize_max+markersize_max)/2)
    return (a, scale_fac)



def get_ncols_legend(n_cluster):

    leg_cols = n_cluster//20 +1

    return leg_cols

def get_scalebar_length(radius):
    radius /= 1000
    base = num.floor(num.log10(radius))
    rest = num.log10(radius) - base

    if rest <= 0.3:
        base -= 1

    scalebar_length = int(num.ceil((radius*0.2)/10.0**base))*10**base

    return scalebar_length

def scale_bar(ax, ortho, length=None,
                location=(0.8, 0.1), linewidth=3, fontsize=20):
    """

    #
    # replace? posted by Siyh at https://stackoverflow.com/questions/32333870/how-can-i-show-a-km-ruler-on-a-cartopy-matplotlib-plot/50674451#50674451
    #

    ax is the axes to draw the scalebar on.
    length is the length of the scalebar in km.
    location is center of the scalebar in axis coordinates.
    (ie. 0.5 is the middle of the plot)
    linewidth is the thickness of the scalebar.
    """
    #Get the limits of the axis in lat long

    llx0, llx1, lly0, lly1 = ax.get_extent(ortho)
    #Make tmc horizontally centred on the middle of the map,
    #vertically at scale bar location
    sbllx = (llx1 + llx0) / 2
    sblly = lly0 + (lly1 - lly0) * location[1]
    tmc = ccrs.LambertConformal(sbllx, sblly)
    #Get the extent of the plotted area in coordinates in metres
    x0, x1, y0, y1 = ax.get_extent(tmc)
    #Turn the specified scalebar location into coordinates in metres
    sbx = x0 + (x1 - x0) * location[0]
    sby = y0 + (y1 - y0) * location[1]

    #Calculate a scale bar length if none has been given
    #(Theres probably a more pythonic way of rounding the number but this works)
    if not length:
        length = (x1 - x0) / 5000 #in km
        ndim = int(num.floor(num.log10(length))) #number of digits in number
        length = round(length, -ndim) #round to 1sf
        #Returns numbers starting with the list
        def scale_number(x):
            if str(x)[0] in ['1', '2', '5']: return int(x)
            else: return scale_number(x - 10 ** ndim)
        length = scale_number(length)
    #Generate the x coordinate for the ends of the scalebar
    bar_xs = [sbx - length * 500, sbx + length * 500]
    #Plot the scalebar
    ax.plot(bar_xs, [sby, sby], transform=tmc, color='k', linewidth=linewidth)
    #Plot the scalebar label
    ax.text(sbx, sby, '%.1f km' % length, transform=tmc,
            horizontalalignment='center', verticalalignment='bottom', fontsize=fontsize)

def map_plot(catalog, method, cc_cnf, cl_cnf, work_dir, fn=False, mark_nodata=False,plformat='pdf'):
    faults = cl_cnf.map_faults_path
    gmtconf = dict(
                   MAP_TICK_PEN_PRIMARY='1.3p',
                   MAP_TICK_PEN_SECONDARY='1.3p',
                   MAP_TICK_LENGTH_PRIMARY='0.2c',
                   MAP_TICK_LENGTH_SECONDARY='0.6c',
                   FONT_ANNOT_PRIMARY='20p,1,black',
                   FONT_LABEL='20p,1,black',
                   MAP_FRAME_TYPE='fancy',
                   FORMAT_GEO_MAP='D',
                   PS_PAGE_ORIENTATION='portrait',
                   MAP_GRID_PEN_PRIMARY='thinnest,0/50/0',
                   #MAP_ANNOT_OBLIQUE='8',
                   PROJ_LENGTH_UNIT='p')

    ev_nomt = []
    ev_mt = []
    lats_nocl = []
    lons_nocl = []
    s_nocl = []
    ev_nocl_notcl = []
    ev_nocl_butmt = []

    mags = [ev.magnitude if ev.magnitude else num.nan for ev in catalog]
    mag_min = num.nanmin(mags)
    mag_max = num.nanmax(mags)


    if num.isnan(mag_min):
        mag_min = 2
        no_mags = True
    else:
        no_mags = False

    if num.isnan(mag_max):
        mag_max = 2

    mag_substituted = False
    for ev in catalog:
        if ev.magnitude == None:
            ev.magnitude = mag_min/2
            mag_substituted = True

    mags = [ev.magnitude for ev in catalog]

    if no_mags:
        logger.warning('WARNING: Catalog without magnitudes. Magnitudes set to 1 for plotting.')
    elif mag_substituted:
        logger.warning('WARNING: Not all events in catalog have magnitudes.Missing magnitudes were \
                        set to min(mag)/2 for plotting. %s %s' % (mag_min, mag_max))


    log_tuple,scale_meth = get_mag_scale_method(cl_cnf)

    for ev in catalog:
        if int(ev.extras['cluster_number']) != -1:
            if ev.moment_tensor:
                ev_mt.append(ev)
            else:
                ev_nomt.append(ev)
        else:
            if ev.moment_tensor:
                ev_nocl_butmt.append(ev)
            else:
                if not mark_nodata and ev.extras['clustered'] is True:
                    lats_nocl.append(ev.lat)
                    lons_nocl.append(ev.lon)
                    s_nocl.append(mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth))
                else:
                    ev_nocl_notcl.append(ev)
    
    # map dimensions:
    lats_all = sorted([ev.lat for ev in catalog])  # if ev.extras['cluster_number'] != -1]
    lons_all = sorted([ev.lon for ev in catalog])   # if ev.extras['cluster_number'] != -1]
    # n_trim = int(len(lats_all) * 0.02)
    # lats_all = lats_all[n_trim: -n_trim]
    # lons_all = lons_all[n_trim: -n_trim]

    lat_m = num.mean(lats_all)
    lon_m = num.mean(lons_all)

    corners = [od.Loc(num.min(lats_all), num.min(lons_all)),
               od.Loc(num.max(lats_all), num.max(lons_all))]

    dist1 = od.distance_accurate50m(od.Loc(lat_m, lon_m), corners[0])
    dist2 = od.distance_accurate50m(od.Loc(lat_m, lon_m), corners[1])
    radius = max(dist1, dist2)*cl_cnf.focus_map_plot
    
    if not mark_nodata:
        eps = method.split('_')[-2]
        minpts = method.split('_')[-1]

    if cc_cnf:
        freqs = '%.2f - %.2f Hz' % (cc_cnf.bp[1],cc_cnf.bp[2])
        title='Bandpass: %s     MinPts: %i      Eps: %.2f' % (
                    freqs, int(minpts), float(eps))
    else:
        title='Final result (merged)'

    m = Map(
        lat=lat_m,
        lon=lon_m,
        radius=radius,
        width=30,
        height=30,
        show_grid=False,
        show_topo=False,
        show_scale=True,
        color_dry=(238,236,230),
        illuminate=True,
        illuminate_factor_ocean=0.15,
        # illuminate_factor_land = 0.2,
        show_rivers=True,
        show_plates=False,
        gmt_config=gmtconf,
        comment=title)

    if faults:
        for f in faults:
            m.gmt.psxy(f, G='black', S='f', *m.jxyr)

    # events that were sorted out due to thresholds or no data:
    if ev_nocl_notcl:
        lat_nocl_data = []
        lon_nocl_data = []
        s_nocl_data = []
        ev_nodatathresh = []
        for ev in ev_nocl_notcl:
            if len(ev.extras) == 2:
                lat_nocl_data.append(ev.lat)
                lon_nocl_data.append(ev.lon)
                s_nocl_data.append(mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth))
            else:
                ev_nodatathresh.append(ev)

        if mark_nodata:
            # this is old
            rows_nodatasnr = [(ev.lon, ev.lat, mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth)) for ev in ev_nodatathresh if ev.extras['note'] == 'nodatasnr']
            rows_thresh = [(ev.lon, ev.lat, mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth)) for ev in ev_nodatathresh if ev.extras['note'] == 'thresh']
            m.gmt.psxy(in_rows=rows_nodatasnr, S='c', *m.jxyr)
            m.gmt.psxy(in_rows=rows_thresh, S='d', *m.jxyr)
            m.gmt.psxy(in_columns=(lon_nocl_data, lat_nocl_data, s_nocl_data), S='c', G='grey', *m.jxyr)
        else:
            rows = [(ev.lon, ev.lat, mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth)) for ev in ev_nodatathresh if ev.extras['clustered'] is False]
            m.gmt.psxy(in_rows=rows, S='c', W='0.5p,grey', *m.jxyr)

    # events without MT and without cluster number but entered DBSCAN
    if not mark_nodata:
        m.gmt.psxy(in_columns=(lons_nocl, lats_nocl, s_nocl), S='c', G='grey', *m.jxyr)

    # events without cluster number but with MT
    for ev in ev_nocl_butmt:
        g_col = 'grey'
        mmt = ev.moment_tensor
        devi = mmt.deviatoric()
        mt = devi.m_up_south_east()
        mt = mt/ mmt.scalar_moment() \
                * pmt.magnitude_to_moment(ev.magnitude)
        m6 = pmt.to6(mt)
        data = (ev.lon, ev.lat, 10) + tuple(m6) + (1,0,0)
        s = 'd%sp' % mag_scale_log(ev.magnitude, log_tuple, mag_max,mag_min, plotlib=cl_cnf.map_plotlib, method=scale_meth)

        if not cc_cnf and not mark_nodata:
            if ev.extras['origins'][0] == True:
                m.gmt.psmeca(
                        in_rows=[data],
                        G=g_col, 
                        S=s,
                        W='0.05p,%s' %'grey',
                        M=True,
                        *m.jxyr)
        else:
            if len(ev.extras) == 2:
                m.gmt.psmeca(
                        in_rows=[data],
                        G=g_col, 
                        S=s,
                        W='0.05p,%s' %'grey',
                        M=True,
                        *m.jxyr)

            elif len(ev.extras) == 3:
                if ev.extras['clustered'] is True:
                    ggcol = 'grey'
                else:
                    ggcol = 'black'
                m.gmt.psmeca(
                        in_rows=[data],
                        G='lightgrey', 
                        S=s,
                        W='0.01p,%s' % ggcol,
                        M=True,
                        *m.jxyr)

    # events without MT but in a cluster
    cluster_color_dict = {}
    if not cc_cnf and not mark_nodata:
        origins = len(ev_nomt[0].extras['origins'])
        symbol_list = ['c', 'a', 'd', 'i', 't', 'g', 'n']
        clusters = list(set([ev.extras['cluster_number'] for ev in ev_nomt]))

        for o in range(origins):
            symbol = symbol_list[o]
            for cl in clusters:
                pl_ev = [ev for ev in ev_nomt
                         if ev.extras['cluster_number'] == cl and
                         ev.extras['origins'][o] == True]
                if pl_ev:
                    g_col = util_clusty.color2rgb(pl_ev[0].extras['color'])
                    cluster_color_dict[cl]=g_col
                    lonsnomt = [ev.lon for ev in pl_ev]
                    latsnomt = [ev.lat for ev in pl_ev]
                    sizenomt = [mag_scale_log(ev.magnitude, log_tuple, mag_max, mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth) for ev in pl_ev]
                    m.gmt.psxy(in_columns=(lonsnomt, latsnomt, sizenomt),
                               G=g_col, S=symbol, W='0.5p,%s' % g_col,
                               *m.jxyr)                

    else:
        clusters = list(set([ev.extras['cluster_number'] for ev in ev_nomt]))

        for cl in clusters:
            pl_ev = [ev for ev in ev_nomt
                     if ev.extras['cluster_number'] == cl]
            g_col = util_clusty.color2rgb(pl_ev[0].extras['color'])
            lonsnomt = [ev.lon for ev in pl_ev]
            latsnomt = [ev.lat for ev in pl_ev]
            sizenomt = [mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth) for ev in pl_ev]
            m.gmt.psxy(in_columns=(lonsnomt, latsnomt, sizenomt),
                       G=g_col, S='c', W='0.5p,%s' % g_col,
                       *m.jxyr)
            cluster_color_dict[cl]=g_col

    # clustered events with a MT
    for ev in ev_mt:
        g_col = util_clusty.color2rgb(ev.extras['color'])
        mmt = ev.moment_tensor
        devi = mmt.deviatoric()
        mt = devi.m_up_south_east()
        mt = mt/ mmt.scalar_moment() \
                * pmt.magnitude_to_moment(ev.magnitude)
        m6 = pmt.to6(mt)
        data = (ev.lon, ev.lat, 10) + tuple(m6) + (1,0,0)
        s = 'd%sp' % mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth)
        m.gmt.psmeca(
                in_rows=[data],
                G=g_col, 
                S=s,
                W='0.05p,%s' %g_col,
                M=True,
                *m.jxyr)



    #print(mag_min, mag_max)
    lower = num.floor(mag_min)
    upper = num.ceil(mag_max)

    if upper-lower <= 2:
        step = 0.5
    else:
        step = 1

    if mag_min != mag_max:
        if upper-lower <= 2:
            mags_for_leg = num.array(num.arange(lower,num.round(mag_max,1), step))
            mags_for_leg = num.append(mags_for_leg,mag_max)
        else:
            mags_for_leg = num.array(num.arange(lower,upper,step))
    else:
        mags_for_leg = [1]

    for i_ma, ma in enumerate(mags_for_leg):
        if i_ma == 0:
            leg = 'S 0.1i c %sp black 1p 0.3i M %.1f\n' % (mag_scale_log(ma, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth),ma)
        else:
            leg = leg + 'S 0.1i c %sp black 1p 0.3i M %.1f\n' % (mag_scale_log(ma, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth),ma)


    leg = leg + 'D 0 1p\n'
    n_cluster = len(cluster_color_dict.keys())
    leg_cols = get_ncols_legend(n_cluster)
    leg = leg + 'N %i\n' % leg_cols

    mag_leg = (mag_min+mag_max)/2
    for key in sorted(cluster_color_dict.keys()):
        item = cluster_color_dict[key]
        leg = leg + 'S 0.1i c %sp %s - 0.3i %s\n' % (mag_scale_log(mag_leg, log_tuple, mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth), item, key)

    with open('leg.txt', 'w') as f:
        f.write(leg)

    m.gmt.pslegend(
        'leg.txt',
        D='x2.0c/2.0c+w3.5c',
        F='+gwhite+p')

    if not fn:
        fn = os.path.join(work_dir, 'results/cluster_map_%s.%s' % (method,plformat))

    m.save(fn)

    os.remove('leg.txt')

def map_plot_carto_local(catalog, method, cc_cnf, cl_cnf, work_dir, fn=False,plformat='pdf'):

    if cl_cnf.map_plotlib in ['gmt','GMT']:
        logger.warning('WARNING: map_plotlib set to GMT, but local maps are not implemented in GMT.\n\
                            --> Using cartopy/matplotlib instead')

    #forcing matplotlib in case GMT set for cl_cnf.map_plotlib
    plotlib = 'matplotlib'
    faults = cl_cnf.map_faults_path

    ev_nomt = []
    ev_mt = []
    norths_nocl = []
    easts_nocl = []
    s_nocl = []
    ev_nocl_notcl = []
    ev_nocl_butmt = []

    mags = [ev.magnitude if ev.magnitude else num.nan for ev in catalog]
    mag_min = num.nanmin(mags)
    mag_max = num.nanmax(mags)



    if num.isnan(mag_min):
        mag_min = 2
        no_mags = True
    else:
        no_mags = False

    if num.isnan(mag_max):
        mag_max = 2

    mag_substituted = False
    for ev in catalog:
        if ev.magnitude == None:
            ev.magnitude = mag_min/2
            mag_substituted = True

    mags = [ev.magnitude for ev in catalog]

    if no_mags:
        logger.warning('WARNING: Catalog without magnitudes. Magnitudes set to 1 for plotting.')
    elif mag_substituted:
        logger.warning('WARNING: Not all events in catalog have magnitudes.Missing magnitudes were \
                        set to min(mag)/2 for plotting. %s %s' % (mag_min, mag_max))


    log_tuple,scale_meth = get_mag_scale_method(cl_cnf)

    for ev in catalog:
        if int(ev.extras['cluster_number']) != -1:
            if ev.moment_tensor:
                ev_mt.append(ev)
            else:
                ev_nomt.append(ev)
        else:
            if ev.moment_tensor:
                ev_nocl_butmt.append(ev)
            else:
                if ev.extras['clustered'] is True:
                    norths_nocl.append(ev.north_shift)
                    easts_nocl.append(ev.east_shift)
                    s_nocl.append(mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=plotlib,method=scale_meth))
                else:
                    ev_nocl_notcl.append(ev)

    print('Total:', len(catalog))
    print('Not in cluster:', len(easts_nocl))
    print('Not clustered:', len(ev_nocl_notcl), ' (thereof with MT: %i)' %len(ev_nocl_butmt))
    print('In cluster:', len(ev_mt), '+', len(ev_nomt))

    # map dimensions:
    norths_all = [ev.north_shift for ev in catalog]
    easts_all = [ev.east_shift for ev in catalog]

    norths_all_sorted = sorted(norths_all)  # if ev.extras['cluster_number'] != -1]
    easts_all_sorted = sorted(easts_all)  # if ev.extras['cluster_number'] != -1]
    # n_trim = int(len(norths_all) * 0.02)
    # norths_all_sorted = norths_all_sorted[n_trim: -n_trim]
    # easts_all_sorted = easts_all_sorted[n_trim: -n_trim]

    north_m = num.mean(norths_all)
    east_m = num.mean(easts_all)

    min_north = num.min(norths_all_sorted)
    min_east = num.min(easts_all_sorted)
    max_north = num.max(norths_all_sorted)
    max_east = num.max(easts_all_sorted)

    extent = max(num.abs(max_north-min_north),num.abs(max_east-min_east))*0.5*cl_cnf.focus_map_plot

    param1 = method.split('_')[-2]
    pts_min = method.split('_')[-1]
    param_name = get_cluster_params_cnf(cl_cnf,name_only=True)

    if cc_cnf:
        freqs = '%.2f - %.2f Hz' % (cc_cnf.bp[1],cc_cnf.bp[2])
        title='Bandpass: %s     MinPts: %i      %s: %.2f' % (
                    freqs, int(pts_min), param_name, float(param1))
    else:
        title='Final result (merged)'

    #setup plot
    fig,ax = plt.subplots(figsize=(12,12))

    ax.set_ylim(north_m-extent, north_m+extent)
    ax.set_xlim(east_m-extent, east_m+extent)

    ax.set_ylabel('North shift [m]')
    ax.set_xlabel('East shift [m]')
    ax.tick_params(axis='both', which='major', labelsize=18)

    clusters = list(set([ev.extras['cluster_number'] for ev in catalog]))

    # events that were sorted out due to thresholds or no data:
    if ev_nocl_notcl:
        easts_nocl_notcl = [ev.east_shift for ev in ev_nocl_notcl if ev.extras['clustered'] is False]
        norths_nocl_notcl = [ev.north_shift for ev in ev_nocl_notcl if ev.extras['clustered'] is False]
        size_nocl_notcl= [mag_scale_log(ev.magnitude, log_tuple, mag_max, mag_min, plotlib=plotlib,method=scale_meth)\
                             for ev in ev_nocl_notcl if ev.extras['clustered'] is False]

        ax.scatter(easts_nocl_notcl,norths_nocl_notcl, s=size_nocl_notcl, marker='o',
                        facecolors='None', edgecolor='#BEBEBE', lw=0.5, zorder=1)

    # events without MT and without cluster number but entered DBSCAN
    ax.scatter(easts_nocl,norths_nocl,s=s_nocl,marker='o', c='#BEBEBE', lw=0.5, zorder=2)

    # events without cluster number but with MT
    for ev in ev_nocl_butmt:

        mmt = ev.moment_tensor
        markersize = mag_scale_log(ev.magnitude, log_tuple, mag_max,mag_min,
                                    plotlib=plotlib,method=scale_meth)

        if ev.extras['clustered']:
            ggcol = 'grey'
        else:
            ggcol = 'black'

        beachball.plot_beachball_mpl(
                mmt, ax,
                beachball_type='dc',
                size=markersize**0.5,
                position=(ev.east_shift, ev.north_shift),
                color_t = '#BEBEBE',
                linewidth=0.5,
                edgecolor=ggcol,
                size_units='points',
                projection='lambert',
                zorder=2)

    # events without MT but in a cluster
    leg_handles = []

    # multiple merged catalogs from freq_merge
    if not cc_cnf:
        origins = len(ev_nomt[0].extras['origins'])

        symbol_list = ['o','*','D','v','^','p','H']

        clusters = list(set([ev.extras['cluster_number'] for ev in ev_nomt]))

        for org_index in range(origins):
            symbol = symbol_list[org_index]
            for cl in clusters:
                pl_ev = [ev for ev in ev_nomt
                         if ev.extras['cluster_number'] == cl and
                         ev.extras['origins'][org_index] == True]
                if pl_ev:
                    hex_color = pl_ev[0].extras['color']
                    easts_nomt = [ev.east_shift for ev in pl_ev]
                    norths_nomt = [ev.north_shift for ev in pl_ev]
                    sizenomt = [mag_scale_log(ev.magnitude, log_tuple, mag_max, mag_min, plotlib=plotlib,method=scale_meth)\
                                 for ev in pl_ev]

                    ax.scatter(easts_nomt, norths_nomt, s=sizenomt, marker=symbol,
                                        c=hex_color, lw=0.5, zorder=3)

    else:
        clusters = list(set([ev.extras['cluster_number'] for ev in catalog if ev.extras['cluster_number'] != -1]))
        for cl in clusters:
            hex_col = [ev for ev in catalog
                     if ev.extras['cluster_number'] == cl][0].extras['color']

            leg_handle = plt.plot((0,0), marker="o", ls='', ms=10, color=hex_col, label=cl)[0]
            leg_handles.append(leg_handle)

        clusters_nomt = list(set([ev.extras['cluster_number'] for ev in ev_nomt]))
        for cl in clusters_nomt:
            pl_ev = [ev for ev in ev_nomt
                     if ev.extras['cluster_number'] == cl]

            hex_col = pl_ev[0].extras['color']

            easts_nomt = [ev.east_shift for ev in pl_ev]
            norths_nomt = [ev.north_shift for ev in pl_ev]
            sizenomt = [mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=plotlib,method=scale_meth) for ev in pl_ev]

            ax.scatter(easts_nomt, norths_nomt, s=sizenomt, marker='o', c=hex_col,
                                lw=0.5, zorder=4)

    # clustered events with an MT
    for ev in ev_mt:
        mmt = ev.moment_tensor
        markersize = mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=plotlib,method=scale_meth)

        beachball.plot_beachball_mpl(
            mmt, ax,
            beachball_type='dc',
            size=markersize**0.5,
            position=(ev.east_shift, ev.north_shift),
            color_t = ev.extras['color'],
            linewidth=0.5,
            edgecolor= 'k',
            size_units='points',
            projection='lambert',
            zorder=5)

    lower = num.floor(mag_min)
    upper = num.ceil(mag_max)

    if upper-lower <= 2:
        step = 0.5
    else:
        step = 1

    # cluster legend
    leg_cols = get_ncols_legend(len(clusters))
    leg = plt.legend(loc='lower left', handles=leg_handles, fontsize=12,
                    ncol=leg_cols,handleheight=1, labelspacing=0.05,
                    handletextpad=0.05)
    leg.set_title("Clusters", prop = {'size': 15})

    #magnitude legend
    if mag_min != mag_max:
        if upper-lower <= 2:
            mags_for_leg = num.array(num.arange(lower,num.round(mag_max,1), step))
            mags_for_leg = num.append(mags_for_leg,mag_max)
        else:
            mags_for_leg = num.array(num.arange(lower,upper,step))
    else:
        mags_for_leg = [1]

    mag_handles = []
    for i_ma, ma in enumerate(mags_for_leg):

        markersize = mag_scale_log(ma, log_tuple,mag_max,mag_min,
                                    plotlib=plotlib,method=scale_meth)
        mag_handle = plt.scatter(0,0, marker="o", c='None',
                edgecolors='black', s=markersize, label='%.1f' % ma)

        mag_handles.append(mag_handle)

    leg_mag = plt.legend(handles=mag_handles, fontsize=15,loc='lower right')
    leg_mag.set_title("Magnitude", prop = {'size': 15})

    ax.add_artist(leg)
    ax.add_artist(leg_mag)

    #set title
    ax.text(0.5,0.95,title,transform=ax.transAxes,fontsize=20, horizontalalignment='center',
                bbox=dict(facecolor='white', edgecolor= 'None', alpha=0.8, pad =10))

    fig.tight_layout()

    if not fn:
        fn = os.path.join(work_dir, 'results/cluster_map_%s.%s' % (method,plformat))

    fig.savefig(fn)

    return fig, ax

def map_plot_carto(catalog, method, cc_cnf, cl_cnf, work_dir, fn=False, plformat='pdf'):

    plotlib = 'matplotlib'
    faults = cl_cnf.map_faults_path

    ev_nomt = []
    ev_mt = []
    lats_nocl = []
    lons_nocl = []
    s_nocl = []
    ev_nocl_notcl = []
    ev_nocl_butmt = []

    mags = [ev.magnitude if ev.magnitude else num.nan for ev in catalog]
    mag_min = num.nanmin(mags)
    mag_max = num.nanmax(mags)

    if num.isnan(mag_min):
        mag_min = 2
        no_mags = True
    else:
        no_mags = False

    if num.isnan(mag_max):
        mag_max = 2

    mag_substituted = False
    for ev in catalog:
        if ev.magnitude == None:
            ev.magnitude = mag_min/2
            mag_substituted = True

    mags = [ev.magnitude for ev in catalog]

    if no_mags:
        logger.warning('WARNING: Catalog without magnitudes. Magnitudes set to 1 for plotting.')
    elif mag_substituted:
        logger.warning('WARNING: Not all events in catalog have magnitudes.Missing magnitudes were \
                        set to min(mag)/2 for plotting. %s %s' % (mag_min, mag_max))


    log_tuple, scale_meth = get_mag_scale_method(cl_cnf)
    for ev in catalog:
        if int(ev.extras['cluster_number']) != -1:
            if ev.moment_tensor:
                ev_mt.append(ev)
            else:
                ev_nomt.append(ev)
        else:
            if ev.moment_tensor:
                ev_nocl_butmt.append(ev)
            else:
                if ev.extras['clustered'] is True:
                    lats_nocl.append(ev.lat)
                    lons_nocl.append(ev.lon)
                    s_nocl.append(mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=plotlib,method=scale_meth))
                else:
                    ev_nocl_notcl.append(ev)

    print('Total:', len(catalog))
    print('Not in cluster:', len(lons_nocl))
    print('Not clustered:', len(ev_nocl_notcl), ' (thereof with MT: %i)' %len(ev_nocl_butmt))
    print('In cluster:', len(ev_mt), '+', len(ev_nomt))


    # map dimensions:
    lats_all = [ev.lat for ev in catalog]
    lons_all = [ev.lon for ev in catalog]

    lats_all_sorted = sorted(lats_all)  # if ev.extras['cluster_number'] != -1]
    lons_all_sorted = sorted(lons_all)  # if ev.extras['cluster_number'] != -1]
    # n_trim = int(len(lats_all) * 0.02)
    # lats_all_sorted = lats_all_sorted[n_trim: -n_trim]
    # lons_all_sorted = lons_all_sorted[n_trim: -n_trim]

    lat_m = num.mean(lats_all)
    lon_m = num.mean(lons_all)

    minlat = num.min(lats_all_sorted)
    minlon = num.min(lons_all_sorted)
    maxlat = num.max(lats_all_sorted)
    maxlon = num.max(lons_all_sorted)

    corners = [od.Loc(minlat, minlon),
               od.Loc(maxlat, maxlon)]

    dist1_km = od.distance_accurate50m(od.Loc(lat_m, lon_m), corners[0])
    dist2_km = od.distance_accurate50m(od.Loc(lat_m, lon_m), corners[1])
    dist_km = max(dist1_km, dist2_km)*cl_cnf.focus_map_plot

    extent = max(num.abs(maxlat-minlat),num.abs(maxlon-minlon))*0.5*cl_cnf.focus_map_plot

    scalebar_length = get_scalebar_length(dist_km)

    param1 = method.split('_')[-2]
    pts_min = method.split('_')[-1]
    param_name = get_cluster_params_cnf(cl_cnf,name_only=True)

    if cc_cnf:
        freqs = '%.2f - %.2f Hz' % (cc_cnf.bp[1],cc_cnf.bp[2])
        title='Bandpass: %s     MinPts: %i      %s: %.3f' % (
                        freqs,int(pts_min),param_name,float(param1))
    else:
        title='Final result (merged)'

    #setup plot
    geo = ccrs.Geodetic()
    ortho = ccrs.PlateCarree()
    fig,ax = plt.subplots(subplot_kw={'projection': ortho}, figsize=(12,12))

    ax.set_extent([lon_m-extent,lon_m+extent,lat_m-extent, lat_m+extent],crs=ortho)

    ax.set_facecolor("#D8F2FE") # fake ocean
    coast_line = cartopy.feature.GSHHSFeature(scale='high', levels=[1], facecolor='#EEECE6', lw=0.5, edgecolor='grey')
    ax.add_feature(coast_line,zorder=0)

    ax.set_xticks(ax.get_xticks(),crs=ortho)
    ax.set_yticks(ax.get_yticks(),crs=ortho)

    lon_formatter = LongitudeFormatter(zero_direction_label=True)
    lat_formatter = LatitudeFormatter()

    ax.xaxis.set_major_formatter(lon_formatter)
    ax.yaxis.set_major_formatter(lat_formatter)

    ax.tick_params(axis='both', which='major', labelsize=18)

    clusters = list(set([ev.extras['cluster_number'] for ev in catalog]))

    if faults:
        print('faults are not implemented in cartopy/matplotlib maps yet')

    # events that were sorted out due to thresholds or no data:
    if ev_nocl_notcl:
        lons_nocl_notcl = [ev.lon for ev in ev_nocl_notcl if ev.extras['clustered'] is False]
        lats_nocl_notcl = [ev.lat for ev in ev_nocl_notcl if ev.extras['clustered'] is False]
        size_nocl_notcl = [mag_scale_log(ev.magnitude, log_tuple, mag_max, mag_min, plotlib=plotlib,method=scale_meth) for ev in ev_nocl_notcl if ev.extras['clustered'] is False]

        ax.scatter(lons_nocl_notcl,lats_nocl_notcl, s=size_nocl_notcl, marker='o',
                        facecolors='None', edgecolor='#BEBEBE', lw=0.5, zorder=1)

    # events without MT and without cluster number but entered DBSCAN
    ax.scatter(lons_nocl,lats_nocl,s=s_nocl,alpha=0.6,marker='o', c='#BEBEBE', lw=0.5, zorder=2)

    # events without cluster number but with MT
    for ev in ev_nocl_butmt:

        mmt = ev.moment_tensor
        markersize = mag_scale_log(ev.magnitude, log_tuple, mag_max,mag_min,
                                    plotlib=plotlib,method=scale_meth)

        if ev.extras['clustered']:
            ggcol = 'grey'
        else:
            ggcol = 'black'

        beachball.plot_beachball_mpl(
                mmt, ax,
                beachball_type='dc',
                size=markersize**0.5,
                position=(ev.lon, ev.lat),
                color_t = '#BEBEBE',
                linewidth=0.5,
                edgecolor=ggcol,
                size_units='points',
                projection='lambert',
                zorder=2)

    # events without MT but in a cluster
    leg_handles = []

    # multiple merged catalogs from freq_merge
    if not cc_cnf:
        origins = len(ev_nomt[0].extras['origins'])

        symbol_list = ['o','*','D','v','^','p','H']

        clusters_nomt = list(set([ev.extras['cluster_number'] for ev in ev_nomt]))

        for org_index in range(origins):
            symbol = symbol_list[org_index]
            for cl in clusters_nomt:
                pl_ev = [ev for ev in ev_nomt
                         if ev.extras['cluster_number'] == cl and
                         ev.extras['origins'][org_index] == True]
                if pl_ev:
                    hex_color = pl_ev[0].extras['color']
                    lonsnomt = [ev.lon for ev in pl_ev]
                    latsnomt = [ev.lat for ev in pl_ev]
                    sizenomt = [mag_scale_log(ev.magnitude, log_tuple, mag_max, mag_min, plotlib=plotlib,method=scale_meth) for ev in pl_ev]

                    ax.scatter(lonsnomt, latsnomt, s=sizenomt, marker=symbol,
                                        c=hex_color, lw=0.5, zorder=3)

    else:
        clusters = list(set([ev.extras['cluster_number'] for ev in catalog if ev.extras['cluster_number'] != -1]))
        for cl in clusters:
            hex_col = [ev for ev in catalog
                     if ev.extras['cluster_number'] == cl][0].extras['color']

            leg_handle = plt.plot((0,0), marker="o", ls='', ms=10, color=hex_col, label=cl)[0]
            leg_handles.append(leg_handle)

        clusters_nomt = list(set([ev.extras['cluster_number'] for ev in ev_nomt]))
        for cl in clusters_nomt:

            pl_ev = [ev for ev in ev_nomt
                     if ev.extras['cluster_number'] == cl]

            hex_col = pl_ev[0].extras['color']

            lonsnomt = [ev.lon for ev in pl_ev]
            latsnomt = [ev.lat for ev in pl_ev]
            sizenomt = [mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=plotlib,method=scale_meth) for ev in pl_ev]

            ax.scatter(lonsnomt, latsnomt, s=sizenomt, marker='o', c=hex_col,
                                lw=0.5, zorder=4, alpha=0.6,edgecolor=hex_col)

    # clustered events with an MT
    for ev in ev_mt:
        mmt = ev.moment_tensor
        markersize = mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=plotlib,method=scale_meth)

        beachball.plot_beachball_mpl(
            mmt, ax,
            beachball_type='dc',
            size=markersize**0.5,
            position=(ev.lon, ev.lat),
            color_t = ev.extras['color'],
            linewidth=0.5,
            edgecolor='k',
            size_units='points',
            projection='lambert',
            zorder=5)

    if num.isnan(mag_min):
        mag_min = 2
        no_mags = True
    else:
        no_mags = False


    if num.isnan(mag_max):
        mag_max = 2

    lower = num.floor(mag_min)
    upper = num.ceil(mag_max)

    if upper-lower <= 2:
        step = 0.5
    else:
        step = 1

    # cluster legend
    leg_cols = get_ncols_legend(len(clusters))
    leg = plt.legend(loc='lower left', handles=leg_handles, fontsize=12,
                    ncol=leg_cols,handleheight=1, labelspacing=0.05,
                    handletextpad=0.05)
    leg.set_title("Clusters", prop = {'size': 15})

    #magnitude legend
    if mag_min != mag_max:
        if upper-lower <= 2:
            mags_for_leg = num.array(num.arange(lower,num.round(mag_max,1), step))
            mags_for_leg = num.append(mags_for_leg,mag_max)
        else:
            mags_for_leg = num.array(num.arange(lower,upper,step))
    else:
        mags_for_leg = [1]

    mag_handles = []
    for i_ma, ma in enumerate(mags_for_leg):

        markersize = mag_scale_log(ma, log_tuple,mag_max,mag_min,
                                    plotlib=plotlib,method=scale_meth)

        mag_handle = plt.scatter(0,0, marker="o", c='None',
                edgecolors='black', s=markersize, label='%.1f' % ma)

        mag_handles.append(mag_handle)

    leg_mag = plt.legend(handles=mag_handles, fontsize=15,loc='lower right')
    leg_mag.set_title("Magnitude", prop = {'size': 15})

    ax.add_artist(leg)
    ax.add_artist(leg_mag)

    #set title
    ax.text(0.5,0.95,title,transform=ax.transAxes,fontsize=20, horizontalalignment='center',
                bbox=dict(facecolor='white', edgecolor= 'None', alpha=0.8, pad =10))

    #set scalebar
    scale_bar(ax, ortho, scalebar_length, (0.5,0.02),2, fontsize=15)
    fig.tight_layout()

    if not fn:
        fn = os.path.join(work_dir, 'results/cluster_map_%s.%s' % (method,plformat))

    fig.savefig(fn)

    return fig, ax

def netsim_matrix(network_similarity, clustering_list, catalog, method, work_dir,plformat):
    '''
    plot results
    (a) network similarity matrix sorted by clusters
    '''

    # (a) net sim sorted
    # quiet complicated sorting procedure... might be a faster way.
    indices = range(len(catalog))
    unsorted_tuples = [(i, cl, ev) for (i, cl, ev) in zip(indices,
                                                          clustering_list,
                                                          catalog)]
    sorted_tuples = sorted(unsorted_tuples, key=lambda x: x[1], reverse=True)
    sorted_indices = [s[0] for s in sorted_tuples]

    network_similarity_cluster = num.empty(network_similarity.shape)
    network_similarity_cluster.fill(num.nan)
    for i in range(network_similarity_cluster.shape[0]):
        for j in range(network_similarity_cluster.shape[1]):
            if j <= i:
                ii = sorted_indices[i]
                jj = sorted_indices[j]
                network_similarity_cluster[i, j] = network_similarity[ii, jj]

    events_sorted = [s[2] for s in sorted_tuples]
    title = 'Net-sim, sorted by cluster'
    fig_name = '%s/results/net_sim_cl_%s.%s' % (work_dir, method, plformat)
    small_imshow_plot(network_similarity_cluster, events_sorted, title,
                      cbarlabel=None, fig_name=fig_name)


def plot_cluster_timing(result_catalog, method_string, work_dir, cl_cnf, plformat):
    '''
    cluster number as y axis, time as x axis --> see if cluster occur at same time etc.

    '''
    fig, ax = plt.subplots(nrows=2, figsize=(8/2.54, 12/2.54), sharex=True)

    clustered_events = [ev for ev in result_catalog if ev.extras['cluster_number'] != -1]
    #x_values = [ev.time for ev in result_catalog if ev.extras['cluster_number'] != -1]
    x_values = [ev.time for ev in clustered_events]
    xvals_d = [datetime.datetime.strptime(util.time_to_str(x), '%Y-%m-%d %H:%M:%S.%f') for x in x_values]
    cl_numbers = [ev.extras['cluster_number'] for ev in clustered_events]
    cum_mom = []
    cum = 0

    tmin = util.stt(util.time_to_str(min(x_values), '%Y-%m-%d'), '%Y-%m-%d')
    tmax = util.stt(util.time_to_str(max(x_values), '%Y-%m-%d'), '%Y-%m-%d')

    day_bins = num.arange(tmin,tmax,3*60*60*24)

    one_day = datetime.timedelta(days = 1) 

    mags = [ev.magnitude if ev.magnitude else num.nan for ev in result_catalog]
    mag_min = num.nanmin(mags)
    mag_max = num.nanmax(mags)


    if num.isnan(mag_min):
        mag_min = 2
        no_mags = True
    else:
        no_mags = False

    if num.isnan(mag_max):
        mag_max = 2

    mag_substituted = False
    for ev in result_catalog:
        if ev.magnitude == None:
            ev.magnitude = mag_min/2
            mag_substituted = True

    mags = [ev.magnitude for ev in result_catalog]

    if no_mags:
        logger.warning('WARNING: Catalog without magnitudes. Magnitudes set to 1 for plotting.')
    elif mag_substituted:
        logger.warning('WARNING: Not all events in catalog have magnitudes.Missing magnitudes were \
                        set to min(mag)/2 for plotting. %s %s' % (mag_min, mag_max))

    log_tuple,scale_meth = get_mag_scale_method(cl_cnf)

    for clust in sorted(list(set(cl_numbers))):
        clcat = [ev for ev in result_catalog if ev.extras['cluster_number'] == clust]
        c = [ev.extras['color'] for ev in clcat if ev][0]

        moms = [pmt.magnitude_to_moment(ev.magnitude) for ev in clcat]
        moms.insert(0,0.)
        moms_cum = num.cumsum(moms)

        ts_unix = [ev.time for ev in clcat]
        ts = [datetime.datetime.strptime(util.tts(ev.time), '%Y-%m-%d %H:%M:%S.%f') for ev in clcat]
        start = ts[0]
        ts.insert(0,start)

        ax[1].step(ts, moms_cum, c=c, lw=0.5, where='post')

    set_labels = sorted(set(cl_numbers))
    y_values = [set_labels.index(cl) for cl in cl_numbers]

    mags = num.array([ev.magnitude for ev in result_catalog if ev.extras['cluster_number'] != -1])
    markersize = mag_scale_log(mags, log_tuple,mag_max,mag_min,plotlib='gmt',method=scale_meth)
    fs = 12

    c = [ev.extras['color'] for ev in result_catalog if ev.extras['cluster_number'] != -1]
    ax[0].scatter(xvals_d, y_values,  s=markersize, marker='o', edgecolor=c, facecolor='none', alpha=0.5)
    ax[0].set_ylabel('Cluster number', fontsize=fs)

    plt.xticks(rotation=70, fontsize=fs)

    if len(set_labels) >= 10:
        spacing = 2
    else:
        spacing = 1

    ax[0].yaxis.set_major_locator(MultipleLocator(spacing))
    ax[0].set_ylim(-1, len(set_labels))

    if len(set_labels) >= 10:
        set_labels = set_labels[::2]
        y_labels = [' '] + set_labels 
    else:
        y_labels = [' ',' '] + set_labels + [' ',' ']

    ax[0].set_yticklabels(y_labels,fontsize=10)

    ax[0].grid(which='both', color='grey', axis='y', linestyle=':', linewidth='0.1')

    ax[1].set_ylabel('Cum. moment per cluster', fontsize=fs)
    ax[1].set_yscale('log')

    plt.tight_layout()
    fig.savefig('%s/results/cluster_%s.%s' % (work_dir, method_string,plformat))
    plt.close()

def knn_plot(dists, pts_min_list, phase_name, work_dir, plformat):
        '''
        k nearest neighbor plot for DBSACN eps estimation
        params dist: network similarity values (distances in DBSCAN)
        params pts_min_list: see DBSCAN 
        '''
        dist = dists.copy()
        bluecolors = util_clusty.get_colormap('bluecolors')
        
        fig, ax = plt.subplots(figsize=(3.486, 3.486/1.618))
        # ax2 = ax.twinx() # gradient helper for knee finding

        for i_pts, pts_min in enumerate(pts_min_list):
            dist[num.where(dist == 0)] = 1.
            dist_sort = num.sort(dist,axis=1) 

            knns = dist_sort[:,pts_min-1]
            knns = num.sort(knns)

            idx = num.min(num.argwhere(knns == 1))
            knns = knns[:idx]
            x = range(len(knns))

            ax.plot(x,knns, label=pts_min, c=bluecolors[i_pts], lw = 0.5)

            # #gradient helper for knee finding
            # grads = num.gradient(knns)
            # grads = grads/num.mean(grads) -pts_min-1

            # #smooth gradient(hanning window of 10)
            # s=num.r_[grads[10-1:0:-1],grads,grads[-2:-10-1:-1]]
            # w = num.hanning(20)
            # grads=num.convolve(w/w.sum(),s,mode='valid')

            # #N = 40
            # #grads=num.convolve(grads, num.ones((N,))/N, mode='valid')

            # #cumsum = numpy.cumsum(numpy.insert(x, 0, 0))
            # #return (cumsum[N:] - cumsum[:-N]) / float(N)

            # ax2.plot(range(len(grads))[10:-10],grads[10:-10], zorder=1, c=bluecolors[i_pts], alpha=0.3)

        ax.set_ylabel('Similarity-distance of \n k- nearest neighbor ( ~ $Eps$)', fontsize=6)
        ax.set_xlabel('Index (sorted)', fontsize=6)

        ymin, ymax = ax.get_ylim()
        ax.set_ylim(0,ymax)

        ax.yaxis.set_major_locator(MultipleLocator(0.1))
        ax.yaxis.set_minor_locator(MultipleLocator(0.05))

        ax.grid(which='both', color='grey', axis='y', linestyle=':', linewidth='0.1')
        ax.tick_params(axis='both', which='major', labelsize=6, top=False)

        legend = ax.legend(loc='upper left', fontsize=6, title='MinPts', fancybox=True,
                           framealpha=0.3, borderaxespad=1.0)
        legend.get_title().set_fontsize('6')


        #ax2.set_yticklabels([])
        #ax2.set_yticks([])

        #ax.set_ylim(-0.1,ymax)
        #ax.set_yticks(num.arange(0.0, ymax, step=0.05))

        #ax2.tick_params(axis='both', which='major', labelsize=6, top=False)

        #ax2.set_ylabel('normalized smoothed gradient', color='grey', fontsize=6)

        for axis in ['top','bottom','left','right']:
            ax.spines[axis].set_linewidth(0.5)
            #ax2.spines[axis].set_linewidth(0.5)

        legend.get_frame().set_linewidth(0.5)

        plt.tight_layout()
        fig.savefig('%s/results/knn_plot_%s.%s' % (work_dir, phase_name, plformat))
        plt.close()


def get_twd(i_st, i_ev, phases, cc_cnf, arrivals=None):
    if not cc_cnf.twd:
        n_phases = len(phases)
        if n_phases > 1:
            if 'P' in phases and 'S' in phases:
                dps = arrivals[1, i_st, i_ev] - arrivals[0, i_st, i_ev]
                twd = [[-2,dps],[-2, 1.5*dps]]
            elif 'R' in phases and 'L' in phases:
                len_twd = (3.* 1./(cc_cnf.bp[1])) +10
                twd = [[-10,len_twd],[-10,len_twd]]
        else:
            if 'P' in phases:
                twd = [[-2, 15]]
            if 'S' in phases:
                twd = [[0,15]]
            if 'R' in phases or 'L' in phases:
                len_twd = (3.* 1./(cc_cnf.bp[1])) +10
                twd = [[-10,len_twd]]
    else:
        twd = cc_cnf.twd

    return twd


def c_to_iph(c):

    if c == 'Z':
        i_ph = 0

    elif c in ['E','N','1','2','3']:
        i_ph =1

    else:
        logger.error('ERROR: Not in supported components [Z,E,N,1,2,3]')

    return i_ph


def wf_cluster_plot(resultcat, subset_stations_and_indices, waveform_dir, arrivals, 
                    cc_cnf, method_string, representative_events,
                    cl_cnf, work_dir, plformat):

    '''
    read result file and plot waveforms of each cluster for chosen stations on 
    top of each other.
    nrows = nclusters
    ncolums = nstations (select a subset or random number)
    '''

    logger.info('Starting waveform plots of clustering results.')

    test_comps = [c[-1] for c in cc_cnf.components]
    # translate non-oriented horizontal components to NE for plotting ONLY! No data rotation
    test_comps_input = test_comps.copy()

    for icomp, comp in enumerate(test_comps):
        if comp == '1':
            test_comps[icomp] = 'E'
        elif comp == '2':
            if '1' in test_comps_input:
                test_comps[icomp] = 'N'
            elif '3' in test_comps_input:
                test_comps[icomp] = 'E'
        elif comp == '3':
            test_comps[icomp] = 'N'

    comp_dict = {}
    for icomp, comp in enumerate(test_comps):
        comp_dict[comp] = test_comps_input[icomp]

    if 'Z' in test_comps_input:
        tshift_arrayZ = num.load('%s/precalc_arrays/cctshifts_array_Z.npy' % work_dir)
    if 'N' in test_comps_input:
        tshift_arrayN = num.load('%s/precalc_arrays/cctshifts_array_N.npy' % work_dir)
    if 'E' in test_comps_input:
        tshift_arrayE = num.load('%s/precalc_arrays/cctshifts_array_E.npy' % work_dir)
    if '1' in test_comps_input:
        tshift_arrayE = num.load('%s/precalc_arrays/cctshifts_array_1.npy' % work_dir)
    if '2' in test_comps_input:
        if '1' in test_comps_input:
            tshift_arrayN = num.load('%s/precalc_arrays/cctshifts_array_2.npy' % work_dir)
        elif '3' in test_comps_input:
            tshift_arrayE = num.load('%s/precalc_arrays/cctshifts_array_2.npy' % work_dir)
    if '3' in test_comps_input:
        tshift_arrayN = num.load('%s/precalc_arrays/cctshifts_array_2.npy' % work_dir)


    nstations = len(subset_stations_and_indices)

    if cl_cnf.wf_cluster_choice:
        set_clusters = cl_cnf.wf_cluster_choice
    else:
        set_clusters = list(set([ev.extras['cluster_number'] for ev in resultcat 
                             if ev.extras['cluster_number'] != -1]))
    set_clusters.sort()
    nclusters = len(set_clusters)
    phases = cc_cnf.phase
    n_phases = len(phases)

    result_dict = {}
    for cl in set_clusters:
        result_dict[cl] = [(i_ev, ev) for i_ev, ev in enumerate(resultcat)
                           if ev.extras['cluster_number'] == cl]

    for i_st, st in subset_stations_and_indices:

        logger.info('Preparing waveform plot for %s.%s' % (st.network, st.station))

        if not '*' in waveform_dir:
            #p = pile.make_pile(waveform_dir, regex='%s.%s' % (st.network, st.station), 
            #                   show_progress=False)
            p = pile.make_pile(waveform_dir, regex='%s' % (st.station),
                               show_progress=True, fileformat='detect')
        else:
            files = glob.glob('%s/*%s.%s*' % (waveform_dir, st.network, st.station))
            p = pile.make_pile(files, show_progress=True)

        trZs_allcl = []
        trEs_allcl = []
        trNs_allcl = []

        for i_cl, cl in enumerate(set_clusters):
            
            trZs = []
            trEs = []
            trNs = []
            
            cl_events = result_dict[cl]
            nev = len(cl_events)
            ntr = 0
            plot_last = False
            plot_last_list = []

            # representative event of cluster:
            repre_ev = [ev for ev in representative_events if ev.extras['cluster_number'] == cl][0]
            sil_val = repre_ev.extras['max_sil_val']
            i_repr = [i for i,ev in enumerate(resultcat) if ev.name == repre_ev.name][0]

            ii = [ii for ii,iiev in enumerate(cl_events) if iiev[1].name == repre_ev.name][0]

            # get list of time shifts for current cluster:
            t_shift_listZ = num.empty((nev))
            t_shift_listZ.fill(num.nan)
            t_shift_listN = num.empty((nev))
            t_shift_listN.fill(num.nan)
            t_shift_listE = num.empty((nev))
            t_shift_listE.fill(num.nan)

            # fill tshift of representative event, others are filled relative to this event
            t_shift_listZ[ii] = -repre_ev.time#-arrivals[0, i_st, i_ev]
            t_shift_listN[ii] = -repre_ev.time#-arrivals[1, i_st, i_ev]
            t_shift_listE[ii] = -repre_ev.time#-arrivals[1, i_st, i_ev]

            i_ev_list = [clev[0] for clev in cl_events]

            # fill all that have cc and tshift value relative to representative event
            for iii, (i_ev, ev) in enumerate(cl_events):
                if iii == i_repr:
                    continue
                else:
                    if 'Z' in test_comps:
                        if not num.isnan(tshift_arrayZ[i_st, i_ev, i_repr]):
                            t_shift_listZ[iii] = t_shift_listZ[ii] + tshift_arrayZ[i_st, i_ev, i_repr]
                        elif not num.isnan(tshift_arrayZ[i_st, i_repr, i_ev]):
                            t_shift_listZ[iii] = t_shift_listZ[ii] - tshift_arrayZ[i_st, i_repr, i_ev]
                    if 'N' in test_comps:
                        if not num.isnan(tshift_arrayN[i_st, i_ev, i_repr]):
                            t_shift_listN[iii] = t_shift_listN[ii] + tshift_arrayN[i_st, i_ev, i_repr]
                        elif not num.isnan(tshift_arrayN[i_st, i_repr, i_ev]):
                            t_shift_listN[iii] = t_shift_listN[ii] - tshift_arrayN[i_st, i_repr, i_ev]
                    if 'E' in test_comps:
                        if not num.isnan(tshift_arrayE[i_st, i_ev, i_repr]):
                            t_shift_listE[iii] = t_shift_listE[ii] + tshift_arrayE[i_st, i_ev, i_repr]
                        elif not num.isnan(tshift_arrayE[i_st, i_ev, i_repr]):
                            t_shift_listE[iii] = t_shift_listE[ii] - tshift_arrayE[i_st, i_repr, i_ev]


            # test if there are still some shift times unset, if so use shift times
            # relative to other than representative event.
            
            if 'Z' in test_comps:
                if True in num.isnan(t_shift_listZ):
                    for i0, i_ev0 in enumerate(i_ev_list):
                        if not num.isnan(t_shift_listZ[i0]):
                            continue
                        else:
                            for i1, i_ev1 in enumerate(i_ev_list):
                                if i0 == i1:
                                    continue
                                dt = tshift_arrayZ[i_st, i_ev1, i_ev0]
                                #dt2 = tshift_array[i_ph, i_st, i_ev0, i_ev1]
                                if not num.isnan(dt):
                                    t_shift_listZ[i0] = t_shift_listZ[i1] - dt
            if 'N' in test_comps:
                if True in num.isnan(t_shift_listN):
                    for i0, i_ev0 in enumerate(i_ev_list):
                        if not num.isnan(t_shift_listN[i0]):
                            continue
                        else:
                            for i1, i_ev1 in enumerate(i_ev_list):
                                if i0 == i1:
                                    continue
                                dt = tshift_arrayN[i_st, i_ev1, i_ev0]
                                #dt2 = tshift_array[i_ph, i_st, i_ev0, i_ev1]
                                if not num.isnan(dt):
                                    t_shift_listN[i0] = t_shift_listN[i1] - dt

            if 'E' in test_comps:
                if True in num.isnan(t_shift_listZ):
                    for i0, i_ev0 in enumerate(i_ev_list):
                        if not num.isnan(t_shift_listE[i0]):
                            continue
                        else:
                            for i1, i_ev1 in enumerate(i_ev_list):
                                if i0 == i1:
                                    continue
                                dt = tshift_arrayE[i_st, i_ev1, i_ev0]
                                #dt2 = tshift_array[i_ph, i_st, i_ev0, i_ev1]
                                if not num.isnan(dt):
                                    t_shift_listE[i0] = t_shift_listE[i1] - dt


            for iii, (i_ev, ev) in enumerate(cl_events):
                arr = arrivals[0, i_st, i_ev]
                twd = get_twd(i_st, i_ev, phases, cc_cnf, arrivals)
                twd_ = twd
                twd = twd[0]

                try:
                    if 'Z' in test_comps:
                        tr_Z = p.all(tmin=arr-cc_cnf.filtertmin, tmax=arr+cc_cnf.filtertmax,
                            trace_selector=lambda tr: str(tr.station) == str(st.station) and
                            tr.channel.endswith('Z'))

                        for itr, tr in enumerate(tr_Z):
                            if tr.tmin < arr and tr.tmax > arr:
                                useZ = itr
                        tr_Z = tr_Z[useZ]


                    if 'N' in test_comps:
                        tr_N = p.all(tmin=arr-cc_cnf.filtertmin, tmax=arr+cc_cnf.filtertmax,
                            trace_selector=lambda tr: str(tr.station) == str(st.station) and
                            tr.channel.endswith(comp_dict['N']))
                        
                        for itr, tr in enumerate(tr_N):
                            if tr.tmin < arr and tr.tmax > arr:
                                useN = itr
                        tr_N = tr_N[useN]


                    if 'E' in test_comps:
                        tr_E = p.all(tmin=arr-cc_cnf.filtertmin, tmax=arr+cc_cnf.filtertmax,
                             trace_selector=lambda tr: str(tr.station) == str(st.station) and
                             tr.channel.endswith(comp_dict['E']))

                        for itr, tr in enumerate(tr_E):
                            if tr.tmin < arr and tr.tmax > arr:
                                 useE = itr

                        tr_E = tr_E[useE]

                except:
                    logger.debug('WF cluster plot: %s.%s %s - no data found' %
                                  (st.network, st.station, ev.name))
                    continue

                if cc_cnf.downsample_to:
                    if 'Z' in test_comps:
                        tr_Z.downsample_to(cc_cnf.downsample_to)
                    if 'N' in test_comps:
                        tr_N.downsample_to(cc_cnf.downsample_to)
                    if 'E' in test_comps:    
                        tr_E.downsample_to(cc_cnf.downsample_to)

                if 'Z' in test_comps:
                    tr_Z.bandpass(int(cc_cnf.bp[0]), cc_cnf.bp[1], cc_cnf.bp[2])
                if 'N' in test_comps:
                    tr_N.bandpass(int(cc_cnf.bp[0]), cc_cnf.bp[1], cc_cnf.bp[2])
                if 'E' in test_comps:
                    tr_E.bandpass(int(cc_cnf.bp[0]), cc_cnf.bp[1], cc_cnf.bp[2])

                if cc_cnf.snr_calc:
                    snr = comp_snr(tr_Z, arr, twd_, st.station, ev.name)
                else:
                    snr = 9999

                if snr > cc_cnf.snr_thresh:
                    #print('before %s %s %s ' % (tr_Z.tmin, len(tr_Z.ydata), arr))
                    t_buffer = cl_cnf.t_buffer_wfplot  # 20
                    # print('evtime', ev.time)
                    # print('arr', arr)
                    #print('t_shift_list', num.nanmax(t_shift_list), num.nanmin(t_shift_list))
                    #input()
                    #t_buffer = num.nanmax(t_shift_list), num.nanmin(t_shift_list)
                    try:
                        if 'Z' in test_comps:
                            tr_Z.chop(tmin=arr - t_buffer, tmax=arr + twd[1]+t_buffer)
                            logger.debug('first chop %s %s %s ' % (tr_Z.tmin, tr_Z.tmax, len(tr_Z.ydata)))
                        if 'N' in test_comps:
                            tr_N.chop(tmin=arr - t_buffer, tmax=arr + twd[1]+t_buffer)
                        if 'E' in test_comps:
                            tr_E.chop(tmin=arr - t_buffer, tmax=arr + twd[1]+t_buffer)

                    except trace.NoData:
                        logger.info('Trace to short for cutting')
                        continue

                    except ValueError:
                        logger.info('Value error. comps: %s, arr: %s, t_buffer: %s,\
                                    twd1: %s, tr tmin: %s, tr tmax: %s'
                                    % (test_comps_input, arr, t_buffer, twd[1], tr_Z.tmin,
                                        tr_Z.tmax))

                    if 'Z' in test_comps:
                        dtZ = t_shift_listZ[iii]
                    if 'N' in test_comps:
                        dtN = t_shift_listN[iii]
                    if 'E' in test_comps:
                        dtE = t_shift_listE[iii]


                    if 'Z' in test_comps and not num.isnan(dtZ):
                        trZ_ = tr_Z.copy()
                        trZ_.shift(dtZ)
                        trZ_.set_codes(location=str(cl))
                        trZ_.ydata = trZ_.ydata/ num.max(trZ_.ydata)
                        trZs.append(trZ_)

                    if 'N' in test_comps and not num.isnan(dtN):
                        trN_ = tr_N.copy()
                        trN_.shift(dtN)
                        trN_.set_codes(location=str(cl))
                        trN_.ydata = trN_.ydata/ num.max(trN_.ydata)
                        trNs.append(trN_)

                    if 'E' in test_comps and not num.isnan(dtE):
                        trE_ = tr_E.copy()
                        trE_.shift(dtE)
                        trE_.set_codes(location=str(cl))
                        trE_.ydata = trE_.ydata/ num.max(trE_.ydata)
                        trEs.append(trE_)
 

                else:
                    continue

            trZs_allcl.append(trZs)
            trNs_allcl.append(trNs)
            trEs_allcl.append(trEs)

        if 'Z' in test_comps:
            #trace.snuffle(trZs)
            #io.save(trZs, './results/%s_traces_Z_aligned.yaff' % st.station,
            #        format='yaff')

            #for i_cl, cl in enumerate(set_clusters):
            #    trace.snuffle(trZs_allcl[i_cl])

            if nclusters > 10:
                ipl = 0
                for i_cl, cl in enumerate(set_clusters):
                    cl_events = result_dict[cl]
                    nev = len(cl_events)
                    ntr = len(trZs_allcl[i_cl])
                    repre_ev = [ev for ev in representative_events if ev.extras['cluster_number'] == cl][0]
                    sil_val = round(repre_ev.extras['max_sil_val'],2)
                    if i_cl == 0 or i_cl % 10 == 0:
                        ii = 0
                        fig, ax = plt.subplots(nrows=10, ncols=1, figsize=(10, 10),sharex=True)
                        fig.suptitle('%s.%s Z %s' % (st.network, st.station, method_string), fontsize=10)
                    
                    for tr in trZs_allcl[i_cl]:
                        ax[ii].plot(tr.get_xdata(), tr.ydata, linewidth=0.5, c='black', alpha=0.3)

                    ax[ii].set_axis_off()
                    ax[ii].text(0.02, 0.0, 'cl: %s, nev: %s, ntr = %s, %s' %
                             (cl, nev, ntr, sil_val), fontsize=8,
                             horizontalalignment='center',
                             verticalalignment='center', 
                             transform=ax[ii].transAxes)

                    if i_cl == 9 or i_cl % 10 == 9 or i_cl == nclusters-1:
                        if i_cl == nclusters-1 and i_cl % 10 != 9 and i_cl != 9:
                            for ia in range(1,9):
                                ax[ii+ia].set_axis_off()
                                if ii + ia == 9 and ii + ia % 10 == 9:
                                    break
                
                        plt.tight_layout()
                        fig.savefig('%s/results/wf_plot_%s.%s_%s_Z_%s.%s' %
                            (work_dir, st.network, st.station, method_string, ipl,plformat))
                        plt.close()
                        ipl +=1
                    
                    ii +=1
             
            else:

                fig, ax = plt.subplots(nrows=nclusters, ncols=1, figsize=(10, nclusters),sharex=True)

                
                for i_cl, cl in enumerate(set_clusters):
                    cl_events = result_dict[cl]
                    nev = len(cl_events)
                    ntr = len(trZs_allcl[i_cl])
                    ntr = len(trZs_allcl[i_cl])
                    repre_ev = [ev for ev in representative_events if ev.extras['cluster_number'] == cl][0]
                    sil_val = round(repre_ev.extras['max_sil_val'],2)


                    if nclusters > 1:
                        for tr in trZs_allcl[i_cl]:
                            ax[i_cl].plot(tr.get_xdata(), tr.ydata, linewidth=0.5, c='black', alpha=0.3)
                        ax[i_cl].set_axis_off()
                        ax[i_cl].text(0.02, 0.0, 'cl: %s, nev: %s, ntr = %s, %s' %
                                 (cl, nev, ntr, sil_val), fontsize=8,
                                 horizontalalignment='center',
                                 verticalalignment='center', 
                                 transform=ax[i_cl].transAxes)
                    else:
                        for tr in trZs_allcl[i_cl]:
                            ax.plot(tr.get_xdata(), tr.ydata, linewidth=0.5, c='black', alpha=0.3)
                        ax.set_axis_off()
                        ax.text(0.02, 0.0, 'cl: %s, nev: %s, ntr = %s, %s' %
                                 (cl, nev, ntr, sil_val), fontsize=8,
                                 horizontalalignment='center',
                                 verticalalignment='center', 
                                 transform=ax.transAxes)
                    

                fig.suptitle('%s.%s Z %s' % (st.network, st.station, method_string), fontsize=10)
                
                #plt.show()
                plt.tight_layout()
                fig.savefig('%s/results/wf_plot_%s.%s_Z_%s.%s' %
                    (work_dir, st.network, st.station, method_string,plformat))
                plt.close()


        if 'N' in test_comps:
            #trace.snuffle(trZs)
            #io.save(trNs, './results/%s_traces_N_aligned.yaff' % st.station,
            #        format='yaff')

            #for i_cl, cl in enumerate(set_clusters):
            #    trace.snuffle(trNs_allcl[i_cl])


            if nclusters > 10:
                ipl = 0
                for i_cl, cl in enumerate(set_clusters):
                    ntr = len(trNs_allcl[i_cl])
                    cl_events = result_dict[cl]
                    nev = len(cl_events)
                    ntr = len(trNs_allcl[i_cl])
                    repre_ev = [ev for ev in representative_events if ev.extras['cluster_number'] == cl][0]
                    sil_val = round(repre_ev.extras['max_sil_val'],2)

                    if i_cl == 0 or i_cl % 10 == 0:
                        ii = 0
                        fig, ax = plt.subplots(nrows=10, ncols=1, figsize=(10, 10))
                        fig.suptitle('%s.%s N %s' % (st.network, st.station, method_string), fontsize=10)
                    
                    for tr in trNs_allcl[i_cl]:
                        ax[ii].plot(tr.get_xdata(), tr.ydata, linewidth=0.5, c='black', alpha=0.3)
                
                    ax[ii].set_axis_off()
                    ax[ii].text(0.02, 0.0, 'cl: %s, nev: %s, ntr = %s, %s' %
                             (cl, nev, ntr, sil_val), fontsize=8,
                             horizontalalignment='center',
                             verticalalignment='center', 
                             transform=ax[ii].transAxes)
                        
                    if i_cl == 9 or i_cl % 10 == 9 or i_cl == nclusters-1:
                        if i_cl == nclusters-1 and i_cl % 10 != 9 and i_cl != 9:
                            for ia in range(1,9):
                                ax[ii+ia].set_axis_off()
                                if ii + ia == 9 and ii + ia % 10 == 9:
                                    break

                        #plt.show()
                        plt.tight_layout()
                        fig.savefig('%s/results/wf_plot_%s.%s_%s_%s_%s.%s' %
                            (work_dir, st.network, st.station, method_string, comp_dict['N'], ipl,plformat))
                        plt.close()
                        ipl += 1
                    
                    ii +=1

            else:
                fig, ax = plt.subplots(nrows=nclusters, ncols=1, figsize=(10, nclusters))

                
                for i_cl, cl in enumerate(set_clusters):
                    cl_events = result_dict[cl]
                    nev = len(cl_events)
                    ntr = len(trNs_allcl[i_cl])
                    ntr = len(trNs_allcl[i_cl])
                    repre_ev = [ev for ev in representative_events if ev.extras['cluster_number'] == cl][0]
                    sil_val = round(repre_ev.extras['max_sil_val'],2)


                    if nclusters > 1:
                        for tr in trNs_allcl[i_cl]:
                            ax[i_cl].plot(tr.get_xdata(), tr.ydata, linewidth=0.5, c='black', alpha=0.3)
                        ax[i_cl].set_axis_off()
                        ax[i_cl].text(0.02, 0.0, 'cl: %s, nev: %s, ntr = %s, %s' %
                                 (cl, nev, ntr, sil_val), fontsize=8,
                                 horizontalalignment='center',
                                 verticalalignment='center', 
                                 transform=ax[i_cl].transAxes)
                    else:
                        for tr in trNs_allcl[i_cl]:
                            ax.plot(tr.get_xdata(), tr.ydata, linewidth=0.5, c='black', alpha=0.3)
                        ax.set_axis_off()
                        ax.text(0.02, 0.0, 'cl: %s, nev: %s, ntr = %s, %s' %
                                 (cl, nev, ntr, sil_val), fontsize=8,
                                 horizontalalignment='center',
                                 verticalalignment='center', 
                                 transform=ax.transAxes)
                    

                fig.suptitle('%s.%s %s %s' % (st.network, st.station, comp_dict['N'], method_string), fontsize=10)
                
                #plt.show()
                plt.tight_layout()
                fig.savefig('%s/results/wf_plot_%s.%s_%s_%s.%s' %
                    (work_dir, st.network, st.station, comp_dict['N'], method_string,plformat))
                plt.close()


        if 'E' in test_comps:
                        #trace.snuffle(trZs)
            #io.save(trZs, './results/%s_traces_Z_aligned.yaff' % st.station,
            #        format='yaff')

            #for i_cl, cl in enumerate(set_clusters):
            #    trace.snuffle(trEs_allcl[i_cl])


            if nclusters > 10:
                ipl = 0
                for i_cl, cl in enumerate(set_clusters):
                    ntr = len(trEs_allcl[i_cl])
                    cl_events = result_dict[cl]
                    nev = len(cl_events)
                    ntr = len(trEs_allcl[i_cl])
                    repre_ev = [ev for ev in representative_events if ev.extras['cluster_number'] == cl][0]
                    sil_val = round(repre_ev.extras['max_sil_val'],2)
                    if i_cl == 0 or i_cl % 10 == 0:
                        ii = 0
                        fig, ax = plt.subplots(nrows=10, ncols=1, figsize=(10, 10))
                        fig.suptitle('%s.%s E %s' % (st.network, st.station, method_string))
                    
                    for tr in trEs_allcl[i_cl]:
                        ax[ii].plot(tr.get_xdata(), tr.ydata, linewidth=0.5, c='black', alpha=0.3)
                
                    ax[ii].set_axis_off()
                    ax[ii].text(0.02, 0.0, 'cl: %s, nev: %s, ntr = %s, %s' %
                             (cl, nev, ntr, sil_val), fontsize=8,
                             horizontalalignment='center',
                             verticalalignment='center', 
                             transform=ax[ii].transAxes)
                        
                    if i_cl == 9 or i_cl % 10 == 9 or i_cl == nclusters-1:
                        if i_cl == nclusters-1 and i_cl % 10 != 9 and i_cl != 9:
                            for ia in range(1,9):
                                ax[ii+ia].set_axis_off()
                                if ii + ia == 9 and ii + ia % 10 == 9:
                                    break
                        #plt.show()
                        plt.tight_layout()
                        fig.savefig('%s/results/wf_plot_%s.%s_%s_%s_%s.%s' %
                            (work_dir, st.network, st.station, method_string, comp_dict['E'], ipl,plformat))
                        plt.close()
                        ipl += 1
                    
                    ii +=1

            else:
                fig, ax = plt.subplots(nrows=nclusters, ncols=1, figsize=(10, nclusters))

                
                for i_cl, cl in enumerate(set_clusters):
                    cl_events = result_dict[cl]
                    nev = len(cl_events)
                    ntr = len(trEs_allcl[i_cl])
                    ntr = len(trEs_allcl[i_cl])
                    repre_ev = [ev for ev in representative_events if ev.extras['cluster_number'] == cl][0]
                    repre_ev = [ev for ev in representative_events if ev.extras['cluster_number'] == cl][0]
                    sil_val = round(repre_ev.extras['max_sil_val'],2)


                    if nclusters > 1:
                        for tr in trEs_allcl[i_cl]:
                            ax[i_cl].plot(tr.get_xdata(), tr.ydata, linewidth=0.5, c='black', alpha=0.3)
                        ax[i_cl].set_axis_off()
                        ax[i_cl].text(0.02, 0.0, 'cl: %s, nev: %s, ntr = %s, %s' %
                                 (cl, nev, ntr, sil_val), fontsize=8,
                                 horizontalalignment='center',
                                 verticalalignment='center', 
                                 transform=ax[i_cl].transAxes)
                    else:
                        for tr in trEs_allcl[i_cl]:
                            ax.plot(tr.get_xdata(), tr.ydata, linewidth=0.5, c='black', alpha=0.3)
                        ax.set_axis_off()
                        ax.text(0.02, 0.0, 'cl: %s, nev: %s, ntr = %s, %s' %
                                 (cl, nev, ntr, sil_val), fontsize=8,
                                 horizontalalignment='center',
                                 verticalalignment='center', 
                                 transform=ax.transAxes)
                    

                fig.suptitle('%s.%s %s %s' % (st.network, st.station, comp_dict['E'], method_string), fontsize=10)
                
                #plt.show()
                plt.tight_layout()
                fig.savefig('%s/results/wf_plot_%s.%s_%s_%s.%s' %
                    (work_dir, st.network, st.station, comp_dict['E'], method_string,plformat))
                plt.close()




def wf_cluster_snuffle(resultcat, subset_stations_and_indices, waveform_dir, arrivals, 
                    cc_cnf, method_string, representative_events, cl_cnf, work_dir):

    '''
    read result file and plot waveforms of each cluster for chosen stations on 
    top of each other.
    nrows = nclusters
    ncolums = nstations (select a subset or random number)
    '''

    logger.info('Starting waveform snuffling of clustering results.')

    test_comps = [c[-1] for c in cc_cnf.components]
    # translate non-oriented horizontal components to NE for plotting ONLY! No data rotation
    test_comps_input = test_comps.copy()

    for icomp, comp in enumerate(test_comps):
        if comp == '1':
            test_comps[icomp] = 'E'
        elif comp == '2':
            if '1' in test_comps_input:
                test_comps[icomp] = 'N'
            elif '3' in test_comps_input:
                test_comps[icomp] = 'E'
        elif comp == '3':
            test_comps[icomp] = 'N'

    comp_dict = {}
    for icomp, comp in enumerate(test_comps):
        comp_dict[comp] = test_comps_input[icomp]

    if 'Z' in test_comps_input:
        tshift_arrayZ = num.load('%s/precalc_arrays/cctshifts_array_Z.npy' % work_dir)
    if 'N' in test_comps_input:
        tshift_arrayN = num.load('%s/precalc_arrays/cctshifts_array_N.npy' % work_dir)
    if 'E' in test_comps_input:
        tshift_arrayE = num.load('%s/precalc_arrays/cctshifts_array_E.npy' % work_dir)
    if '1' in test_comps_input:
        tshift_arrayE = num.load('%s/precalc_arrays/cctshifts_array_1.npy' % work_dir)
    if '2' in test_comps_input:
        if '1' in test_comps_input:
            tshift_arrayN = num.load('%s/precalc_arrays/cctshifts_array_2.npy' % work_dir)
        elif '3' in test_comps_input:
            tshift_arrayE = num.load('%s/precalc_arrays/cctshifts_array_2.npy' % work_dir)
    if '3' in test_comps_input:
        tshift_arrayN = num.load('%s/precalc_arrays/cctshifts_array_2.npy' % work_dir)

    nstations = len(subset_stations_and_indices)

    if cl_cnf.wf_cluster_choice:
        set_clusters = cl_cnf.wf_cluster_choice
    else:
        set_clusters = list(set([ev.extras['cluster_number'] for ev in resultcat 
                             if ev.extras['cluster_number'] != -1]))
    set_clusters.sort()
    nclusters = len(set_clusters)
    phases = cc_cnf.phase
    n_phases = len(phases)

    result_dict = {}
    for cl in set_clusters:
        result_dict[cl] = [(i_ev, ev) for i_ev, ev in enumerate(resultcat)
                           if ev.extras['cluster_number'] == cl]

    for i_st, st in subset_stations_and_indices:

        logger.info('Preparing waveform snuffle for %s.%s' % (st.network, st.station))

        if not '*' in waveform_dir:
            #p = pile.make_pile(waveform_dir, regex='%s.%s' % (st.network, st.station), 
            #                   show_progress=False)
            p = pile.make_pile(waveform_dir, regex='%s' % (st.station),
                               show_progress=True, fileformat='detect')
        else:
            files = glob.glob('%s/*%s.%s*' % (waveform_dir, st.network, st.station))
            p = pile.make_pile(files, show_progress=False)

        if not p:
            continue

        trZs = []
        trEs = []
        trNs = []

        for i_cl, cl in enumerate(set_clusters):
            
            cl_events = result_dict[cl]
            nev = len(cl_events)
            ntr = 0
            plot_last = False
            plot_last_list = []

            # representative event of cluster:
            repre_ev = [ev for ev in representative_events if ev.extras['cluster_number'] == cl][0]
            i_repr = [i for i,ev in enumerate(resultcat) if ev.name == repre_ev.name][0]  ## cl_events?

            ii = [ii for ii,iiev in enumerate(cl_events) if iiev[1].name == repre_ev.name][0]

            # get list of time shifts for current cluster:
            t_shift_listZ = num.empty((nev))
            t_shift_listZ.fill(num.nan)
            t_shift_listN = num.empty((nev))
            t_shift_listN.fill(num.nan)
            t_shift_listE = num.empty((nev))
            t_shift_listE.fill(num.nan)

            # fill tshift of representative event, others are filled relative to this event
            t_shift_listZ[ii] = -repre_ev.time#-arrivals[0, i_st, i_ev]
            t_shift_listN[ii] = -repre_ev.time#-arrivals[1, i_st, i_ev]
            t_shift_listE[ii] = -repre_ev.time#-arrivals[1, i_st, i_ev]

            i_ev_list = [clev[0] for clev in cl_events]

            # fill all that have cc and tshift value relative to representative event
            for iii, (i_ev, ev) in enumerate(cl_events):
                if iii == i_repr:
                    continue
                else:
                    if 'Z' in test_comps:
                        if not num.isnan(tshift_arrayZ[i_st, i_ev, i_repr]):
                            t_shift_listZ[iii] = t_shift_listZ[ii] + tshift_arrayZ[i_st, i_ev, i_repr]
                        elif not num.isnan(tshift_arrayZ[i_st, i_repr, i_ev]):
                            t_shift_listZ[iii] = t_shift_listZ[ii] - tshift_arrayZ[i_st, i_repr, i_ev]
                    if 'N' in test_comps:
                        if not num.isnan(tshift_arrayN[i_st, i_ev, i_repr]):
                            t_shift_listN[iii] = t_shift_listN[ii] + tshift_arrayN[i_st, i_ev, i_repr]
                        elif not num.isnan(tshift_arrayN[i_st, i_repr, i_ev]):
                            t_shift_listN[iii] = t_shift_listN[ii] - tshift_arrayN[i_st, i_repr, i_ev]
                    if 'E' in test_comps:
                        if not num.isnan(tshift_arrayE[i_st, i_ev, i_repr]):
                            t_shift_listE[iii] = t_shift_listE[ii] + tshift_arrayE[i_st, i_ev, i_repr]
                        elif not num.isnan(tshift_arrayE[i_st, i_ev, i_repr]):
                            t_shift_listE[iii] = t_shift_listE[ii] - tshift_arrayE[i_st, i_repr, i_ev]


            # test if there are still some shift times unset, if so use shift times
            # relative to other than representative event.
            
            if 'Z' in test_comps:
                if True in num.isnan(t_shift_listZ):
                    for i0, i_ev0 in enumerate(i_ev_list):
                        if not num.isnan(t_shift_listZ[i0]):
                            continue
                        else:
                            for i1, i_ev1 in enumerate(i_ev_list):
                                if i0 == i1:
                                    continue
                                dt = tshift_arrayZ[i_st, i_ev1, i_ev0]
                                #dt2 = tshift_array[i_ph, i_st, i_ev0, i_ev1]
                                if not num.isnan(dt):
                                    t_shift_listZ[i0] = t_shift_listZ[i1] - dt

            if 'N' in test_comps:
                if True in num.isnan(t_shift_listN):
                    for i0, i_ev0 in enumerate(i_ev_list):
                        if not num.isnan(t_shift_listN[i0]):
                            continue
                        else:
                            for i1, i_ev1 in enumerate(i_ev_list):
                                if i0 == i1:
                                    continue
                                dt = tshift_arrayN[i_st, i_ev1, i_ev0]
                                #dt2 = tshift_array[i_ph, i_st, i_ev0, i_ev1]
                                if not num.isnan(dt):
                                    t_shift_listN[i0] = t_shift_listN[i1] - dt

            if 'E' in test_comps:
                if True in num.isnan(t_shift_listZ):
                    for i0, i_ev0 in enumerate(i_ev_list):
                        if not num.isnan(t_shift_listE[i0]):
                            continue
                        else:
                            for i1, i_ev1 in enumerate(i_ev_list):
                                if i0 == i1:
                                    continue
                                dt = tshift_arrayE[i_st, i_ev1, i_ev0]
                                #dt2 = tshift_array[i_ph, i_st, i_ev0, i_ev1]
                                if not num.isnan(dt):
                                    t_shift_listE[i0] = t_shift_listE[i1] - dt

            for iii, (i_ev, ev) in enumerate(cl_events):
                arr = arrivals[0, i_st, i_ev]
                twd = get_twd(i_st, i_ev, phases, cc_cnf, arrivals)
                twd_ = twd
                twd = twd[0]

                try:
                    if 'Z' in test_comps:
                        tr_Z = p.all(tmin=arr-cc_cnf.filtertmin, tmax=arr+cc_cnf.filtertmax,
                            trace_selector=lambda tr: str(tr.station)== str(st.station) and
                            tr.channel.endswith('Z'))

                        for itr, tr in enumerate(tr_Z):
                            if tr.tmin < arr and tr.tmax > arr:
                                useZ = itr

                        tr_Z = tr_Z[useZ]

                    if 'N' in test_comps:
                        tr_N = p.all(tmin=arr-cc_cnf.filtertmin, tmax=arr+cc_cnf.filtertmax,
                            trace_selector=lambda tr: str(tr.station) == str(st.station) and
                            tr.channel.endswith(comp_dict['N']))

                        for itr, tr in enumerate(tr_N):
                            if tr.tmin < arr and tr.tmax > arr:
                                useN = itr

                        tr_N = tr_N[useN]

                    if 'E' in test_comps:

                        tr_E = p.all(tmin=arr-cc_cnf.filtertmin, tmax=arr+cc_cnf.filtertmax,
                             trace_selector=lambda tr: str(tr.station) == str(st.station) and
                             tr.channel.endswith(comp_dict['E']))

                        for itr, tr in enumerate(tr_E):
                            if tr.tmin < arr and tr.tmax > arr:
                                 useE = itr

                        tr_E = tr_E[useE]

                except:
                    logger.debug('WF cluster plot: %s.%s %s - no data found' %
                                  (st.network, st.station, ev.name))
                    continue


                if cc_cnf.downsample_to:
                    if 'Z' in test_comps:
                        tr_Z.downsample_to(cc_cnf.downsample_to)
                    if 'N' in test_comps:
                        tr_N.downsample_to(cc_cnf.downsample_to)
                    if 'E' in test_comps:    
                        tr_E.downsample_to(cc_cnf.downsample_to)

                if 'Z' in test_comps:
                    tr_Z.bandpass(int(cc_cnf.bp[0]), cc_cnf.bp[1], cc_cnf.bp[2])
                if 'N' in test_comps:
                    tr_N.bandpass(int(cc_cnf.bp[0]), cc_cnf.bp[1], cc_cnf.bp[2])
                if 'E' in test_comps:
                    tr_E.bandpass(int(cc_cnf.bp[0]), cc_cnf.bp[1], cc_cnf.bp[2])

                if cc_cnf.snr_calc:
                    snr = comp_snr(tr_Z, arr, twd_, st.station, ev.name)
                else:
                    snr = 9999

                if snr > cc_cnf.snr_thresh:
                    #print('before %s %s %s ' % (tr_Z.tmin, len(tr_Z.ydata), arr))
                    t_buffer = cl_cnf.t_buffer_wfplot  # 20
                    # print('evtime', ev.time)
                    # print('arr', arr)
                    #print('t_shift_list', num.nanmax(t_shift_list), num.nanmin(t_shift_list))
                    #input()
                    #t_buffer = num.nanmax(t_shift_list), num.nanmin(t_shift_list)
                    '''
                    try:
                        if 'Z' in test_comps:
                            tr_Z.chop(tmin=arr - t_buffer, tmax=arr + twd[1]+t_buffer)
                            logger.debug('first chop %s %s %s ' % (tr_Z.tmin, tr_Z.tmax, len(tr_Z.ydata)))

                        if 'N' in test_comps:
                            tr_N.chop(tmin=arr - t_buffer, tmax=arr + twd[1]+t_buffer)
                        if 'E' in test_comps:
                            tr_E.chop(tmin=arr - t_buffer, tmax=arr + twd[1]+t_buffer)

                    except trace.NoData:
                        logger.info('Trace to short for cutting')
                        continue
                    '''
                    if 'Z' in test_comps:
                        dtZ = t_shift_listZ[iii]
                    if 'N' in test_comps:
                        dtN = t_shift_listN[iii]
                    if 'E' in test_comps:
                        dtE = t_shift_listE[iii]

                    if 'Z' in test_comps and not num.isnan(dtZ):
                        trZ_ = tr_Z.copy()
                        trZ_.shift(dtZ)
                        trZ_.set_codes(location=str(cl))
                        trZ_.ydata = trZ_.ydata/ num.max(trZ_.ydata)
                        trZs.append(trZ_)

                    if 'N' in test_comps and not num.isnan(dtN):
                        trN_ = tr_N.copy()
                        trN_.shift(dtN)
                        trN_.set_codes(location=str(cl))
                        trN_.ydata = trN_.ydata/ num.max(trN_.ydata)
                        trNs.append(trN_)

                    if 'E' in test_comps and not num.isnan(dtE):
                        trE_ = tr_E.copy()
                        trE_.shift(dtE)
                        trE_.set_codes(location=str(cl))
                        trE_.ydata = trE_.ydata/ num.max(trE_.ydata)
                        trEs.append(trE_)

                else:
                    logger.info('snr not met')
                    continue

        if 'Z' in test_comps:
            trace.snuffle(trZs)
            #io.save(trZs, './results/%s_traces_Z_aligned.yaff' % st.station,
            #        format='yaff')
        if 'N' in test_comps:
            trace.snuffle(trNs)
            #io.save(trNs, './results/%s_traces_N_aligned.yaff' % st.station,
            #        format='yaff')
        if 'E' in test_comps:
            trace.snuffle(trEs)
            #io.save(trEs, './results/%s_traces_E_aligned.yaff' % st.station,
            #        format='yaff')


def export_stacked_traces(resultcat, waveform_dir, arrivals, #coef_array,
                          #tshift_array, 
                          station_list,
                          prep_grond, debug_stacking, comps,
                          method_string, cc_cnf, cl_cnf, work_dir,
                          preprocess_stacked_traces,
                          representative_events):
    

    os.makedirs('./stacked_waveforms/%s/' % (method_string), exist_ok=True)
    
    logger.info('Stacking and saving waveforms for all clusters.')
    grond_events = []

    cluster_label_set = list(set([ev.extras['cluster_number'] for ev in resultcat]))
    nclusters = len(cluster_label_set)
    nstations = len(station_list)

    test_comps = [c[-1] for c in cc_cnf.components]
    # translate non-oriented horizontal components to NE for plotting ONLY! No data rotation
    test_comps_input = test_comps.copy()

    for icomp, comp in enumerate(test_comps):
        if comp == '1':
            test_comps[icomp] = 'E'
        elif comp == '2':
            if '1' in test_comps_input:
                test_comps[icomp] = 'N'
            elif '3' in test_comps_input:
                test_comps[icomp] = 'E'
        elif comp == '3':
            test_comps[icomp] = 'N'

    comp_dict = {}
    for icomp, comp in enumerate(test_comps):
        comp_dict[comp] = test_comps_input[icomp]

    if 'Z' in test_comps_input:
        tshift_arrayZ = num.load('%s/precalc_arrays/cctshifts_array_Z.npy' % work_dir)
    if 'N' in test_comps_input:
        tshift_arrayN = num.load('%s/precalc_arrays/cctshifts_array_N.npy' % work_dir)
    if 'E' in test_comps_input:
        tshift_arrayE = num.load('%s/precalc_arrays/cctshifts_array_E.npy' % work_dir)
    if '1' in test_comps_input:
        tshift_arrayE = num.load('%s/precalc_arrays/cctshifts_array_1.npy' % work_dir)
    if '2' in test_comps_input:
        if '1' in test_comps_input:
            tshift_arrayN = num.load('%s/precalc_arrays/cctshifts_array_2.npy' % work_dir)
        elif '3' in test_comps_input:
            tshift_arrayE = num.load('%s/precalc_arrays/cctshifts_array_2.npy' % work_dir)
    if '3' in test_comps_input:
        tshift_arrayN = num.load('%s/precalc_arrays/cctshifts_array_2.npy' % work_dir)

    stations_and_indices = [(i_st, st) for i_st, st in enumerate(station_list)]
    
    nstations = len(station_list)

    if cl_cnf.wf_cluster_choice:
        set_clusters = cl_cnf.wf_cluster_choice
    else:
        set_clusters = list(set([ev.extras['cluster_number'] for ev in resultcat 
                             if ev.extras['cluster_number'] != -1]))
    set_clusters.sort()
    nclusters = len(set_clusters)
    phases = cc_cnf.phase
    n_phases = len(phases)

    logger.info('Found %s clusters, %s stations and %s phases.' % 
                 (nclusters, nstations, n_phases))

    result_dict = {}
    for cl in set_clusters:
        result_dict[cl] = [(i_ev, ev) for i_ev, ev in enumerate(resultcat)
                           if ev.extras['cluster_number'] == cl]

    for i_st, st in stations_and_indices:

        logger.info('Preparing stacked waveforms for %s.%s' % (st.network, st.station))

        if not '*' in waveform_dir:
            #p = pile.make_pile(waveform_dir, regex='%s.%s' % (st.network, st.station), 
            #                   show_progress=False)
            p = pile.make_pile(waveform_dir, regex='%s' % (st.station),
                               show_progress=False, fileformat='detect')
        else:
            files = glob.glob('%s/*%s.%s*' % (waveform_dir, st.network, st.station))
            p = pile.make_pile(files, show_progress=False)

        if not p:
            continue

        for i_cl, cl in enumerate(set_clusters):
            
            trZs = []
            trEs = []
            trNs = []

            cl_events = result_dict[cl]
            nev = len(cl_events)
            ntr = 0
            plot_last = False
            plot_last_list = []

            repre_ev = [ev for ev in representative_events if ev.extras['cluster_number'] == cl][0]
            i_repr = [i for i,ev in enumerate(resultcat) if ev.name == repre_ev.name][0]  ## cl_events?

            ii = [ii for ii,iiev in enumerate(cl_events) if iiev[1].name == repre_ev.name][0]


            # get list of time shifts for current cluster:
            t_shift_listZ = num.empty((nev))
            t_shift_listZ.fill(num.nan)
            t_shift_listN = num.empty((nev))
            t_shift_listN.fill(num.nan)
            t_shift_listE = num.empty((nev))
            t_shift_listE.fill(num.nan)

            # fill tshift of representative event, others are filled relative to this event
            t_shift_listZ[ii] = -repre_ev.time  # -arrivals[0, i_st, i_repr]  # -repre_ev.time
            t_shift_listN[ii] = -repre_ev.time  # -arrivals[1, i_st, i_repr]  # -repre_ev.time
            t_shift_listE[ii] = -repre_ev.time  # -arrivals[1, i_st, i_repr]  # -repre_ev.time

            i_ev_list = [clev[0] for clev in cl_events]

            # fill all that have cc and tshift value relative to representative event
            for iii, (i_ev, ev) in enumerate(cl_events):
                if iii == i_repr:
                    continue
                else:
                    if 'Z' in test_comps:
                        if not num.isnan(tshift_arrayZ[i_st, i_ev, i_repr]):
                            t_shift_listZ[iii] = t_shift_listZ[ii] + tshift_arrayZ[i_st, i_ev, i_repr]
                        elif not num.isnan(tshift_arrayZ[i_st, i_repr, i_ev]):
                            t_shift_listZ[iii] = t_shift_listZ[ii] - tshift_arrayZ[i_st, i_repr, i_ev]
                    if 'N' in test_comps:
                        if not num.isnan(tshift_arrayN[i_st, i_ev, i_repr]):
                            t_shift_listN[iii] = t_shift_listN[ii] + tshift_arrayN[i_st, i_ev, i_repr]
                        elif not num.isnan(tshift_arrayN[i_st, i_repr, i_ev]):
                            t_shift_listN[iii] = t_shift_listN[ii] - tshift_arrayN[i_st, i_repr, i_ev]
                    if 'E' in test_comps:
                        if not num.isnan(tshift_arrayE[i_st, i_ev, i_repr]):
                            t_shift_listE[iii] = t_shift_listE[ii] + tshift_arrayE[i_st, i_ev, i_repr]
                        elif not num.isnan(tshift_arrayE[i_st, i_ev, i_repr]):
                            t_shift_listE[iii] = t_shift_listE[ii] - tshift_arrayE[i_st, i_repr, i_ev]

        
            # test if there are still some shift times unset, if so use shift times
            # relative to other than first events.
            


            if 'Z' in test_comps:
                if True in num.isnan(t_shift_listZ):
                    for i0, i_ev0 in enumerate(i_ev_list):
                        if not num.isnan(t_shift_listZ[i0]):
                            continue
                        else:
                            for i1, i_ev1 in enumerate(i_ev_list):
                                if i0 == i1:
                                    continue
                                dt = tshift_arrayZ[i_st, i_ev1, i_ev0]
                                #dt2 = tshift_array[i_ph, i_st, i_ev0, i_ev1]
                                if not num.isnan(dt):
                                    t_shift_listZ[i0] = t_shift_listZ[i1] - dt

            if 'N' in test_comps:
                if True in num.isnan(t_shift_listN):
                    for i0, i_ev0 in enumerate(i_ev_list):
                        if not num.isnan(t_shift_listN[i0]):
                            continue
                        else:
                            for i1, i_ev1 in enumerate(i_ev_list):
                                if i0 == i1:
                                    continue
                                dt = tshift_arrayN[i_st, i_ev1, i_ev0]
                                #dt2 = tshift_array[i_ph, i_st, i_ev0, i_ev1]
                                if not num.isnan(dt):
                                    t_shift_listN[i0] = t_shift_listN[i1] - dt

            if 'E' in test_comps:
                if True in num.isnan(t_shift_listZ):
                    for i0, i_ev0 in enumerate(i_ev_list):
                        if not num.isnan(t_shift_listE[i0]):
                            continue
                        else:
                            for i1, i_ev1 in enumerate(i_ev_list):
                                if i0 == i1:
                                    continue
                                dt = tshift_arrayE[i_st, i_ev1, i_ev0]
                                #dt2 = tshift_array[i_ph, i_st, i_ev0, i_ev1]
                                if not num.isnan(dt):
                                    t_shift_listE[i0] = t_shift_listE[i1] - dt


            for iii, (i_ev, ev) in enumerate(cl_events):
                arr = arrivals[0, i_st, i_ev]
                twd = get_twd(i_st, i_ev, phases, cc_cnf, arrivals)
                twd_ = twd
                twd = twd[0]

                try:
                    if 'Z' in test_comps:
                        tr_Zs = p.all(tmin=arr-cc_cnf.filtertmin, tmax=arr+cc_cnf.filtertmax,
                                trace_selector=lambda tr: str(tr.station) == str(st.station) and
                                tr.channel.endswith('Z'))

                        for itr, tr in enumerate(tr_Zs):
                            if tr.tmin < arr and tr.tmax > arr:
                                useZ = itr
                        
                        tr_Z = tr_Zs[useZ]

                    if 'N' in test_comps:
                        tr_Ns = p.all(tmin=arr-cc_cnf.filtertmin, tmax=arr+cc_cnf.filtertmax,
                                trace_selector=lambda tr: str(tr.station) == str(st.station) and
                                tr.channel.endswith(comp_dict['N']))
                        
                        for itr, tr in enumerate(tr_Ns):
                            if tr.tmin < arr and tr.tmax > arr:
                                useN = itr

                        tr_N = tr_Ns[useN]


                    if 'E' in test_comps:
                        tr_Es = p.all(tmin=arr-cc_cnf.filtertmin, tmax=arr+cc_cnf.filtertmax,
                                trace_selector=lambda tr: str(tr.station) == str(st.station) and
                                tr.channel.endswith(comp_dict['E']))

                        for itr, tr in enumerate(tr_Es):
                            if tr.tmin < arr and tr.tmax > arr:
                                useE = itr
                    
                        tr_E = tr_Es[useE]

                except:
                    logger.debug('WF exporting: %s.%s %s - no data found' %
                                 (st.network, st.station, ev.name))
                    continue

                #if not arr > tr_Z.tmin and not arr < tr_Z.tmax:
                #    print('arr error')
                #    continue

                if preprocess_stacked_traces:
                    if cc_cnf.downsample_to:
                        if 'Z' in test_comps:
                            tr_Z.downsample_to(cc_cnf.downsample_to)
                        if 'N' in test_comps:
                            tr_N.downsample_to(cc_cnf.downsample_to)
                        if 'E' in test_comps:    
                            tr_E.downsample_to(cc_cnf.downsample_to)

                    if 'Z' in test_comps:
                        tr_Z.bandpass(int(cc_cnf.bp[0]), cc_cnf.bp[1], cc_cnf.bp[2])
                    if 'N' in test_comps:
                        tr_N.bandpass(int(cc_cnf.bp[0]), cc_cnf.bp[1], cc_cnf.bp[2])
                    if 'E' in test_comps:
                        tr_E.bandpass(int(cc_cnf.bp[0]), cc_cnf.bp[1], cc_cnf.bp[2])

                if cc_cnf.snr_calc:
                    snr = comp_snr(tr_Z, arr, twd_, st.station, ev.name)
                else:
                    snr = 9999

                if snr > cc_cnf.snr_thresh:
                    t_buffer = cl_cnf.t_buffer_wfplot

                    '''
                    try:
                        if 'Z' in test_comps:
                            tr_Z.chop(tmin=arr - t_buffer, tmax=arr + twd[1]+t_buffer)
                            logger.debug('first chop %s %s %s ' % (tr_Z.tmin, tr_Z.tmax, len(tr_Z.ydata)))

                        if 'N' in test_comps:
                            tr_N.chop(tmin=arr - t_buffer, tmax=arr + twd[1]+t_buffer)
                        if 'E' in test_comps:
                            tr_E.chop(tmin=arr - t_buffer, tmax=arr + twd[1]+t_buffer)

                    except trace.NoData:
                        logger.info('Trace to short for cutting')
                        continue
                    '''


                    if 'Z' in test_comps:
                        dtZ = t_shift_listZ[iii]
                    if 'N' in test_comps:
                        dtN = t_shift_listN[iii]
                    if 'E' in test_comps:
                        dtE = t_shift_listE[iii]


                    if 'Z' in test_comps and not num.isnan(dtZ):
                        trZ_ = tr_Z.copy()
                        trZ_.shift(dtZ)
                        trZ_.set_codes(location=str(cl))
                        trZ_.ydata = trZ_.ydata/ num.max(trZ_.ydata)
                        trZs.append(trZ_)

                    if 'N' in test_comps and not num.isnan(dtN):
                        trN_ = tr_N.copy()
                        trN_.shift(dtN)
                        trN_.set_codes(location=str(cl))
                        trN_.ydata = trN_.ydata/ num.max(trN_.ydata)
                        trNs.append(trN_)

                    if 'E' in test_comps and not num.isnan(dtE):
                        trE_ = tr_E.copy()
                        trE_.shift(dtE)
                        trE_.set_codes(location=str(cl))
                        trE_.ydata = trE_.ydata/ num.max(trE_.ydata)
                        trEs.append(trE_)


                else:
                    logger.info('below snr')
                    continue

            if 'Z' in test_comps:
                if trZs:
                    io.save(trZs, './stacked_waveforms/%s/cl%s/aligned/%s.%s_cl%s_Z_aligned.mseed' 
                        % (method_string, cl, st.network, st.station, cl), format='mseed')
                
                    n_ev = len(trZs)

                    for itr, trZ in enumerate(trZs):
                        if itr == 0:
                            trZ_stack = trZ
                        else:
                            trZ_stack.add(trZ)

                    trZ_stack.ydata /= num.max(abs(trZ_stack.ydata))
                    
                    if debug_stacking is True:
                        cp_trZ_list = trZs
                        #for itr, tr in enumerate(trZs):
                        #    tr.set_location(str(itr))
                        trace.snuffle(trZs)
                        trace.snuffle([trZ_stack])

                    if prep_grond:
                        # to use grond we shift here by origin time of representative event in cluster
                        evcl = repre_ev
                        trZ_stack.shift(evcl.time)
                    
                    io.save(trZ_stack, './stacked_waveforms/%s/cl%s/stacks/stack_cl%s_%s.%s_Z.mseed' 
                            % (method_string, cl, cl, st.network, st.station))

                    logger.info(' Z component, cluster %s, station %s.%s: Saved all aligned and stacked traces' % (cl, st.network, st.station)
                                + ' in directory ./stacked_waveforms/.')

            if 'N' in test_comps:
                if trNs:
                    io.save(trNs, './stacked_waveforms/%s/cl%s/aligned/%s.%s_cl%s_%s_aligned.mseed'
                        % (method_string, cl, st.network, st.station, cl, comp_dict['N']), format='mseed')
                
                    n_ev = len(trNs)

                    for itr, trN in enumerate(trNs):
                        if itr == 0:
                            trN_stack = trN
                        else:
                            trN_stack.add(trN)

                    trN_stack.ydata /= num.max(abs(trN_stack.ydata))
                    
                    if debug_stacking is True:
                        cp_trN_list = trNs
                        #for itr, tr in enumerate(trNs):
                        #    tr.set_location(str(itr))
                        trace.snuffle(trNs)
                        trace.snuffle([trN_stack])

                    if prep_grond:
                        # to use grond we shift here by origin time of representative event in cluster
                        evcl = repre_ev
                        trN_stack.shift(evcl.time)
                    
                    io.save(trN_stack, './stacked_waveforms/%s/cl%s/stacks/stack_cl%s_%s.%s_%s.mseed'
                            % (method_string, cl, cl, st.network, st.station, comp_dict['N'],))

                    logger.info(' %s component, cluster %s, station %s.%s: Saved all aligned and stacked traces' % (comp_dict['N'], cl, st.network, st.station)
                                + ' in directory ./stacked_waveforms/.')

            if 'E' in test_comps:
                if trEs:
                    io.save(trEs, './stacked_waveforms/%s/cl%s/aligned/%s.%s_cl%s_%s_aligned.mseed'
                        % (method_string, cl, st.network, st.station, cl, comp_dict['E'],), format='mseed')
                
                    n_ev = len(trEs)

                    for itr, trE in enumerate(trEs):
                        if itr == 0:
                            trE_stack = trE
                        else:
                            trE_stack.add(trE)

                    trE_stack.ydata /= num.max(abs(trE_stack.ydata))
                    
                    if debug_stacking is True:
                        cp_trE_list = trEs
                        #for itr, tr in enumerate(trEs):
                        #    tr.set_location(str(itr))
                        trace.snuffle(trEs)
                        trace.snuffle([trE_stack])

                    if prep_grond:
                        # to use grond we shift here by origin time of representative event in cluster
                        evcl = repre_ev
                        trE_stack.shift(evcl.time)
                    
                    io.save(trE_stack, './stacked_waveforms/%s/cl%s/stacks/stack_cl%s_%s.%s_%s.mseed'
                            % (method_string, cl, cl, st.network, st.station, comp_dict['E'],))

                    logger.info(' %s component, cluster %s, station %s.%s: Saved all aligned and stacked traces' % (comp_dict['E'], cl, st.network, st.station)
                                + ' in directory ./stacked_waveforms/.')


            if prep_grond:
                if trZs or trNs or trEs:
                    # to use grond (mainly for twd selection and restitution of data)
                    # we shift here by origin time of first event in cluster and
                    # save an according event file in pyrocko format for each cluster
                    if 'stack_cl_%s' % cl not in [ev.name for ev in grond_events]:                          
                        grond_events.append(model.Event(lat=evcl.lat, lon=evcl.lon, depth= evcl.depth,
                                        time=evcl.time, name='stack_cl_%s' % cl))

    if prep_grond:
        if trZs or trNs or trEs:
            os.makedirs('./stack_events/%s/' % (method_string), exist_ok=True)
            model.dump_events(set(grond_events), filename='./stack_events/%s/events.pf' %(method_string))


def harmonize_cluster_labels(file_list, work_dir, autosort=True, single_freq=True):

    cluster_lists =[]
    n_clusters =[]
    c_labels =[]
    method_strings=[]

    if autosort:
        if single_freq:
            file_list = sorted(file_list)
        logger.info('file list for harmonization: %s ' % file_list)

    for i_f, file in enumerate(file_list):
        cat = model.load_events(file)
        method_string = os.path.split(file)[-1][4:-5]

        cluster_list = num.array([ev.extras['cluster_number'] for ev in cat])
        c_label = list(range(-1,max(cluster_list)+1))
        n_cluster = len(c_label)

        cluster_lists.append(cluster_list)
        n_clusters.append(n_cluster)
        c_labels.append(c_label) 
        method_strings.append(method_string)
        
    max_std = 0 
    # loop over consecutive catalog file pairs
    for i_file in range(len(file_list)-1):
        logger.debug('harmonizing %s' % file_list[i_file+1])
        if logger.isEnabledFor(logging.DEBUG): #debug_harmonize:

            fig_arr,ax_arr = plt.subplots(1,3,figsize=(10,6))

        if (len(set(cluster_lists[i_file])) == 1 or
                len(set(cluster_lists[i_file+1])) == 1):
            logger.info("No clusters found for %s. Skip in harmonization" % method_strings[i_file])
            continue

        cluster1 = cluster_lists[i_file]
        cluster2 = cluster_lists[i_file+1]

        n_cluster1 = n_clusters[i_file] 
        n_cluster2 = n_clusters[i_file+1]

        #setup confusion array
        confu_array = num.zeros((n_cluster1,n_cluster2))

        if logger.isEnabledFor(logging.DEBUG):#debug_harmonize:

            logger.debug(num.shape(confu_array))
            test_array = num.zeros_like(confu_array)
            

        for i_c in range(len(cat)):
            confu_array[int(cluster1[i_c]+1),int(cluster2[i_c]+1)] += 1
            if logger.isEnabledFor(logging.DEBUG): #debug_harmonize:
                test_array[int(cluster1[i_c]+1),int(cluster2[i_c]+1)] = 1

        harmo_dict ={}
        common_dict ={}

        if logger.isEnabledFor(logging.DEBUG): #debug_harmonize: 

            ax_arr[0].imshow(test_array)
            ax_arr[1].imshow(confu_array, vmax = 20)

            num.set_printoptions(suppress=True, linewidth=130)
            logger.debug('------confusion array-----')
            logger.debug(confu_array)
            logger.debug('-----------')

        #harmonization of cluster labels
        cluster_harmonized = num.zeros_like(cat)
        max_std = max(max(c_labels[i_file]),max_std)
        n_new = 1

        for i_lab, lab in enumerate(c_labels[i_file+1]):
            idx = confu_array[:,i_lab].argsort()[::-1]
            cluster_corrected = num.array(confu_array[:,i_lab].argsort()[::-1])-1
            common_events = confu_array[:,i_lab][idx]

            #sort zero in confusion row so class -1 is up front after non-zero values
            zero_idx = num.where(common_events == 0)[0]
            if zero_idx.size != 0:
                cluster_corrected[min(zero_idx):] = sorted(cluster_corrected[zero_idx])

            logger.debug('similar old cluster ids (sorted) %s' % cluster_corrected)
            logger.debug('number of common events %s' % common_events)

            if cluster_corrected[0] == -1 and lab == -1:
                new_cluster = cluster_corrected[0]
                n_common = common_events[0]
                #case_flag = 'a'
            elif cluster_corrected[0] !=-1:
                new_cluster = cluster_corrected[0]
                n_common = common_events[0]
                #case_flag = 'b'
            elif cluster_corrected[0] == -1 and common_events[1] != 0:
                new_cluster = cluster_corrected[1]
                n_common = common_events[1]
                #case_flag = 'c'
            else:
                n_new += 1
                new_cluster = max_std + n_new
                n_common = common_events[0]
                #case_flag = 'd'

            logger.debug(' %s --> %s ' % (lab, new_cluster))

            harmo_dict[lab] = new_cluster
            common_dict[lab] = n_common

        harmos = [harmo_dict[lab] for lab in sorted(harmo_dict.keys())]
        harmos_sorted = sorted(num.array(harmos))
        commons = [common_dict[lab] for lab in sorted(harmo_dict.keys())]

        if len(harmos) != len(set(harmos)):

            logger.debug('--------correct for doublet harmonization labels-------')
            logger.debug(harmos)

            #get duplicate labels
            dup_idx = num.where(num.diff(harmos_sorted)==0)[0]
            doublet_labels = list(set([harmos_sorted[idx] for idx in dup_idx]))

            for dupl in doublet_labels:
                dupl_idx = num.where(harmos == dupl)[0]
                dupl_keys =num.array([num.array(sorted(harmo_dict.keys()))[idx] for idx in dupl_idx])

                common_list = [common_dict[dup_key] for dup_key in dupl_keys]

                logger.debug('new clusters assigned to old cluster %i: %s' % (dupl, dupl_keys[num.argsort(common_list)]))
                logger.debug('assigning new clusters...')
                sorted_keys = dupl_keys[num.argsort(common_list)]

                for key in sorted_keys:
                    if key == sorted_keys[-1]:
                        logger.debug('    %s --> %s ' % (key, dupl))
                        continue

                    else:
                        logger.debug('    %s --> %s ' % (key, (max_std + n_new)))
                        harmo_dict[key] = max_std + n_new
                        n_new += 1

        logger.debug(harmo_dict)

        for cluster_harm in harmo_dict.keys():
            cluster_harmonized[cluster2==cluster_harm] = harmo_dict[cluster_harm]

        #update harmonized cluster 
        cluster_lists[i_file+1] = cluster_harmonized
        c_labels[i_file+1] = list(range(-1,max(cluster_harmonized)+1))
        n_clusters[i_file+1] = len(c_labels[i_file+1])

        if logger.isEnabledFor(logging.DEBUG): #debug_harmonize:

            confu_array = num.zeros((n_clusters[i_file],
                                       max(cluster_harmonized)+2))

            for i_c in range(len(cat)):
                confu_array[cluster1[i_c]+1,cluster_harmonized[i_c]+1] += 1

            ax_arr[2].imshow(confu_array, vmax=20)

            for i in [0,1]:
                ax_arr[i].set_ylabel('reference clusters (at left in sankey)')
                ax_arr[i].set_xlabel('new clusters')

            ax_arr[2].set_ylabel('reference clusters (at left in sankey)')
            ax_arr[2].set_xlabel('harmonized new clusters (at right in sankey)')

            fig_arr.suptitle('confusion arrays\n%s\n%s' % (
                        os.path.split(file_list[i_file])[-1],
                        os.path.split(file_list[i_file+1])[-1]))

            fig_arr.tight_layout()
            plt.show()

        #save harmonized catalog
        if single_freq:
            outfile =  os.path.join(work_dir, 'results', 'cat_%s.yaml' % method_strings[i_file+1])
        else:
            outfile=file_list[i_file+1]

        catalog_output(cat, cluster_lists[i_file+1], outfile=outfile)


def fruchtermann(catalog, dists, cluster_params, method_string, cc_cnf, netsim_cnf, cl_cnf, work_dir, plformat):

    G = nx.Graph()

    param1, pts_min, param_name = get_cluster_params(cluster_params,cl_cnf)

    for i_ev, ev1 in enumerate(catalog):
        for j_ev, ev2 in enumerate(catalog):
            if (i_ev > j_ev and 0 < dists[i_ev,j_ev] <= 1-netsim_cnf.cc_thresh):
                G.add_edge(str(i_ev), str(j_ev), weight=1-dists[i_ev, j_ev])

    sim_connect =[(u, v) for (u, v, d) in G.edges(data=True) if d['weight'] >= 1-float(param1)]

    mags = [ev.magnitude if ev.magnitude else num.nan for ev in catalog]
    mag_min = num.nanmin(mags)
    mag_max = num.nanmax(mags)


    if num.isnan(mag_min):
        mag_min = 2
        no_mags = True
    else:
        no_mags = False

    if num.isnan(mag_max):
        mag_max = 2

    mag_substituted = False
    for ev in catalog:
        if ev.magnitude == None:
            ev.magnitude = mag_min/2
            mag_substituted = True

    mags = [ev.magnitude for ev in catalog]

    if no_mags:
        logger.warning('WARNING: Catalog without magnitudes. Magnitudes set to 1 for plotting.')
    elif mag_substituted:
        logger.warning('WARNING: Not all events in catalog have magnitudes.Missing magnitudes were \
                        set to min(mag)/2 for plotting. %s %s' % (mag_min, mag_max))

    mags = num.array([ev.magnitude for ev in catalog])

    log_tuple,scale_meth = get_mag_scale_method(cl_cnf)

    mag_analyzed = num.array([catalog[int(i)].magnitude for i in G.nodes()])

    node_colors = [catalog[int(idx)].extras['color'] for idx in G.nodes()]

    e_colors = [catalog[int(i[0])].extras['color']
                if catalog[int(i[0])].extras['color'] == catalog[int(i[1])].extras['color']
                else 'grey'
                for i in G.edges(data=True) if i[2]['weight'] >= 1-float(param1)]
    
    #calculate node distribution
    k = 1/num.sqrt(len(list(G.nodes())))*cl_cnf.frucht_k_factor # default value by setting k_factor = 1

    pos = nx.spring_layout(G,k=k, seed=10001)

    freqs = '%.2f - %.2f Hz' % (cc_cnf.bp[1],cc_cnf.bp[2])
    title='Bandpass: %s     MinPts: %i      %s: %.3f' % (
                    freqs,int(pts_min),param_name,float(param1))

    if cl_cnf.frucht_plot_lib == 'plotly':
        node_x = []
        node_y = []
        idx = []
        for key in pos:
            node_x.append(pos[key][0])
            node_y.append(pos[key][1])
            idx.append(int(key))

        names = ['%s (cl %i)' %(catalog[i].name,catalog[i].extras['cluster_number']) for i in idx]

        ply_colors = [catalog[i].extras['color'] for i in idx]

        mag_scale = mag_scale_log(mag_analyzed, log_tuple,mag_max,mag_min,plotlib=cl_cnf.frucht_plot_lib,method=scale_meth)

        node_trace = go.Scatter(
            x=node_x, y=node_y,
            mode='markers',
            hoverinfo='text',
            marker=dict(
                color=ply_colors,
                size=mag_scale),
                line_width=2)

        node_trace.text = names

        fig = go.Figure(layout=go.Layout(
                        title=title,
                        titlefont_size=16,
                        showlegend=False,
                        hovermode='closest',
                        margin=dict(b=20,l=5,r=5,t=40),
                        xaxis=dict(showgrid=False, zeroline=False,  showticklabels=False),
                        yaxis=dict(showgrid=False, zeroline=False,  showticklabels=False))
                        )

        cluster_list = num.array(sorted(list(set([ev.extras['cluster_number'] for ev in catalog]))))
        n_cluster = len(cluster_list)

        edg_x = [[] for e in range(n_cluster)]
        edg_y = [[] for e in range(n_cluster)]
        edg_color = []

        edge_colors_hex= [util_clusty.cluster_to_color(i_cl) for i_cl in cluster_list]
        edge_colors =[util_clusty.hex_to_rgb(col, out='plotly_rgb', alpha = 0.1) for col in edge_colors_hex]

        for edge in G.edges(data=True):
            if edge[2]['weight'] >= 1-float(param1):
                x0, y0 = pos[edge[0]]
                x1, y1 = pos[edge[1]]

                clust1 = catalog[int(edge[0])].extras['cluster_number']
                clust2 = catalog[int(edge[1])].extras['cluster_number']

                if clust1 == clust2:
                    i = num.where(cluster_list==clust1)[0][0]
                else:
                    i = 0

                edg_x[i].extend([x0,x1,None])
                edg_y[i].extend([y0,y1,None])

        for cluster in range(n_cluster):
            fig.add_trace(go.Scatter(
                x=edg_x[cluster], y=edg_y[cluster],
                line_color=edge_colors[cluster],
                line_width=0.5,
                hoverinfo='none',
                mode='lines'))


        fig.add_trace(node_trace)
    
        plotly.offline.plot(fig, filename = '%s/results/network_%s.html' % (work_dir, method_string),
                             auto_open=cl_cnf.frucht_show)

    elif cl_cnf.frucht_plot_lib in ['matplotlib','plt']:

        fig,ax = plt.subplots(figsize=(10,7))

        mag_scale = mag_scale_log(mag_analyzed, log_tuple,mag_max,mag_min,plotlib=cl_cnf.frucht_plot_lib,method=scale_meth)/2
    
        #draw nodes and edges
        nx.draw_networkx_nodes(G, pos, node_size=mag_scale, node_color=node_colors, alpha=0.5)
        nx.draw_networkx_edges(G, pos, edgelist=sim_connect, width=1,alpha=0.3,edge_color=e_colors)
        ax.set_title(title)
        plt.axis('off')

        if cl_cnf.frucht_show:
            plt.show()

        fig.savefig('%s/results/network_%s.%s' % (work_dir, method_string,plformat))
        plt.close()


def sankey_multi(file_list, work_dir='.', fn=False, debug=False, single_freq=True, plot_labels=True):
    '''
    order in file_list defines the position of cluster result in cluster change plot
    --> catalog based files to be included

    only usable for same input catalog
    '''

    alphabet = list(string.ascii_uppercase)

    n_files = len(file_list)
    cluster_lists =[]
    n_clusters =[]
    c_labels =[]
    clabels_clean = []

    if single_freq:
        file_list = sorted(file_list)
    sources=[]
    targets=[]
    values=[]
    colors =[]
    labels=[]

    for i_f, file in enumerate(file_list):
        
        cat = model.load_events(file)
        cluster_list = num.array([ev.extras['cluster_number'] for ev in cat])
        c_label = list(range(-1,max(cluster_list)+1))
        clabel_clean = sorted(list(set(cluster_list)))
        n_cluster = len(c_label)

        cluster_lists.append(cluster_list)
        n_clusters.append(n_cluster)
        clabels_clean.append(clabel_clean)   

    mycolors = util_clusty.get_colormap()

    start_source =0
    for i_file in range(len(file_list)-1):
        if debug:
            fig, ax_arr = plt.subplots(1,2, figsize=(6,3))

        cluster1 = cluster_lists[i_file]
        cluster2 = cluster_lists[i_file+1]

        # calculate confusion matrix  
        confu_array = num.zeros((n_clusters[i_file],n_clusters[i_file+1]))
        test_array = num.zeros_like(confu_array)

        for i_c in range(len(cat)):
            try:
                confu_array[cluster1[i_c]+1,cluster2[i_c]+1] += 1
                if debug:
                    test_array[cluster1[i_c]+1,cluster2[i_c]+1] = 1
            except:
                print('fail', cluster1[i_c]+1, cluster2[i_c]+1)

        if debug:
            ax_arr[0].imshow(confu_array,vmax=20)
            ax_arr[1].imshow(test_array)

        inds = num.where(confu_array != 0)

        # determine links of unclustered and clustered to keep a constant
        # number of proxy unclustered events
        from_unclustered = 0
        for a,b in zip(inds[0],inds[1]):
            if a == 0 and b != 0:
                from_unclustered += confu_array[a,b]

        confu_array[0,0] = int(math.ceil(from_unclustered / 100.0)) * 100 - from_unclustered
        confu_array[0,0] = 10

        max_in = int(max(inds[0]))
        max_out = int(max(inds[1]))

        for a,b in zip(inds[0],inds[1]):

            src = a+start_source
            trgt = b+start_source+max_in+1

            if debug:
                print(a-1,b-1,confu_array[a,b],'-->',
                    src, trgt, confu_array[a,b])

            sources.append(src)
            targets.append(trgt)
            values.append(confu_array[a,b])

        start_source = max(sources)+1

        if i_file == 0:
            if plot_labels:
                labels = ['%s%i' % (alphabet[i_file],s) for s in range(-1,max_in)]
            colors = [mycolors[i+1] for i in range(-1,max_in)]

        if plot_labels:
            labels += ['%s%i' % (alphabet[i_file+1],t) for t in range(-1,max_out)]

        try:
            colors += [mycolors[i] for i in range(max_out+1)]
        except IndexError:
            print('Not enough colors for visualization:\n' +
                    '   a) make sure to use unharmonized cluster catalogs\n' +
                    '   b) reduce the number of cluster results to be harmonized\n' +
                    '   c) use/implement another color scale')

        if debug:
            print(labels)
            print('sources', sources)
            print('targest', targets)
            print(values)
            print(colors)
            print('max_sources', max(sources))
            plt.show()

        fig = go.Figure(data=[go.Sankey(
            node=dict(
              pad=15,
              thickness=20,
              line=dict(color="black", width=0.5),
              label=labels,
              color=colors,
              #y = y, x=x
            ),
            link=dict(
              source=sources,
              target=targets,
              value=values
            ))])
            
    
    for i_file in range(len(file_list)):
        method_string1 = os.path.split(file_list[i_file])[-1][4:-5]
        eps = method_string1.split('_')[-2]
        fig.add_annotation(
            x=(1/(n_files-1))*i_file,
            y=-0.1,
            text='eps<br>' + eps,
            font=dict(
                size=20),
            showarrow=False)

    fig.update_layout(width=n_files*150)

    if not fn:
        fn = '%s/sankey.html' % work_dir


    plotly.offline.plot(fig, filename=fn,
                        auto_open=False)


def get_cluster_labels(cat, only_clustered = False):
    if type(cat) == str:
        cat = model.load_events(cat, format='yaml')

    # cluster_labels = clustering_instance.labels_
    cluster_labels = num.asarray([int(ev.extras['cluster_number']) for ev in cat])
    cluster_labels_set = list(set(list(cluster_labels)))

    if only_clustered:
        cluster_labels_set.remove(-1)

    return cluster_labels, cluster_labels_set


def calc_silhouette(cluster_labels, dist_array):
    # Compute the silhouette scores for each sample

    silhouette_vals = silhouette_samples(dist_array, cluster_labels, metric='precomputed')
    silhouette_scr = num.mean(silhouette_vals[cluster_labels != -1])
    silhouette_median = num.median(silhouette_vals[cluster_labels != -1])

    return silhouette_vals, silhouette_scr, silhouette_median


def get_repr_ev(cat,dist_array, method_string, work_dir):
    cluster_labels = num.asarray([int(ev.extras['cluster_number']) for ev in cat])
    cluster_labels_set = sorted(list(set(cluster_labels)))

    if len(cluster_labels_set) !=1:

        silhouette_vals, silhouette_scr, silhouette_med = calc_silhouette(cluster_labels, dist_array)

        repr_evs = []
        #max_sil_vals = {}

        for i, cl in enumerate(cluster_labels_set[1:]):
            sil_vals = silhouette_vals.copy()
            sil_vals[cluster_labels != cl] = 0
            #max_sil_vals[cl] = max(sil_vals)
            repr_ev = cat[num.argmax(sil_vals)]
            repr_ev.extras['max_sil_val'] = num.max(sil_vals)
            repr_evs.append(repr_ev)

        model.dump_events(repr_evs, '%s/results/representative_events_%s.yaml' % (work_dir, method_string), format='yaml')

        return repr_evs#, max_sil_vals
    else:
        repr_evs = []
        return repr_evs


def silhouette_plot(dist_array, cat, method_string, cluster_params, cc_cnf, work_dir, plformat):
    '''
    Plot to evaluate consistency within clusters.
    Wikipedia:
    The silhuette value measures how similar an object is to its own cluster. 
    (-1:+1) - hight values = high similarity, low values = low similarity
     If most objects have a high value, then the clustering configuration is appropriate.
     If many points have a low or negative value, then the clustering configuration 
     may have too many or too few clusters.

     Non-clustered events are not taken into account.

     :param dist_array: Numpy array with distances (as used for dbscan)
     :param resultcat: Pyrocko event catalog with cluster in extras
     :param method_string: Str representing the used method in the clustering
     :param eps: eps value used in DBSCAN clustering
     :param min_pts MinPts value used in DBSCAN clustering
    
    '''
    fig, ax = plt.subplots(figsize=(3.486*1.3, (3.486/1.618)*1.3))
    cluster_labels = num.asarray([int(ev.extras['cluster_number']) for ev in cat])

    param1, pts_min = cluster_params

    if 'dbscan' in method_string:
        param_name = 'Eps'
    elif 'optics' in method_string:
        param_name = 'Xi'

    try:
        silhouette_vals, silhouette_scr, silhouette_med = calc_silhouette(cluster_labels, dist_array)
        logger.info('Preparing Silhouette plot for %s ' % method_string)
        y_lower = 0
        padding = 1
        for i, cl in enumerate(sorted(set((cluster_labels)))[1:]):
            # Aggregate the silhouette scores for samples belonging to
            cluster_sil_vals = silhouette_vals[cluster_labels == cl]
            cluster_sil_vals.sort()
            color = util_clusty.cluster_to_color(cl)

            size_cluster_i = cluster_sil_vals.shape[0]
            y_upper = y_lower + size_cluster_i
            ax.fill_betweenx(num.arange(y_lower, y_upper),
                           0,
                           cluster_sil_vals,
                           facecolor=color,
                           edgecolor=color,
                           alpha=0.7)

            # Label the silhouette plots with their cluster numbers at the middle
            if i % 2 == 0:
                x_label = -0.1
            else:
                x_label = -0.15

            y_label = y_lower + size_cluster_i/2.
            ax.text(x_label, y_label, str(cl), fontsize=6, verticalalignment='center')
            # for next cluster:
            y_lower = y_upper + padding

        ax.set_xlabel("Silhouette coefficient", fontsize=8)
        ax.set_ylabel("N events", fontsize=8)
        ax.set_xlim((-1.0, 1.0))

        title = 'Silhouette plot - Bandpass: %s-%s Hz     MinPts: %i      %s: %.3f' % (
                        cc_cnf.bp[1], cc_cnf.bp[2], int(pts_min), param_name, float(param1))
        ax.set_title(title, fontsize=8)
    
        # The vertical line for average silhoutte score of all the values
        ax.axvline(x=silhouette_scr, c='r', alpha=0.8, lw=0.8, ls='-')

        ax.tick_params(axis='both', which='major', labelsize=6, top=False)
        for axis in ['top','bottom','left','right']:
            ax.spines[axis].set_linewidth(0.5)

        plt.tight_layout()    
        # plt.show()
        fig.savefig('%s/results/silhouette_%s.%s' % (work_dir, method_string,plformat))
        plt.close()

    except ValueError:
        logger.info("No clusters found for %s. Skip silhouette plot" % method_string)

def silhouette_scores(params_sil_list, sil_score_list,
    sil_med_list, n_cluster_list, n_clustered_list, minpts, comp_name, cl_cnf, work_dir):

    #fig,ax = plt.subplots(figsize=(3.486, 3.486/1.618))
    fig,ax = plt.subplots(figsize=(3.486, 3.486/1.618))
    ax1 = ax.twinx()
    ax2 = ax.twinx()

    p1, = ax.plot(params_sil_list, sil_score_list, 'k', label='silh. score', linewidth=0.5)
    p1b, = ax.plot(params_sil_list, sil_med_list, 'k:', label='silh. medium', linewidth=0.5)
    p3, = ax2.plot(params_sil_list, n_clustered_list, 'g--', label='n cl. ev', linewidth=0.5)
    p2, = ax1.plot(params_sil_list, n_cluster_list, 'b:', label='n clusters', linewidth=0.5)

    ax.yaxis.label.set_color('k')
    ax1.yaxis.label.set_color('b')
    ax2.yaxis.label.set_color('g')

    param_name = get_cluster_params_cnf(cl_cnf, name_only=True)

    ax.set_xlabel(param_name, fontsize=6)

    ax.set_ylabel('Silhouette score', fontsize=6)
    ax1.set_ylabel('Number of clusters', fontsize=6)
    ax2.set_ylabel('Number of clustered events', fontsize=6)

    #ax1.set_ylim(0,22)
    #ax2.set_ylim(0,600)

    ax.tick_params(which='both', top=True, bottom=True, labelbottom=True, labeltop=True, labelsize=6)

    # get back here
    for label in ax.xaxis.get_ticklabels(which='both')[1::2]:
        label.set_visible(False)

    ax.tick_params(axis='both',which='both', colors='k', labelsize=6, direction='in')
    ax1.tick_params(axis='y', which='both', colors='b', labelsize=6, direction='in')
    ax2.tick_params(axis='y', colors='g', labelsize=6, direction='in')

    ax2.spines["right"].set_position(("axes", 1.25))

    ax.xaxis.set_major_locator(ticker.MultipleLocator(0.05))
    ax.xaxis.set_minor_locator(ticker.MultipleLocator(0.01))


    ax1.yaxis.set_major_locator(ticker.MultipleLocator(2))
    ax1.yaxis.set_minor_locator(ticker.MultipleLocator(1))

    ax.xaxis.grid(True, which='minor', linewidth='0.2',  color='grey', linestyle=(0, (1, 3)))
    
    
    lines = [p1, p1b, p2, p3]
    legend = ax.legend(lines, [l.get_label() for l in lines], loc='lower right', fontsize=3, fancybox=False, 
                       framealpha=0.8, borderaxespad=1.0, facecolor='white')
    legend.get_title().set_fontsize('6')

    for axis in ['top','bottom','left','right']:
        ax.spines[axis].set_linewidth(0.5)
        ax1.spines[axis].set_linewidth(0.5)
        ax2.spines[axis].set_linewidth(0.5)

    ax.spines['right'].set_color('b')
    ax1.spines['right'].set_color('b')
    ax2.spines['right'].set_color('g')

    #ax3.cla()
    #ax3.set_xlim(ax.get_xlim())
    #ax3.set_xticks(ax.get_xticks())
    #ax3.set_xbound(ax.get_xbound())
    #ax3.set_xticklabels(ax.get_xticklabels())

    plt.tight_layout()
    fig.savefig('%s/results/silhouette_scores_MinPts%s_%s.pdf' % (work_dir, minpts, comp_name))

    plt.close()


def plot_param_analysis(dists, comp_name, cl_cnf, netsim_cnf, work_dir):

    if cl_cnf.method == 'dbscan':
        min_pts_list = cl_cnf.dbscan_min_pts
        freeparam_list = num.arange(0.01, 0.41, 0.01)

    elif cl_cnf.method == 'optics':
        min_pts_list = cl_cnf.optics_min_pts
        freeparam_list = num.arange(0.01, 0.41, 0.01)

    else:
        logger.error('Clustering method not implemented. Please use ```dbscan``` or ```optics```.')

    cluster_labels_list,methodstrings = cluster(dists, comp_name, cl_cnf, netsim_cnf, freeparam_list=freeparam_list)

    for minpts in list(set(min_pts_list)):

        # use only the clustering results of current minpts for the plot
        idx_currentMinPts = [i for i,m in enumerate(methodstrings) if m.split('_')[-1].startswith(str(minpts))]
        current_cluster_labels_list = [cluster_labels_list[i] for i in idx_currentMinPts]

        sil_score_list = []
        sil_med_list = []
        n_cluster_list = []
        n_clustered_list = []

        for i_cl, cluster_labels in enumerate(current_cluster_labels_list):
            try:
                silhouette_vals, silhouette_scr, silhouette_medi = calc_silhouette(cluster_labels,dists)
                n_cluster = len(set(cluster_labels))-1
                n_clustered = sum(num.array(cluster_labels) != -1)

                sil_score_list.append(silhouette_scr)
                sil_med_list.append(silhouette_medi)
                n_cluster_list.append(n_cluster)
                n_clustered_list.append(n_clustered)

            except ValueError:
                sil_score_list.append(0)
                n_cluster_list.append(0)
                n_clustered_list.append(0)
                sil_med_list.append(0)

        ### is sorting relevant here?
        idx = num.argsort(freeparam_list)

        # print(sil_score_list)
        # print(num.shape(sil_score_list))
        freeparam_list = num.array(freeparam_list)[idx]
        sil_score_list = num.array(sil_score_list)[idx]
        sil_med_list = num.array(sil_med_list)[idx]
        n_cluster_list = num.array(n_cluster_list)[idx]
        n_clustered_list = num.array(n_clustered_list)[idx]

        #shall this be interactive?
        silhouette_scores(freeparam_list, sil_score_list, sil_med_list,
                                n_cluster_list, n_clustered_list,
                                minpts, comp_name,cl_cnf,work_dir)

def calc_adjusted_rand(cat_list, work_dir, plformat):
    '''
    The adjusted rand score quantifies the similarity of two cluster results
    with 1.0 being identical and 0 for complete difference
    params: a list of cluster files or clustered catalogs
    '''
    fig, ax = plt.subplots(figsize=(3.486, (3.486)*0.9))

    n_cat = len(cat_list)
    ARI_array = num.zeros((n_cat, n_cat))

    eps_list = []
    minpts_list = []

    for i_cat, cat_path in enumerate(cat_list):
        cat1 = model.load_events(filename=cat_path, format='yaml')
        cluster1 = [ev.extras['cluster_number'] for ev in cat1]
        method_string1 = os.path.split(cat_path)[-1][4:-5]
        eps = method_string1.split('_')[-2]
        eps_list.append(eps)
        minpts_list.append(method_string1.split('_')[-1])

        for j_cat, cat_path2 in enumerate(cat_list):
            if i_cat < j_cat:
                cat2 = model.load_events(filename=cat_path2, format='yaml')
                cluster2 = [ev.extras['cluster_number'] for ev in cat2]

                ARI_array[i_cat, j_cat] = adjusted_rand_score(cluster1, cluster2)

    ARI_array += ARI_array.T
    num.fill_diagonal(ARI_array, 1.0)

    im = ax.imshow(ARI_array, origin='lower', aspect='equal')
    ax.set_yticks(range(0, n_cat))
    ax.set_yticklabels(['%s, %s' % (eps, minpts) for eps, minpts in zip(eps_list, minpts_list)], fontsize=8)
    ax.set_xticks(range(0, n_cat))
    ax.set_xticklabels(['%s, %s' % (eps, minpts) for eps, minpts in zip(eps_list, minpts_list)], rotation=40, fontsize=8)
    cb = fig.colorbar(im, ax=ax)
    cb.ax.tick_params(labelsize=8)

    plt.tight_layout()
    fig.savefig('%s/results/adjusted_rand_matrix.%s' % (work_dir,plformat))
    plt.close()


def map_plot_freqmerge(catalog, method, cl_cnf, plformat):
    faults = cl_cnf.map_faults_path
    log_tuple = cl_cnf.mag_scale_log
    gmtconf = dict(
                   MAP_TICK_PEN_PRIMARY='1.25p',
                   MAP_TICK_PEN_SECONDARY='1.25p',
                   MAP_TICK_LENGTH_PRIMARY='0.2c',
                   MAP_TICK_LENGTH_SECONDARY='0.6c',
                   FONT_ANNOT_PRIMARY='16p,1,black',
                   FONT_LABEL='16p,1,black',
                   MAP_FRAME_TYPE='fancy',
                   FORMAT_GEO_MAP='D',
                   PS_PAGE_ORIENTATION='portrait',
                   MAP_GRID_PEN_PRIMARY='thinnest,0/50/0',
                   MAP_ANNOT_OBLIQUE='6',
                   PROJ_LENGTH_UNIT='p')

    ev_nomt = []
    ev_mt = []
    lats_nocl = []
    lons_nocl = []
    s_nocl = []
    ev_nocl_butmt = []

    mags = [ev.magnitude if ev.magnitude else num.nan for ev in catalog]
    mag_min = num.nanmin(mags)
    mag_max = num.nanmax(mags)


    if num.isnan(mag_min):
        mag_min = 2
        no_mags = True
    else:
        no_mags = False

    if num.isnan(mag_max):
        mag_max = 2

    mag_substituted = False
    for ev in catalog:
        if ev.magnitude == None:
            ev.magnitude = mag_min/2
            mag_substituted = True

    mags = [ev.magnitude for ev in catalog]

    if no_mags:
        logger.warning('WARNING: Catalog without magnitudes. Magnitudes set to 1 for plotting.')
    elif mag_substituted:
        logger.warning('WARNING: Not all events in catalog have magnitudes.Missing magnitudes were \
                        set to min(mag)/2 for plotting. %s %s' % (mag_min, mag_max))

    if cl_cnf.mag_scale_log:
        log_tuple = cl_cnf.mag_scale_log
        scale_meth = 'manual'
    else:
        log_tuple = None  # get_mag_scale(mag_min,mag_max)
        scale_meth = 'auto'

    for ev in catalog:
        if int(ev.extras['cluster_number']) != -1:
            if ev.moment_tensor:
                ev_mt.append(ev)
            else:
                ev_nomt.append(ev)
        else:
            if ev.moment_tensor:
                ev_nocl_butmt.append(ev)
            else:
                lats_nocl.append(ev.lat)
                lons_nocl.append(ev.lon)
                s_nocl.append(mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,libmethod=scale_meth))
    
    # map dimensions:
    lats_all = sorted([ev.lat for ev in catalog])  # if ev.extras['cluster_number'] != -1]
    lons_all = sorted([ev.lon for ev in catalog])   # if ev.extras['cluster_number'] != -1]
    n_trim = int(len(lats_all) * 0.02)
    lats_all = lats_all[n_trim: -n_trim]
    lons_all = lons_all[n_trim: -n_trim]

    lat_m = num.mean(lats_all)
    lon_m = num.mean(lons_all)

    corners = [od.Loc(num.min(lats_all), num.min(lons_all)),
               od.Loc(num.max(lats_all), num.max(lons_all))]

    dist1 = od.distance_accurate50m(od.Loc(lat_m, lon_m), corners[0])
    dist2 = od.distance_accurate50m(od.Loc(lat_m, lon_m), corners[1])
    radius = max(dist1, dist2)
    #if radius < 1000:
    #    radius = 1000.

    if cl_cnf.method == ['dbscan', 'optics']:
        eps = method.split('_')[-2]

    minpts = method.split('_')[-1]

    title = 'Merged frequency bands, trimmed mean (0.3)'

    m = Map(
        lat=lat_m,
        lon=lon_m,
        radius=radius,
        width=30,
        height=30,
        show_grid=False,
        show_topo=False,
        # topo_cpt_dry='/home/gesap/Documents/CETperceptual_GMT/CET-L2.cpt',#'/usr/local/share/cpt/gray.cpt',
        # color_dry=(143, 188, 143),  # grey
        illuminate=True,
        illuminate_factor_ocean=0.15,
        # illuminate_factor_land = 0.2,
        show_rivers=True,
        show_plates=False,
        gmt_config=gmtconf,
        comment=title)

    if faults:
        for f in faults:
            m.gmt.psxy(f, G='black', S='f1', *m.jxyr)

    # events without MT and without cluster number
    m.gmt.psxy(in_columns=(lons_nocl, lats_nocl, s_nocl), S='c', G='grey', *m.jxyr)

    # events without cluster number but with MT
    for ev in ev_nocl_butmt:
        g_col = 'grey'
        mmt = ev.moment_tensor
        devi = mmt.dc()
        mt = devi.m_up_south_east()
        mt = mt/ mmt.scalar_moment() \
                * pmt.magnitude_to_moment(ev.magnitude)
        m6 = pmt.to6(mt)
        data = (ev.lon, ev.lat, 10) + tuple(m6) + (1,0,0)
        s = 'd%sp' % mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth)
        m.gmt.psmeca(
                in_rows=[data],
                G=g_col, 
                S=s,
                W='0.5p,%s' %g_col,
                F='a30c/cc',
                M=True,
                *m.jxyr)

    # events without MT but in a cluster
    origins = len(ev_nomt[0].extras['origins'])
    symbol_list = ['c', 'a', 'd', 'i', 't', 'g', 'n']
    for ev in ev_nomt:
        g_col = util_clusty.color2rgb(ev.extras['color'])
        for i in range(origins):
            if ev.extras['origins'][i] == True:
                s = '%s%sp' % (symbol_list[i], mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth))
        #elif ev.extras['origins'][1] == True:
        #    s = '%s%sp' % (symbol_list[], mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth))
        #elif ev.extras['origins'][2] == True:
        #    s = '%s%sp' % (symbol_list[], mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth))
        #elif ev.extras['origins'][3] == True:
        #    s = '%s%sp' % (symbol_list[], mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth))
        m.gmt.psxy(in_columns=([ev.lon], [ev.lat]),
                   G=g_col, S=s, W='0.5p,%s' % g_col,
                   *m.jxyr)

    # clustered events with a MT
    for ev in ev_mt:
        g_col = util_clusty.color2rgb(ev.extras['color'])
        mmt = ev.moment_tensor
        devi = mmt.dc()
        mt = devi.m_up_south_east()
        mt = mt/ mmt.scalar_moment() \
                * pmt.magnitude_to_moment(ev.magnitude)
        m6 = pmt.to6(mt)
        data = (ev.lon, ev.lat, 10) + tuple(m6) + (1,0,0)
        s = 'd%sp' % mag_scale_log(ev.magnitude, log_tuple,mag_max,mag_min,plotlib=cl_cnf.map_plotlib,method=scale_meth)
        if ev.extras['origins'][0] == True:
            m.gmt.psmeca(
                    in_rows=[data],
                    G=g_col, 
                    S=s,
                    W='0.05p,%s' %g_col,
                    M=True,
                    #F='a0.01/cc',
                    *m.jxyr)
        else:
            m.gmt.psmeca(
                    in_rows=[data],
                    G=g_col, 
                    S=s,
                    W='0.05p,%s' %'black',
                    #F='a0.01c/cc',
                    M=True,
                    *m.jxyr)            

    lower = num.floor(mag_min)
    upper = num.ceil(mag_max)

    if upper-lower <= 2:
        step = 0.5
    else:
        step = 1

    if mag_min != mag_max:
        if upper-lower <= 2:
            mags_for_leg = num.array(num.arange(lower,num.round(mag_max,1), step))
            mags_for_leg = num.append(mags_for_leg,mag_max)
        else:
            mags_for_leg = num.array(num.arange(lower,upper,step))
    else:
        mags_for_leg = [1]

    for i_ma, ma in enumerate(mags_for_leg):
        if i_ma == 0:
            leg = 'S 0.1i c %sp black 1p 0.3i M %.1f\n' % (mag_scale_log(ma, log_tuple, mag_max, mag_min,method=scale_meth),ma)
        else:
            leg = leg + 'S 0.1i c %sp black 1p 0.3i M %.1f\n' % (mag_scale_log(ma, log_tuple, mag_max, mag_min,method=scale_meth),ma)


    leg = leg + 'D 0 1p\n'

    n_cluster = len(cluster_color_dict.keys())
    leg_cols = get_ncols_legend(n_cluster)
    leg = leg + 'N %i\n' % leg_cols

    for key in sorted(cluster_color_dict.keys()):
        item = cluster_color_dict[key]
        leg = leg + 'S 0.1i c %sp %s - 0.3i %s\n' % (mag_scale_log(num.median(mags), log_tuple, mag_max, mag_min,method=scale_meth), item, key)

    with open('leg.txt', 'w') as f:
        f.write(leg)

    m.gmt.pslegend(
        'leg.txt',
        D='x3.0c/3.0c+w3.5c')

    if not fn:
        fn = os.path.join(basic_cnf.work_dir, 'results/cluster_map_%s_freq_merged.%s' % method,plformat)

    m.save(fn)

    os.remove('leg.txt')