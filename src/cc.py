import sys
import glob, time
import logging, gc

import numpy as num
from os import cpu_count
from pyrocko import pile, trace, util
from pyrocko.gui import marker as pm
from pyrocko import orthodrome as od
from concurrent.futures import ProcessPoolExecutor
import itertools

import matplotlib
#if cpu_count() >= 16:
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from . import util_clusty
from multiprocessing import Pool
from functools import partial
from itertools import repeat, product

np = num
logger = logging.getLogger('__name__')

def check_distance(catalog, dist_thresh, dist_hypo, coordinate_system):
    ''' 
    computes inter event distances and returns 
    a boolean matrix with True where dist < allowed distance
    (spherical and plane coordinate systems)
    '''

    if coordinate_system == 'plane':
        from scipy.spatial.distance import cdist
        ev_coords = num.asarray([[ev.east_shift,ev.north_shift,ev.depth] for ev in catalog])
        dist_array = cdist(ev_coords, ev_coords)

    else:
        dist_array = num.empty((len(catalog), len(catalog)))

        if dist_hypo:
            for i_ev, ev in enumerate(catalog):
                x1,y1,z1 = od.geodetic_to_ecef(ev.lat, ev.lon, ev.depth)
                for i_ev2, ev2 in enumerate(catalog):
                    # if i_ev > i_ev2:
                    x2,y2,z2 = od.geodetic_to_ecef(ev2.lat, ev2.lon, ev2.depth)
                    dist_array[i_ev, i_ev2] = num.sqrt((x1-x2)**2 + (y1-y2)**2 + (z1-z2)**2)

        else:
            if coordinate_system == 'spherical':
                b_lats = num.asarray([ev.lat for ev in catalog])
                b_lons = num.asarray([ev.lon for ev in catalog])

                for i_ev, ev in enumerate(catalog):
                    dist_array[i_ev, :] = od.distance_accurate50m_numpy(
                    ev.lat, ev.lon, b_lats, b_lons)

    if dist_thresh:
        bool_dist = dist_array < dist_thresh
    else:
        bool_dist = num.empty((len(catalog), len(catalog)))
        bool_dist.fill(True)

    return bool_dist, dist_array



def get_midpoint(xs, ys, weights):
    x = num.average(xs, weights=weights)
    y = num.average(ys, weights=weights)

    return x,y


def station_weighting(station_list, events):
    '''
    based on Roten et al. 2012 and two other self-made weighting ideas
        
    :param stations: list of pyrocko stations
    :param catalog: list of pyrocko events
    '''
    mid_point = od.geographic_midpoint(
        num.asarray([ev.lat for ev in events]),
        num.asarray([ev.lon for ev in events]))

    # compute all midpoint-station-azimuths
    azs = od.azimuth_numpy(num.asarray([mid_point[0]]), 
        num.asarray([mid_point[1]]),
        num.asarray([st.lat for st in station_list]),
        num.asarray([st.lon for st in station_list]))

    azs = [az if az > 0 else 360+az for az in azs]
    station_list = station_list[0:len(azs)]

    '''
    METHOD 0:
    based on Roten et al. 2012:
    '''

    st_weights = []
    d_az = 180
    
    for i_st, st in enumerate(station_list):
        summ = 0
        
        for i_st2, st2 in enumerate(station_list):
            diff = abs(azs[i_st] - azs[i_st2])

            if diff > 180:
                diff = 360 - diff

            summ += 1 - (diff / d_az)
        
        st_weights.append(1/summ)

    st_weights = [w/sum(st_weights) for w in st_weights]

    '''
    METHOD 1:
    
    st_weights_test = []
    for i_st, st in enumerate(station_list):
        summ = 0
        for i_st2, st2 in enumerate(station_list):
            diff = abs(azs[i_st] - azs[i_st2])
            if diff > 180:
                diff = 360 - diff
            summ += diff

        st_weights_test.append(summ/(len(station_list)-1))

    st_weights_test = [w/sum(st_weights_test) for w in st_weights_test]
    '''

    '''
    METHOD 2:
    based on az. distance to both neighbouring stations
    '''

    st_weights_test2 = []
    all_dists = 0
    n = 0

    a = zip(azs, station_list)
    a = sorted(a, key= lambda x: x[0])
    stations = [aa[0] for aa in a]
    azs_s = sorted(azs)

    for i_st, st in enumerate(station_list):
        summ = 0
        if i_st == 0:
            diff1 = abs(azs_s[i_st]-azs_s[i_st+1])
            if diff1 > 180:
                diff1 = 360-diff1
            diff2 = abs(azs_s[i_st]-azs_s[-1])
            if diff2 > 180:
                diff2 = 360-diff2
            diff = ((diff1+diff2)/2) /(360/(len(station_list)-1))

        elif i_st == len(station_list)-1:
            diff1 = abs(azs_s[i_st]-azs_s[0])
            if diff1 > 180:
                diff1 = 360-diff1
            diff2 = abs(azs_s[i_st]-azs_s[i_st-1])
            if diff2 > 180:
                diff2 = 360-diff2
            diff = ((diff1+diff2)/2) /(360/(len(station_list)-1))
        
        else:
            diff1 = abs(azs_s[i_st]-azs_s[i_st+1])

            if diff1 > 180:
                diff1 = 360-diff1
            diff2 = abs(azs_s[i_st]-azs_s[i_st-1])
            if diff2 > 180:
                diff2 = 360-diff2
            diff = ((diff1+diff2)/2) / (360/(len(station_list)-1))
        st_weights_test2.append(diff)

    st_weights_test2 = [w/sum(st_weights_test2) for w in st_weights_test2]

    '''
    METHOD 3:
    based on az. distance to closest neighbouring stations
    '''

    st_weights_test3 = []

    for i_st, st in enumerate(station_list):
        summ = 0
        if i_st == 0:
            diff1 = abs(azs_s[i_st]-azs_s[i_st+1])
            if diff1 > 180:
                diff1 = 360-diff1
            diff2 = abs(azs_s[i_st]-azs_s[-1])
            if diff2 > 180:
                diff2 = 360-diff2
            if diff2 < diff1:
                diff = diff2
            else:
                diff = diff1

            diff = diff / (360/(len(station_list)-1))

        elif i_st == len(station_list)-1:
            diff1 = abs(azs_s[i_st]-azs_s[0])
            if diff1 > 180:
                diff1 = 360-diff1
            diff2 = abs(azs_s[i_st]-azs_s[i_st-1])
            if diff2 > 180:
                diff2 = 360-diff2
            if diff2 < diff1:
                diff = diff2
            else:
                diff = diff1

            diff = diff / (360/(len(station_list)-1))
        
        else:
            diff1 = abs(azs_s[i_st]-azs_s[i_st+1])

            if diff1 > 180:
                diff1 = 360-diff1
            diff2 = abs(azs_s[i_st]-azs_s[i_st-1])
            if diff2 > 180:
                diff2 = 360-diff2
            if diff2 < diff1:
                diff = diff2
            else:
                diff = diff1

            diff = diff / (360/(len(station_list)-1))

        st_weights_test3.append(diff)

    st_weights_test3 = [w/sum(st_weights_test3) for w in st_weights_test3]

    # print(st_weights)
    y = [1* num.sin(num.deg2rad(az)) for az in azs]
    x = [1* num.cos(num.deg2rad(az)) for az in azs]

    x_midw0, y_midw0 = get_midpoint(x, y, st_weights)
    #x_midw1, y_midw1 = get_midpoint(x, y, st_weights_test)

    ys = [1* num.sin(num.deg2rad(az)) for az in azs_s]
    xs = [1* num.cos(num.deg2rad(az)) for az in azs_s]
    x_midw2, y_midw2 = get_midpoint(xs, ys, st_weights_test2)
    x_midw3, y_midw3 = get_midpoint(xs, ys, st_weights_test3)  

    fig, ax = plt.subplots(ncols=3, figsize=(4*5,5))
    for xx, yy, xxs, yys, s, s2, s3, az, az_s in zip(x, y, xs, ys, st_weights, 
                                                     st_weights_test2, 
                                                     st_weights_test3, azs, azs_s):
        s = round(s, 2)
        s2 = round(s2, 2)
        s3 = round(s3, 2)

        az = round(az, 1)
        az_s = round(az_s, 1)
        t = '%s, %s' % (s, az)
        t2 = '%s, %s' % (s2, az_s)
        t3 = '%s, %s' % (s3, az_s)

        ax[0].text(xx, yy, s=t)
        ax[0].scatter(xx,yy)
        ax[1].text(xxs, yys, s=t2)
        ax[1].scatter(xxs,yys)
        ax[2].text(xxs, yys, s=t3)
        ax[2].scatter(xxs,yys)
    ax[0].plot(x_midw0, y_midw0, 'ro')
    ax[1].plot(x_midw2, y_midw2, 'ro')
    ax[2].plot(x_midw3, y_midw3, 'ro')
    ax[0].plot(0, 0, 'go')
    ax[1].plot(0, 0, 'go')
    ax[2].plot(0, 0, 'go')            
    ax[0].set_aspect('equal')
    ax[1].set_aspect('equal')
    ax[2].set_aspect('equal')
    ax[0].set_title('a: Roten et al. 2012')
    ax[1].set_title('b: both neighbours')
    ax[2].set_title('c: closest neighbour')
    ax[0].set_ylim(-1.5,1.5)
    ax[1].set_ylim(-1.5,1.5)
    ax[2].set_ylim(-1.5,1.5)
    ax[0].set_xlim(-1.5,1.5)
    ax[1].set_xlim(-1.5,1.5)
    ax[2].set_xlim(-1.5,1.5)
    plt.tight_layout()
    fig.savefig('test0.png')

    plt.show()

    choice = input('Which station weighting do you want to use? Enter b for method b, c for method c or 0 for no weighting.')

    if choice == 'b':
        st_weights = st_weights_test2
        logger.info('Weighting b selected.')
        return st_weights 

    elif choice == 'c':
        st_weighhts = st_weights_test3
        logger.info('Weighting c selected.')
        return st_weights 
    
    else:
        logger.info('No weighting selected.')
        return []


def comb_avg(network_similarity, components_weights=None):
    '''
    compute average component's network similarity as combination method.
    can be called if net-sim computation method does not include a combination 
    of P and S itself like in the product.
    '''

    logger.info('\n----------- Combinig components -----------')
    n_comp = network_similarity.shape[0]
    if not components_weights:
        if n_comp == 3:
            [0.4, 0.3, 0.3]
        if n_comp == 2:
            [0.4, 0.6]

    if n_comp == 1:
        logger.warning('P and S already combined in network similarity computation or '
              +'only one phase used.')
        return network_similarity

    else:
        n_ev = network_similarity.shape[2]

         # ugly not numpy way to do it, should be replaced...:
        combined_netsim = num.empty((1, n_ev, n_ev))
        combined_netsim.fill(num.nan)

        for i_ev in range(n_ev):
            for j_ev in range(n_ev):
                check = True
                for i_comp in range(n_comp):
                    if network_similarity[i_comp, i_ev, j_ev] == 0 or num.isnan(network_similarity[i_comp, i_ev, j_ev]):
                        check = False

                if check:
                    if n_comp == 2:
                        combined_netsim[0, i_ev, j_ev] = (network_similarity[0,i_ev,j_ev] * components_weights[0])\
                                                          + (network_similarity[1,i_ev,j_ev] * components_weights[1])
                    if n_comp == 3:
                        combined_netsim[0, i_ev, j_ev] = (network_similarity[0,i_ev,j_ev] * components_weights[0])\
                                                          + (network_similarity[1,i_ev,j_ev] * components_weights[1])\
                                                          + (network_similarity[2,i_ev,j_ev] * components_weights[2])

        #combined_netsim =  network_similarity[0] * weight_firstphase + network_similarity[1]*(1-weight_firstphase)
        

        #combined_netsim[combined_netsim == 0] = num.nan
        #combined_netsim = combined_netsim[num.newaxis,...]

        return combined_netsim


def apply_cc_station_threshs(coef_array, netsim_settings, station_list, cat):

    ''' 
    apply a threshold for cc-coef that must be met at minimum number
    of stations
    '''

    if netsim_settings.cc_thresh and netsim_settings.min_n_stats:

        # num.nan for all stations if cc thresh not met at at least 
        # min_n_stats stations
        cat_len = len(cat)        
        for i_ev1 in range(cat_len):
            for i_ev2 in range(cat_len):
                counter_l = num.where(coef_array[:, i_ev1, i_ev2] > netsim_settings.cc_thresh)[0]
                counter = counter_l.shape[0]
                if not counter > netsim_settings.min_n_stats:
                    coef_array[:, i_ev1, i_ev2] = num.nan

                else:
                    if not netsim_settings.az_thresh:
                        continue

                    azis_ev1 = []
                    azis_ev2 = []

                    for i_st in counter_l:
                        azis_ev1.append(od.azimuth(cat[i_ev1].lat, 
                                               cat[i_ev1].lon, 
                                               station_list[i_st].lat,
                                               station_list[i_st].lon))
                        azis_ev2.append(od.azimuth(cat[i_ev2].lat, 
                                               cat[i_ev2].lon, 
                                               station_list[i_st].lat,
                                               station_list[i_st].lon))

                    azis_1 = sorted(azis_ev1)
                    azis_2 = sorted(azis_ev2)

                    azis_diff1 = []
                    azis_diff2 = []

                    for i_a, aa1 in enumerate(azis_1[:-1]):
                        if azis_1[i_a+1] < aa1:
                            if azis_1[i_a+1] < 0 and aa1 > 0:
                                diff = 360. + azis_1[i_a+1] - aa1
                            elif azis_1[i_a] > 0 and aa1 > 0:
                                diff = 360. - (aa1 - azis_1[i_a+1])
                            elif azis_1[i_a+1] < 0 and aa1 < 0:
                                diff = 360. - (aa1- azis_1[i_a+1])
                        else:
                            diff = azis_1[i_a+1] -aa1
                        azis_diff1.append(diff)

                    for i_a, aa2 in enumerate(azis_2[:-1]):
                        if azis_2[i_a+1] < aa2:
                            if azis_2[i_a+1] < 0 and aa2 > 0:
                                diff = 360. + azis_2[i_a+1] - aa2
                            elif azis_2[i_a] > 0 and aa2 > 0:
                                diff = 360. - (aa2 - azis_2[i_a+1])
                            elif azis_2[i_a+1] < 0 and aa2 < 0:
                                diff = 360. - (aa2- azis_2[i_a+1])
                        else:
                            diff = azis_2[i_a+1] -aa2
                        azis_diff2.append(diff)
                    
                    if num.max(azis_diff2) > (360. - netsim_settings.az_thresh)\
                        or num.max(azis_diff1) > (360. -netsim_settings.az_thresh):
                        
                        coef_array[:, i_ev1, i_ev2] = num.nan

                        logger.info(netsim_settings.az_thresh, ' not met', max(azis_diff1), max(azis_diff2))  


    return coef_array


def weight_func(coef_array, weights_array, az_stats_weights):
    ''' apply the computed weights array to the coef array'''

    #n_comp, n_st, n_ev, n_ev2 = coef_array.shape
    n_st, n_ev, n_ev2 = coef_array.shape

    weighted_coef_array = num.empty((n_st, n_ev, n_ev))
    weighted_coef_array.fill(num.nan)

    if az_stats_weights:

        # needs some checks...

        # print(weights_array.shape)
        # print(coef_array.shape)
        # print(len(az_stats_weights))

        weights_array = num.divide(weights_array, num.sum(weights_array, axis=0))
        weighted_coef_array = num.multiply(coef_array, weights_array)
        
        x = num.asarray(az_stats_weights)
        bools_3d = weighted_coef_array != 0

        for i_ev0 in range(n_ev):
            for i_ev1 in range(n_ev):
                bools = bools_3d[:, i_ev0, i_ev1] != 0
                n_used = num.sum(bools)
                sum_used_azweights = num.sum(x[bools])
                weighted_coef_array[ :, i_ev0, i_ev1] *= (x/sum_used_azweights) * n_used
        
    else:
        norm_weights_array = num.divide(weights_array, num.sum(weights_array, axis=0))
        weighted_coef_array = num.multiply(coef_array, norm_weights_array)

    return weighted_coef_array


# network similarity computation methods:
def weighted_sum_c_diff(coef_array, weights_array):
    '''
    weighted sum of the CC values (weightings from Shelly, 2015)

    :param weights_array: differences between the two largest peaks 
                          of the according CC functions)
    '''

    masked_coef = num.ma.masked_array(coef_array, coef_array == 0)
    masked_weights = num.ma.masked_array(weights_array, weights_array == 0)

    array_weighted_summed_ccs = num.average(masked_coef,
                        axis=0,
                        weights=masked_weights)

    return array_weighted_summed_ccs


def max_cc(coef_array):
    '''
    simply the maximum CC of an event pair among all stations
    (for testing of windows, etc.)

    '''
    n_ev = num.shape(coef_array)[2]
    array_max_cc = num.empty((n_ev, n_ev))
    array_max_cc[:,:] = num.nanmax(coef_array, axis=0)

    if num.array_equal(array_max_cc[0], coef_array[0,0]):
        logger.info('TRUE - one station used.')
    return array_max_cc


def mean_ccs(coef_array):
    '''
    Mean of CC values of event pairs (Aster and Scott, 1993)
    '''

    n_ev = num.shape(coef_array)[2]
    array_mean_ccs = num.zeros((n_ev,n_ev))
    for i in range(n_ev):
        for j in range(n_ev):
            if i >= j:
                array_mean_ccs[i,j] = num.mean(coef_array[:,i,j][coef_array[:,i,j] != 0])

    return  array_mean_ccs


def median_ccs(coef_array):
    '''
    Median of CC values of event pairs (Aster and Scott, 1993)
    '''

    n_ev = num.shape(coef_array)[2]
    array_median_ccs = num.zeros((n_ev,n_ev))
    for i in range(n_ev):
        for j in range(n_ev):
            if i >= j:
                array_median_ccs[i,j] = num.median(coef_array[:,i,j][coef_array[:,i,j] != 0])

    # #fancy numpy
    # array_median_ccs2 = num.median(coef_array, axis=2)

    return  array_median_ccs


def product(coef_array):
    '''
    (Stuermer et al, 2011)
    '''

    # ahhhh ja da auch so fancy numpy shit oder halt mit median_ccs 
    # zusammen... aber fuer erstmal...
    
    n_ev = num.shape(coef_array)[2]
    array_prod_ccs = num.zeros((n_ev,n_ev))
    for i in range(n_ev):
        for j in range(n_ev):
            if i >= j:
                cc_nonan = coef_array[:,i,j][coef_array[:,i,j] != 0]
                try:
                    array_prod_ccs[i,j] = num.prod(cc_nonan)**(1/(len(cc_nonan)))
                except ZeroDivisionError:
                    array_prod_ccs[i,j] = 0
                    continue

    return  array_prod_ccs


# def product_combPS(coef_array):
#     '''
#     (Stuermer et al, 2011)
#     '''

#     # wie vorher, aber P und S kombiniert

#     n_ph = num.shape(coef_array)[0]
#     n_ev = num.shape(coef_array)[2]
#     array_prod_ccs = num.zeros((1, n_ev,n_ev))
#     if n_ph != 2:
#         logger.error('Product of P and S currently only implemented for 2 phases.')
#         sys.exit()

#     for i in range(n_ev):
#         for j in range(n_ev):
#             if i >= j:
#                 cc_nonan_0 = coef_array[0, :,i,j][coef_array[0,:,i,j] != 0]
#                 cc_nonan_1 = coef_array[1, :,i,j][coef_array[1,:,i,j] != 0]
#                 try:
#                     array_prod_ccs[0,i,j] = (num.prod(cc_nonan_0)*num.prod(cc_nonan_1))**(1/(len(cc_nonan_0)+len(cc_nonan_1)))
#                 except ZeroDivisionError:
#                     array_prod_ccs[0,i,j] = 0
#                     continue

#     return  array_prod_ccs


def trimmed_mean_ccs(coef_array, netsim_settings):
    '''
    Left-side trimmed mean of CC values (Maurer and Deichmann, 1995)
    
    :param cut_off: percentage of CC values cut off
    '''

    # might add some fancy numpy shit here

    n_ev = num.shape(coef_array)[2]
    array_trimmed_mean_ccs = num.zeros((n_ev,n_ev))
    n_stats_used = num.zeros((n_ev, n_ev))
    if netsim_settings.get_bool_sta_use:
        sta_use = num.zeros_like(coef_array, dtype=bool)

    for i in range(n_ev):
        for j in range(n_ev):
            if i > j:
                non_zero_coefs = coef_array[:,i,j][coef_array[:,i,j]!=0]

                non_zero_non_nan_coefs = non_zero_coefs[num.argwhere(~num.isnan(non_zero_coefs))]

                nans = num.argwhere(num.isnan(non_zero_non_nan_coefs))

                if len(nans) > 0:
                    logger.warning('Found num.nans in cc matrix, there should not be any left.!')

                if len(non_zero_non_nan_coefs) != 0:
                    # # old approach
                    # cut_index = int(num.floor(len(non_zero_non_nan_coefs)*netsim_settings.trimm_cut))
                    # cut_non_zero_coefs = sorted(non_zero_non_nan_coefs)[cut_index:]
                    # array_trimmed_mean_ccs[i_comp,i,j] = num.mean(cut_non_zero_coefs)
                    # n_stats_used[i_comp,i,j] = len(cut_non_zero_coefs)

                    #new approach to save station contributions
                    sort_idx = num.argsort(coef_array[:,i,j])
                    #print('sorted indices: ', sort_idx)
                    sorted_array = coef_array[:,i,j][sort_idx]
                    #print('sorted array\n', sorted_array)
                    first_nonzero = num.argmax(sorted_array>0)
                    station_nr = len(sorted_array[first_nonzero:])
                    #print('station_nr', station_nr)

                    cut_index = int(num.floor(station_nr*netsim_settings.trimm_cut))+first_nonzero

                    # an easy but inconsistent way to handle the floor problem for small number of stations
                    # if len_ccs < 10:
                    #     cut_index_altern = int(num.round(station_nr*netsim_settings.trimm_cut))+first_nonzero
                    # mean_altern = num.mean(sorted_array[cut_index_altern:])

                    array_trimmed_mean_ccs[i,j] = num.mean(sorted_array[cut_index:])

                    # print('cut index', station_nr, netsim_settings.trimm_cut, cut_index\
                    #         , '(%.2f)' % array_trimmed_mean_ccs[i,j], '|', \
                    #             cut_index_altern, '(%.2f)' % mean_altern)

                    kept_stations = sort_idx[cut_index:]
                    #print('kept stations', kept_stations)
                    n_stats_used[i,j] = len(kept_stations)

                    if netsim_settings.get_bool_sta_use:

                        for i_sta in kept_stations: 
                            sta_use[i_sta,i,j] = True

                else:
                    array_trimmed_mean_ccs[i,j] = num.nan
                    n_stats_used[i,j] = 0

            elif i == j:
                array_trimmed_mean_ccs[i,j] = 1

    if netsim_settings.get_bool_sta_use:
        return array_trimmed_mean_ccs, n_stats_used, sta_use
    else:
        return array_trimmed_mean_ccs, n_stats_used


# main function for stacking cc arrays of different stations
def stack_ccs_over_stations(coef_array, weights_array, az_stats_weights,
                            netsim_settings, station_list, cat, *kwargs):
    '''
    get network-based cross correlation 

    :param coef_array: Array of maximum CC values with shape 
                       (n_ph, n_st,n_ev,n_ev)
    :param weights_array: Array of CC weightings

    '''

    method = netsim_settings.method_similarity_computation

    if netsim_settings.apply_cc_station_thresh:
        apply_cc_station_threshs(coef_array, netsim_settings,
                                 station_list, cat)

    if method == 'weighted_sum_c_diff':
        # network_similarity = weighted_sum_c_diff(coef_array, weights_array)
        coef_array = weight_func(coef_array, weights_array, az_stats_weights)
        coef_array[coef_array ==0] = num.nan
        neg_values = coef_array[coef_array < 0]
        n_st, n_ev, n_ev2 = coef_array.shape
        network_similarity = num.empty((n_ev, n_ev))
        network_similarity.fill(num.nan)
        #for i_comp in range(n_comp):
        network_similarity = num.nansum(coef_array, axis=0)

    elif method == 'median_ccs':
        network_similarity = median_ccs(coef_array)

    elif method == 'trimmed_mean_ccs':
        if netsim_settings.get_bool_sta_use:
            network_similarity, n_stats_used, sta_use = trimmed_mean_ccs(coef_array, netsim_settings)
        else:
            network_similarity, n_stats_used = trimmed_mean_ccs(coef_array, netsim_settings)

    elif method == 'max_cc':
        network_similarity = max_cc(coef_array)

    elif method == 'mean_ccs':
        network_similarity = mean_ccs(coef_array)

    elif method == 'product':
        network_similarity = product(coef_array)

    #elif method == 'product_combPS':
    #    network_similarity = product_combPS(coef_array)

    elif method == 'add other stacking approaches':
        pass

    else:
        logger.error('Please choose valid stacking approach: \n'
        + 'weighted_sum_c_diff | median_ccs | trimmed_mean_ccs | product | max_cc |  mean_ccs | product_combPS')
        sys.exit()


    if method == 'trimmed_mean_ccs':
        if netsim_settings.get_bool_sta_use:
            return network_similarity, n_stats_used, sta_use
        else:
            return network_similarity, n_stats_used
    else:
        return network_similarity


# cross-correlation computation stuff
def comp_snr(tr, arr, twd, stname, evname):
    ''' 
    compute signal-to-noise-ratio
    
    :param tr: pyrocko trace
    :param arr: arrival time
    :param twd: time-window, tuple with start before and end after 
                arrival time arr.
    '''

    try:
        signal_wdw = tr.chop(tmin=arr+twd[0][0], tmax=arr+twd[-1][1], inplace=False).ydata
    except trace.NoData:
        logger.warning('%s %s: SNR calculation - No data for phase arrival' % (stname, evname))
        snr =0.
        return snr

    try:
        noise_wdw = tr.chop(tmin=arr-180, tmax=arr-60, inplace=False).ydata
        # trace.snuffle([signal_wdw,noise_wdw])
    except trace.NoData:
        logger.warning('%s %s: Longer time series recommended for SNR calculation'
                       % (stname, evname))

        logger.debug('%s %s: tr tmin %s, arr %s, tr tmax %s' 
                      % (stname, evname, util.tts(tr.tmin),util.tts(arr),util.tts(tr.tmax)))
        try:
            noise_wdw = tr.chop(tmin=tr.tmin, tmax=arr, inplace=False).ydata
        
        except trace.NoData:
            logger.warning('%s %s: SNR calculation - No noise time window extracted, skipping event.'
                       % (stname, evname))

            logger.debug('%s %s: tr tmin %s, arr %s, tr tmax %s' 
                          % (stname, evname, util.tts(tr.tmin),util.tts(arr),util.tts(tr.tmax)))
            noise_wdw = False

    if isinstance(noise_wdw, num.ndarray):
        try:
            snr = num.sqrt(num.mean(signal_wdw**2)/ num.mean(noise_wdw**2))
        except UnboundLocalError:
            snr = 0.

    else:
        snr = 0.

    logger.debug('SNR: %s' % snr)

    return snr


def get_pile(waveform_dir,st):
    if not '*' in waveform_dir:
        p = pile.make_pile(waveform_dir, regex='%s' % (st.station),
                           show_progress=False, fileformat='detect')
        #if not p.tmin:
        #    p = pile.make_pile(waveform_dir, regex='_%s_' % (st.station),
        #                       show_progress=False, fileformat='detect')
    else:
        files = glob.glob('%s/*%s.%s*' % (waveform_dir, st.network, st.station))
        p = pile.make_pile(files, show_progress=False, fileformat='detect')

    return p


def make_preprocessed_trace_list(i_st, st, catalog, cc_settings, 
                                 waveform_dir, arrT, comp, i_ph, phases):
    '''
    Make a list of traces for each station, if no trace for event, 
    boolean value ''False'' is appended.

    :param i_st: station-number, remains same in entire work-flow.
    :param st: pyrocko station
    :param catalog: list of pyrocko events
    :param cc_settings: cross-correlation settings defined in config file
    :param waveform_dir: directory containing raw waveforms
    :param arrT: precomputed arrival time array 
                 (shape: n_phases x n_stations x n_events)

    :returns trace_list: list of preprocessed and trimmed pyrocko traces for 
                         current station

    '''

    p = get_pile(waveform_dir,st)
    logger.info('Finished loading waveforms for station %s' % st.station)

    # get channel list to pick out vertical and horizontal channels
    # channel list works, but implementation not...
    # channel_list = sorted([channel for channel in list(p.channels.keys())])

    #vert_channel_list = [chan for chan in channel_list if chan[-1]=='Z']
    #hor_channel_list = [chan for chan in channel_list if chan[-1]!='Z']

    n_phases = len(phases)

    #logger.info('pile: %s' % pile)
    
    n_failed = 0

    trace_list = []
    len_cat = len(catalog)
    for i_ev, ev in enumerate(catalog):
        logger.debug(' %i/%i: %s %s' % (i_ev+1, len_cat,util.tts(ev.time, format='%Y-%m-%d %H:%M:%S.6FRAC'), ev.name))

        # get arrT from arrival time array:
        arr = arrT[i_ph, i_st, i_ev]
        tmin = arr - cc_settings.filtertmin
        tmax = arr + cc_settings.filtertmax

        try:
            logger.debug('%.6f - %.6f' %(tmin,tmax))
            logger.debug('t_arrival: %s, traveltime: %.6f' % (util.tts(arr,format='%Y-%m-%d %H:%M:%S.6FRAC'), arr-ev.time))
            logger.debug('window: %s - %s' % (util.tts(tmin, format='%Y-%m-%d %H:%M:%S.6FRAC'),util.tts(tmax, format='%Y-%m-%d %H:%M:%S.6FRAC')))
        except ValueError:
            logger.debug('no timing available')

        try:

            #if phase_name in ['P', 'p']:
            #    # why is the channel list from above not working here?
            #    tr1 = p.all(trace_selector=lambda tr: tr.channel in ['BHZ', 'HHZ', 'HNZ'], tmin=tmin, tmax=tmax)[0]
            #elif phase_name in ['S', 's']:
            #    tr1 = p.all(trace_selector=lambda tr: tr.channel in ['BHN', 'HHN', 'BHN'], tmin=tmin, tmax=tmax)[0]
            # tr1 = pile_all.all(trace_selector=lambda tr: (tr.channel == comp and tr.station ==st), tmin=tmin, tmax=tmax)[0]
            if not 'x' in comp:
                tr1s = p.all(trace_selector=lambda tr: tr.channel == comp, 
                            tmin=tmin, tmax=tmax)
            elif 'xx' in comp:
                compi = comp[-1]
                tr1s = p.all(trace_selector=lambda tr: tr.channel.endswith(compi), tmin=tmin, tmax=tmax)
            elif 'x' in comp:
                compi = comp[1:]
                tr1s = p.all(trace_selector=lambda tr: tr.channel.endswith(compi), tmin=tmin, tmax=tmax)

            # catch multiple overlapping traces and choose longest
            # make sure that arr is actually within the chosen trace!
            if len(tr1s) > 1:
                tr_idx = np.argmax([len(tr.ydata) for tr in tr1s])
                tr1 = tr1s[tr_idx]
                if tr1.tmin > arr or tr1.tmax < arr:
                    for itr, tr in enumerate(tr1s):
                        if tr.tmin < arr and tr.tmax > arr:
                            tr_idx = itr
                    tr1 = tr1s[tr_idx]
            else:
                tr1 = tr1s[0]

        except IndexError:
            trace_list.append(False)
            try:
                tmin_str = util.tts(tmin, format= '%H:%M:%S.6FRAC')
                tmax_str = util.tts(tmax, format= '%H:%M:%S.6FRAC')

                logger.debug('No trace: %s %s %s - %s' % (ev.name, st.station,tmin_str,tmax_str))

            except ValueError:
                logger.debug('no timing available')

            n_failed += 1
            continue

        tr1c = tr1.copy()
        # what about wrong choices of filtering parameters?
        #try:
        tr1c.bandpass(cc_settings.bp[0], cc_settings.bp[1], 
                      cc_settings.bp[2])
        if cc_settings.downsample_to:
            try:
                tr1c.downsample_to(cc_settings.downsample_to)
            except util.UnavailableDecimation:
                tr1c.fix_deltat_rounding_errors()
                try:
                    tr1c.downsample_to(cc_settings.downsample_to)
                except util.UnavailableDecimation:
                    logger.error('ERROR: Downsampling failed, decimation rate is not available and deltat could not be fixed/rounded.')
                    logger.error('station: %s, sampling rate: %s' % (tr1c.station, tr1c.deltat))
                    sys.exit()

        #except IndexError:
        #    print('Downsampling for station failed') 
        #    continue
        
        
        #logger.info('%s %s %s %s %s' % (i_ph, i_st, i_ev, arr, arrT[i_ph, i_st, i_ev]))

        if not cc_settings.twd:
            if n_phases > 1:
                if num.isnan(arrT[0, i_st, i_ev]):
                    trace_list.append(False)
                    logger.info('no arr T for 1st phase, %s %s ' % (st.station, ev.name))
                    continue

                if num.isnan(arrT[1, i_st, i_ev]):
                    trace_list.append(False)
                    logger.info('no arr T for 2nd phase, %s %s ' % (st.station, ev.name))
                    continue

                if 'P' in phases and 'S' in phases:
                    dps = arrT[1, i_st, i_ev] - arrT[0, i_st, i_ev]
                    twd = [[-2,dps],[-2, 1.5*dps]]
                elif 'R' in phases and 'L' in phases:
                    len_twd = (3.* 1./(cc_settings.bp[1])) + 10
                    twd = [[-10,len_twd],[-10,len_twd]]
            else:
                if 'P' in phases:
                    twd = [[-2, 15]]
                if 'S' in phases:
                    twd = [[0,15]]
                if 'R' in phases or 'L' in phases:
                    len_twd = (3.* 1./(cc_settings.bp[1])) + 10
                    twd = [[-10,len_twd]]
        else:
            twd = cc_settings.twd

        if num.isnan(arr):
            logger.debug('no arr - no trace: %s %s %s %s' % (ev.name, st.station, comp, arr))
            trace_list.append(False)
            continue


        # test here vor snr thresholf, tr1cc=False if snr threshold not met
        if cc_settings.snr_calc:
            logger.debug('Computing SNR for station %s, event %s.'
                         % (st.station, ev.name))

            snr = comp_snr(tr1c, arr, twd, st.station, ev.name)
        else:
            snr = 9999

        if snr > cc_settings.snr_thresh:
            if cc_settings.debug_mode:

                logger.info('snuffler debug mode P')
                trace.snuffle(tr1c, markers=[pm.Marker(
                              nslc_ids=[tr1c.nslc_id],
                              tmin=arr + twd[i_ph][0], 
                              tmax=arr + twd[i_ph][1])])
            if cc_settings.debug_mode_S and (comp[-1] in ['N','E','1','2','3']):
                logger.info('snuffler debug mode S')
                trace.snuffle(tr1c, markers=[pm.Marker(
                              nslc_ids=[tr1c.nslc_id],
                              tmin=arr + twd[i_ph][0], 
                              tmax=arr + twd[i_ph][1])])

            try:
                tr1cc = tr1c.chop(tmin=arr + twd[i_ph][0], 
                                  tmax=arr + twd[i_ph][1], 
                                  inplace=False)

                #logger.info('Trace found: %s %s %s' % (ev.name, st.station, comp))
            except trace.NoData:

                logger.debug('snr failed/ no/incomplete trace after cutting: %s %s %s '
                               % (ev.name, st.station, comp))
                tr1cc = False
                n_failed += 1
        else:
            tr1cc = False

        del tr1, tr1c

        trace_list.append(tr1cc)

    logger.info('%i/%i traces successfully preprocessed for %s %s %s' %
                  (len_cat-n_failed, len_cat, st.station, comp, phases[i_ph]))

    del p
    return trace_list


def corr_cal_backup(iterable, n_ev, trace_list, bool_dist, allow_anticor=False):
    '''
    --> to do: allow anticorrelations
    computes cross-correlations, called in parallel.
    :param n_ev: number of events
    :param trace_list: list of preprocessed pyrocko traces
    :param bool_dist: array with boolean values indicating if 
                      inter-event-distance is within threshold, if set.

    '''
    coef_row, tshift_row, weights_row = [], [], []
    for j in range(int(n_ev)):

        if j > iterable:
            coef = 0
            weighting_factor = 0
            tshift = num.nan

        elif iterable == j:
            coef = 1
            weighting_factor = 1
            tshift = 0
        else:
            if not bool_dist[iterable, j]:
                coef = 0
                weighting_factor = 0
                tshift = 0
            else:
                a = trace_list[iterable]
                b = trace_list[j]

                if not a or not b:
                    weighting_factor = 0
                    coef = 0
                    tshift = num.nan
                else:
                    c = trace.correlate(a, b, mode = 'full', normalization= 'normal')

                    if allow_anticor:
                        # calculate indices of extrema via derivatives and sort extrema 
                        ext_ind = num.diff(num.sign(num.diff(c.ydata))).nonzero()[0] + 1
                        ext = c.ydata[ext_ind]
                        ext.sort()
    
    
                        c_min1 = ext[0]
                        c_min2 = ext[1]
                        c_max2 = ext[-2]
                        c_max1 = ext[-1]
    
                        # set largest absolute cross correlation value and get weighting
                        if abs(c_min1) >= c_max1:
                            coef = c_min1
                            weighting_factor = min(abs(c_min1-c_min2), abs(c_max1 + c_min1))
    
                        else:
                            coef = c_max1
                            weighting_factor = min(c_max1-c_max2, c_max1 + c_min1)
    
                        # #remove me later
                        # print(coef,weighting_factor)
                        # plt.plot(range(len(c.ydata)), c.ydata, marker='o')
                        # plt.plot(range(len(c.ydata)),[c_min1 for i in range(len(c.ydata))])
                        # plt.plot(range(len(c.ydata)),[c_min2 for i in range(len(c.ydata))])
                        # plt.plot(range(len(c.ydata)),[c_max1 for i in range(len(c.ydata))])
                        # plt.plot(range(len(c.ydata)),[c_max2 for i in range(len(c.ydata))])

                        # plt.show()

                    else:
                        
                        coef = c.max()[1]
                        tshift = c.max()[0]
                        ext_ind = num.diff(num.sign(num.diff(c.ydata))).nonzero()[0] + 1
                        ext = c.ydata[ext_ind]
                        if len(ext) > 1:
                        # considers an empty sequence now, but how is an empty trace in here?
                            ext.sort()
                            weighting_factor = ext[-1] - ext[-2]
                        else:
                            coef = 0
                            tshift = num.nan
                            weighting_factor = 0

                        # print(iterable, j, coef, weighting_factor)

        #cor_row.append(c) im moment nicht
        coef_row.append(coef)
        tshift_row.append(tshift)
        weights_row.append(weighting_factor)

    return coef_row, tshift_row, weights_row


def plot_cc_over_dist(coef_array_i_ph, ev_ev_dist_array, phase_name, dist_thresh, work_dir):
    '''
    plot all stations cc-values of event pair over distance between events
    currently not used, since not much learned.

    '''

    coef_array_i_ph = num.where(coef_array_i_ph < 0.03, num.nan, coef_array_i_ph)
    
    fig, ax = plt.subplots(figsize=(6,6))
    dist_array_flat = list(num.ndarray.flatten(ev_ev_dist_array))
    bins = range(0, int(dist_thresh), 500)
    dist_bin_indices = num.digitize(dist_array_flat, bins)
    n_bins = len(bins)
    plot_bins = [(bins[i_b-1]+bins[i_b])/2. for i_b in range(n_bins)[1:]]
    for i_st in range(coef_array_i_ph.shape[0]):
        num.fill_diagonal(coef_array_i_ph[i_st], num.nan)
        coef_st_flat = num.ndarray.flatten(coef_array_i_ph[i_st])  
        coef_means = []
        for b in range(n_bins)[1:]:
            coef_means.append(num.nanmean(coef_st_flat[num.where(dist_bin_indices == b)[0]]))
        ax.plot(plot_bins, coef_means, '.', alpha=0.7)

    ax.set_ylabel('cc value', fontsize=10)
    ax.set_xlabel('inter-event dist.', fontsize=10)
    if dist_thresh:
        ax.set_xlim(0, dist_thresh)

    plt.tight_layout()
    fig.savefig('%s/results/cc_over_dist_%s.png' % (work_dir, phase_name))
    plt.close()


def small_imshow_plot(array, events, title, cbarlabel=None, fig_name=None, scale=None):
    '''simple basic imshow plot, keep flexible!'''

    masked_array = num.ma.array(array, mask=num.where(array != 0, False, True))

    fig, ax = plt.subplots(nrows=1, figsize=(5, 5))
    if not scale:
        vmin = -1
        vmax =1
        
        cmap = util_clusty.net_corr_cmap_rdgy()

    else:
        vmin = scale[0]
        vmax = scale[1]
        cmap = 'viridis'

    a = ax.imshow(masked_array, interpolation='nearest', vmin=vmin,
                  vmax=vmax, origin='lower', cmap=cmap)
    ax.set_title(title)
    if len(events) < 30:
        ax.set_yticks(num.arange(len(events)))
        ax.set_yticklabels([str(ev.magnitude)+'_'+util.time_to_str(ev.time)[0:16] for ev in events],
                           fontsize=6)
        ax.set_xticks(num.arange(len(events)))
        ax.set_xticklabels([str(ev.magnitude)+'_'+util.time_to_str(ev.time)[0:16] for ev in events], 
                           rotation=90, fontsize=6)
    else:
        ax.set_ylabel('event index', fontsize=10)
        ax.set_xlabel('event index', fontsize=10)
    cbar = plt.colorbar(a, ax=ax)
    if cbarlabel:
        cbar.set_label(cbarlabel, rotation=90)
    plt.tight_layout()
    if fig_name:
        fig.savefig(fig_name)
        plt.close()
    else:
        plt.show()



def corr_cal(iterable, trace_list, bool_dist, allow_anticor=False):
    '''
    --> to do: allow anticorrelations
    computes cross-correlations, called in parallel.
    :param n_ev: number of events
    :param trace_list: list of preprocessed pyrocko traces
    :param bool_dist: array with boolean values indicating if 
                      inter-event-distance is within threshold, if set.

    '''
    n_ev = len(trace_list)

    coef_row, tshift_row, weights_row = [], [], []
    for j in range(n_ev):

        if j > iterable:
            coef = 0
            weighting_factor = 0
            tshift = num.nan

        elif iterable == j:
            coef = 1
            weighting_factor = 1
            tshift = 0
        else:
            if not bool_dist[iterable, j]:
                coef = 0
                weighting_factor = 0
                tshift = 0
            else:
                a = trace_list[iterable]
                b = trace_list[j]
    
                if not a or not b:
                    weighting_factor = 0
                    coef = 0
                    tshift = num.nan
                else:
                    c = trace.correlate(a, b, mode = 'full', normalization= 'normal')

                    if allow_anticor:
                        # calculate indices of extrema via derivatives and sort extrema 
                        ext_ind = num.diff(num.sign(num.diff(c.ydata))).nonzero()[0] + 1
                        ext = c.ydata[ext_ind]
                        ext.sort()
    
    
                        c_min1 = ext[0]
                        c_min2 = ext[1]
                        c_max2 = ext[-2]
                        c_max1 = ext[-1]
    
                        # set largest absolute cross correlation value and get weighting
                        if abs(c_min1) >= c_max1:
                            coef = c_min1
                            weighting_factor = min(abs(c_min1-c_min2), abs(c_max1 + c_min1))
    
                        else:
                            coef = c_max1
                            weighting_factor = min(c_max1-c_max2, c_max1 + c_min1)

                    else:
                        
                        coef = c.max()[1]
                        tshift = c.max()[0]
                        ext_ind = num.diff(num.sign(num.diff(c.ydata))).nonzero()[0] + 1
                        ext = c.ydata[ext_ind]
                        if len(ext) > 1:
                            ext.sort()
                            weighting_factor = ext[-1] - ext[-2]
                        else:
                            coef = 0
                            tshift = num.nan
                            weighting_factor = 0

                        # print(iterable, j, coef, weighting_factor)

        #cor_row.append(c) im moment nicht
        coef_row.append(coef)
        tshift_row.append(tshift)
        weights_row.append(weighting_factor)

    return coef_row, tshift_row, weights_row




def get_sta_wise_similarity(i_st, st, catalog, cc_settings, waveform_dir, 
                          arrivals, comp, i_ph, phases, bool_dist, n_workers):
    
        
    ###### THIS IS THE LEAK!!!

    # track.print_diff()
    # print('-----------before trace_list -----------------')
    # track.print_diff()
    # print('----------------------------------------------')

    logger.info('Starting cross-correlation calculations for station %s (%s phase on component %s)' %(st.station, phases[i_ph], comp))
    trace_list = make_preprocessed_trace_list(i_st, st, catalog, cc_settings, 
                                              waveform_dir, arrivals, comp, i_ph, phases)

    # print('-----------after trace_list -----------------')
    # track.print_diff()
    # print('----------------------------------------------')


    n_ev = len(trace_list)
    
    sta_coef_array = np.zeros((n_ev,n_ev))
    sta_tshifts_array = np.zeros((n_ev,n_ev))
    sta_weights_array = np.zeros((n_ev,n_ev))

    iterable = np.arange(len(trace_list))

    ###########PARALLEL###############
    
    # establish ProcessPool, return results from corr_cal via map 
    # function (iterating over each event)
    # corr_cal(iterable, trace_list, bool_dist, allow_anticor=False)

    corr_cal_partial = partial(corr_cal, trace_list=trace_list,bool_dist=bool_dist)
    # with ProcessPoolExecutor(max_workers=n_workers) as executor:
    with Pool(processes=n_workers) as pool:
        results = pool.map(corr_cal_partial, iterable, chunksize=len(iterable)//n_workers)
    
    sta_coef_array, sta_tshifts_array, sta_weights_array = zip(*results)
    # c = coef_array[i_st].copy()
    # num.fill_diagonal(coef_array[i_st], 0.)
    # if num.max(coef_array) < 0.05:
    #     logger.warning('%s %s: all cc 0' % (st.station, comp_base))
    
    # # add correlation coeeficients (symetric for correlation coefficient)
    # A = num.array(coef_array[i_st])
    # coef_array[i_st] = A + A.T - num.diag(num.diag(A))
    
    # A = num.array(weights_array[i_st])
    # weights_array[i_st] = A + A.T - num.diag(num.diag(A))
    # print('-----------after paralell -----------------')
    # track.print_diff()
    # print('----------------------------------------------')
    del results
    pool.close()
    return sta_coef_array, sta_tshifts_array, sta_weights_array


def get_similarity_matrix(comp, station_list, catalog, cc_settings, waveform_dir, 
                          arrivals, bool_dist, n_workers, work_dir):

    '''
    main function for calculation of cross-correlations for all 
    station-event-event combinations.

    :param station_list: list of pyrocko stations
    :param catalog: list of pyrocko events
    :param cc_settings: cross-correlation settings as defined in config file
    :param waveform_dir: path to directory containing waveforms
    :param arrivals: precalculated arrival times for all 
                     phases, stations, events.
    :param n_workers: number of used cores                

    '''
    logger.info('Computing cc similarity matrix.')
    #num_cores = multiprocessing.cpu_count() -1
    
    logger.info('Using %s cores' % n_workers)

    # track = tracker.SummaryTracker()

    n_ev = len(catalog)
    n_st = len(station_list)

    phases = num.asarray([ph.upper() for ph in cc_settings.phase])
    
    comp_base = comp[-1]

    if comp_base == 'Z':
        if 'P' in phases:
            i_ph = num.argwhere(phases=='P')[0,0]
        elif 'R' in phases:
            i_ph = num.argwhere(phases=='R')[0,0]
        else:
            # do not want to use Z trace for S and L phases.
            exit_flag =1 #review gesa

    elif comp_base in ['N', 'E', '1', '2', '3']:

        if 'S' in phases:
            i_ph = num.argwhere(phases=='S')[0,0]
        elif 'L' in phases:
            i_ph = num.argwhere(phases=='L')[0,0]
        elif 'R' in phases:
            i_ph = num.argwhere(phases=='R')[0,0]
            # L and R have same arrival time, therefore it does not matter which 
            # i_ph is used with N and E component, i_ph is only used to get correct
            # arrival time

        else:
            # do not want to use horizontal traces for P phases.
            exit_flag = 1     #review gesa 

    logger.debug('%s %s' % (comp_base,i_ph))
    
    coef_array = num.zeros((n_st, n_ev, n_ev))
    tshifts_array = num.zeros((n_st, n_ev, n_ev))
    weights_array = num.zeros((n_st, n_ev, n_ev))


    for i_st, st in enumerate(station_list):

        logger.info('%s %s %s %s %s ' %(st.station, waveform_dir, comp, i_ph, phases))
        coef_array[i_st], tshifts_array[i_st], weights_array[i_st] = get_sta_wise_similarity(i_st, st, catalog, cc_settings, 
                                                                             waveform_dir, arrivals, comp, i_ph, phases, bool_dist, n_workers)
        gc.collect()

        if cc_settings.cc_plots:
            title = '%s/results/cc_%s.%s_%s' % (work_dir, st.network, st.station, comp_base)
            small_imshow_plot(coef_array[i_st], catalog, title, cbarlabel='cc', 
                              fig_name=title+'.png', scale=[0,1])
        # logger.info('Station: %s:::%.2f' % (st.station,time.time()))

    num.save(arr=coef_array, file='%s/precalc_arrays/cccoef_array_%s' % (work_dir, comp_base))
    num.save(arr=weights_array, file='%s/precalc_arrays/ccweights_array_%s' % (work_dir, comp_base))
    num.save(arr=tshifts_array, file='%s/precalc_arrays/cctshifts_array_%s' % (work_dir, comp_base))
    # logger.info('Finished Processing component %s:::%.2f' % (comp_base,time.time()))