import sys
import numpy as num 

from pyrocko import model, util

'''
Small script to generate hypoDD input files based on clusty's 
cross-correlation and time shift matrices.

Make sure to use the precalc_arrays that belong to your catalog and settings ;-)
And check conventions/ settings for differential times (e.g., t1-t2 vs. t2-t1).

In case you find any bugs, please let us know...

'''


### adjust settings:
phases = ['P','S']
stations = model.load_stations('used_stations.pf')
events = model.load_events('results/picks_ph2tt_input_monroe_LassieEvents_Jun14.yaml', format='yaml')

# threshold to include cc measurement for hypoDD input file:
cc_threshold = 0.8

# outfilename:
outfile = 'dt_2-6Hz_0-100km_cc%s.cc' % (cc_threshold)



#############################################################
#############################################################

n_ev = len(events)

# Origin time correction relative to the event origin time reported in the catalog data.
# Should be zero unless a different catalog is used for pick data...
otc = 0.0

outlines = []

### this is very memory conuming in case of large cc arrays (=many events)
# matrix dimensions: n_stats*n_ev*n_ev
cc_arrayP = num.load('precalc_arrays/cccoef_array_Z.npy')
cctshifts_arrayP = num.load('precalc_arrays/cctshifts_array_Z.npy')

assert n_ev == cc_arrayP.shape[1]

cc_arrayS1 = num.load('precalc_arrays/cccoef_array_N.npy')
cc_arrayS2 = num.load('precalc_arrays/cccoef_array_E.npy')

cctshifts_arrayS1 = num.load('precalc_arrays/cctshifts_array_N.npy')
cctshifts_arrayS2 = num.load('precalc_arrays/cctshifts_array_E.npy')



for i_ev1, ev1 in enumerate(events):
	for i_ev2, ev2 in enumerate(events):
		if i_ev1 <= i_ev2:
			continue

		#print(util.tts(ev1.time), util.tts(ev2.time))

		header = '# %s %s %s\n' % (ev1.name, ev2.name, otc)
		ev1_ev2_lines = []

		dt_orig = ev2.time - ev1.time
		orig_ev1 = ev1.time

		for ph in phases:

			for i_st, st in enumerate(stations):

				if ph in ['p', 'P']:
					ccmax = cc_arrayP[i_st,i_ev1,i_ev2]
				
					if ccmax < cc_threshold:
						continue

					dt = cctshifts_arrayP[i_st,i_ev1,i_ev2] -dt_orig

				elif ph in ['s', 'S']:
					ccmax1 = cc_arrayS1[i_st,i_ev1,i_ev2]
					ccmax2 = cc_arrayS2[i_st,i_ev1,i_ev2]
					if ccmax1 > ccmax2:
						ccmax = ccmax1
						dt = cctshifts_arrayS1[i_st,i_ev1,i_ev2] -dt_orig
					else:
						ccmax = ccmax2
						dt = cctshifts_arrayS2[i_st,i_ev1,i_ev2] -dt_orig

					if ccmax < cc_threshold:
						continue

				wght = ccmax

				#print(dt, ev1.time, ev2.time, ev2.time-ev1.time)
				#input()
				
				obsline = '%s %s %s %s\n' % (st.station, dt*-1, wght, ph) 
					## hDD definition of d tt times is opposed to clusty

				ev1_ev2_lines.append(obsline)
				#print('obs found')

		if len(ev1_ev2_lines)>0:
			outlines.append(header)
			outlines = outlines + ev1_ev2_lines

with open(outfile, 'w') as f:
	for l in outlines:
		f.write(l)
