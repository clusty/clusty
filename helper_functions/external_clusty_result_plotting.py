from pyrocko import guts, model
from clusty.config import clusty_config
from clusty.clustering import map_plot_carto

import matplotlib.pyplot as plt
import cartopy
import cartopy.crs as ccrs

#import pyproj # if a projection of data is needed before plotting

'''
This is a dummy example how to export Clusty's cartopy plot for further
proccessing/refinement

'''

# set config file path
config_file = 'path_to_your_clusty_config_file/config_clusty_gardanne.yaml'

# load/read the config file
try:
    cnf_all = clusty_config.load(filename=config_file)
except guts.ArgumentError as e:
    false_arg = str(e).split(':')[1]
    logger.error('Invalid argument in config file: %s' %false_arg)
    logger.error('--> check config file')
    sys.exit()

try:
    basic_cnf, cc_cnf, netsim_cnf, cl_cnf = cnf_all.settings
except ValueError:
    basic_cnf, cc_cnf, netsim_cnf, cl_cnf, mf_cnf = cnf_all.settings


# preparing the input for clusty's cartopy map plotting function

# set clustering parameters, components, etc.
freeparam = 0.180
min_pts = 4
comp_name ='comb'

# building the method string...
method = '%s_%s_%s_%.3f_%s' % (
                    netsim_cnf.method_similarity_computation,
                    cl_cnf.method, comp_name, freeparam, min_pts)

work_dir = 'your_working_dir'
# might also use the method name here
catalog = model.load_events('Path_to_your_catalog/cat_trimmed_mean_ccs_dbscan_comb_0.180_4.yaml', format='yaml')

# call the clusty map plotting function
fig,ax = map_plot_carto(catalog, method, cc_cnf, cl_cnf, work_dir, mt_type = 'dc', fn=False)

# depending whether local or geographical coordinates are used ygeo/ortho are needed
geo = ccrs.Geodetic()
ortho = ccrs.PlateCarree()

# figure and axes objects can be used as usual wih matplotlib

# you might add stuff here, e.g. faults, mine geometry,etc.
# or set other map ranges, whatever you want
ax.set_ylim(43.43,43.455)

#save the figure
fig.savefig('test.pdf', dpi=100)