import sys
import os.path as op
import numpy as num 

from pyrocko import model, util

'''
Small script to generate hypoDD and growclust input files based on clusty's 
cross-correlation and time shift matrices.

Make sure to use the precalc_arrays that belong to your catalog and settings ;-) 
And check conventions/ settings for differential times (e.g., t1-t2 vs. t2-t1).

In case you find any bugs, please let us know...

'''


### adjust settings:
phases = ['P','S']
path = '/home/peter/working_dir/FORGE/FOR/clusty_circulation/20_40Hz_pdom_upsampled_FORx'
cat_name = 'JOINED_LOKI_mags_toi_clean_manual.yaml'

stations = model.load_stations(op.join(path,'used_stations.yaml'))
events = model.load_events(op.join(path,'results',cat_name), format='yaml')[:100]


# threshold to include cc measurement for differential time input file:
cc_threshold = 0.8

# outfilename:
outfile = 'dt_20-40Hz_cc%s.cc_new' % (cc_threshold)

output_type = 'hypoDD'
#output_type = 'growclust'

#############################################################
#############################################################

n_ev = len(events)

# Origin time correction relative to the event origin time reported in the catalog data.
# Should be zero unless a different catalog is used for pick data...
otc = 0.0

outlines = []

### this is very memory intensiv in case of large cc arrays (=many events)
# matrix dimensions: n_stats*n_ev*n_ev
cc_arrayP = num.load(op.join(path,'precalc_arrays','cccoef_array_Z.npy'))
print(num.shape(cc_arrayP))
cctshifts_arrayP = num.load(op.join(path,'precalc_arrays','cctshifts_array_Z.npy'))
#assert n_ev == cc_arrayP.shape[1]

cc_arrayS1 = num.load(op.join(path,'precalc_arrays','cccoef_array_N.npy'))
cc_arrayS2 = num.load(op.join(path,'precalc_arrays','cccoef_array_E.npy'))

cctshifts_arrayS1 = num.load(op.join(path,'precalc_arrays','cctshifts_array_N.npy'))
cctshifts_arrayS2 = num.load(op.join(path,'precalc_arrays','cctshifts_array_E.npy'))

# use only largest CC of the horizontal components of each stations
# and the corresponding lag time
cc_arrayS = num.zeros_like(cc_arrayS1)
cctshifts_arrayS = cctshifts_arrayS2.copy()

#correct for event origin time difference
t = num.asarray([ev.time for ev in events])
dt_orig = t -t[:,num.newaxis]


for ista in range(num.shape(cc_arrayS1)[0]):

    # use only largest CC of the horizontal components of each stations
    # and the corresponding lag time
    cc_arrayS[ista] = num.maximum(cc_arrayS1[ista],cc_arrayS2[ista])
    inds_ccS =  cc_arrayS1[ista] > cc_arrayS2[ista]
    cctshifts_arrayS[ista,inds_ccS] = cctshifts_arrayS1[ista,inds_ccS]

    #correct for event origin time difference
    cctshifts_arrayS[ista] -= dt_orig
    cctshifts_arrayP[ista] -= dt_orig


with open(outfile, 'w') as f:
    for i_ev1, ev1 in enumerate(events):
        for i_ev2, ev2 in enumerate(events):
            if (i_ev1%100 == 0 and i_ev2%100 == 0):
                print(i_ev1,'|',i_ev2, 'of', n_ev)
            if i_ev1 <= i_ev2:
                continue

            #print(util.tts(ev1.time), util.tts(ev2.time))
            if output_type.lower() in ['growclust']:
                header = '#   %s   %s   %.3f\n' % (str(i_ev1).zfill(6), str(i_ev2).zfill(6), otc)
            elif output_type.lower() in ['hypodd']:
                header = '# %s %s %s\n' % (ev1.name, ev2.name, otc)
            cc_lines = []

            for ph in phases:
                for i_st, st in enumerate(stations):

                    if ph in ['p', 'P']:
                        cc = cc_arrayP[i_st,i_ev1,i_ev2]
                        dt = cctshifts_arrayP[i_st,i_ev1,i_ev2]

                    elif ph in ['s', 'S']:
                        cc = cc_arrayS[i_st,i_ev1,i_ev2]
                        dt = cctshifts_arrayS[i_st,i_ev1,i_ev2]

                    if cc >= cc_threshold:
                        if output_type.lower() in ['growclust']:
                            cc_line = '    %s %s %s %s\n' % (st.station, dt, cc, ph)
                        elif output_type.lower() in ['hypodd']:
                            cc_line = '%s %s %s %s\n' % (st.station, dt*-1, cc, ph)
                            ## hDD definition of d tt times is opposed to clusty
                        cc_lines.append(cc_line)


            if len(cc_lines)>0:
                f.write(header)
                for line in cc_lines:
                    f.write(line)