from pyrocko import model, util, orthodrome
from pyrocko.client import fdsn
from pyrocko.io import stationxml
import numpy as num                                                             
import glob                                                                     
import os                                                                       


save_data_dir = ''

# catalog file in pyrocko format
catalog = model.load_events('')

# station file in pyrocko format
stations = model.station.load_stations(
    filename='')

st_blocklist = ['PLIT', 'RABC', 'SMRN', 'VIRC']

t_min = -600 # [s] before origin time
t_max = 1800

max_dist = 100000  # [m]

repositories = ['noa', 'geofon', 'orfeus', 'iris', 'ingv']

priority_band_code = 'M,B,H,S,E'


# check if all events have names
cat = []
for ev in catalog:
    if not ev.name:
        ev.name = util.tts(ev.time).replace(' ','_')
    cat.append(ev)


os.makedirs(save_data_dir, exist_ok=True)


print('Events:', len(cat))
print('Stations:', len(stations))


dists = num.empty((len(cat), len(stations)))                             
st_lats = [st.lat for st in stations]                                       
st_lons = [st.lon for st in stations]                                       
for i_ev, ev in enumerate(cat):                                              
    dists[i_ev, :] = [float(orthodrome.distance_accurate50m_numpy(              
                          ev.lat, ev.lon, lat, lon))                            
                          for (lat, lon) in zip(st_lats, st_lons)]

for i_ev, ev in enumerate(cat):
    print('EV:', i_ev)
    ev_str = util.time_to_str(ev.time).replace(' ', '_')
    dir_ = os.path.join(save_data_dir, ev_str, 'waveforms/raw/')
    os.makedirs(dir_, exist_ok=True)

    t_start = ev.time + t_min
    t_end = ev.time + t_max

    for i_st, st in enumerate(stations):
        have_data = False
        print('EV, ST:', i_ev, i_st, st.network, st.station)
        mseed_fn_st = '%sdata_%s.%s_'% (dir_, st.network, st.station)


        if dists[i_ev, i_st] <= max_dist:
                print('Dist: %s' % dists[i_ev, i_st])
                                                                 
                priority_band_code_ = priority_band_code
                                                                                    
                for c in priority_band_code_:
                    if have_data is False:
                                                                                    
                        if st.station not in st_blocklist:
                            
                            selection = [(st.network, st.station, '*',
                                          '%s*' % c, t_start, t_end)]

                            for site in repositories:
                                 
                                mseed_fn = mseed_fn_st + ev_str[0:10] +'.mseed'

                                try:
                                    request_waveform = fdsn.dataselect(site=site,
                                                                       selection=selection)
                                                                                
                                    with open(mseed_fn, 'wb') as wffile:
                                        wffile.write(request_waveform.read())
                                    print('saved %s' % mseed_fn)
                                    have_data = True
                                    break                                       
                                                                                
                                except fdsn.EmptyResult:
                                    continue

                                except:
                                    print('timeout')
                                    continue