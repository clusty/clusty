from pyrocko import model, orthodrome, moment_tensor
from pyrocko.plot import beachball

import numpy as num
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import scipy.special as sspec

def  Arup_from_mw(Mw):
    '''
    Wells and Coppersmith (1994)
    '''
    A = 10**((Mw-3.49)/0.91)

    return A

def get_var_scaling(dim=3):
    # variance scaling for ellipse plotting
    if dim == 3:
        # variance for a and b so that 95% in 3 dimension within ellipsoid
        a = sspec.erfinv(.68**(1/3))*num.sqrt(2) 
    elif dim == 2:
        # variance for a and b so that 95% in 2 dimension within ellipse
        a = sspec.erfinv(.68**(1/2))*num.sqrt(2) 
    elif dim == 1:
        # variance for axis so that 95% (quasi 1D)
        a = sspec.erfinv(.68**(1/1))*num.sqrt(2) 

    return a

def rot_z(input_arr, angle, degree= True):
    rotz = num.array([[num.cos(angle), -num.sin(angle),0],
                      [num.sin(angle), num.cos(angle), 0],
                      [0,0,1]])

    rot_arr = (rotz@input_arr.T).T

    return rot_arr

def rot_y(input_arr, angle, degree= True):
    roty = num.array([[num.cos(angle),0,num.sin(angle)],
                    [0,1,0],
                    [-num.sin(angle),0,num.cos(angle)]])

    rot_arr = (roty@input_arr.T).T

    return rot_arr

def project_to_plane(locs,mean,normal):
    
    locs_proj = num.zeros_like(locs)
    for i, loc in enumerate(locs):
        try:
            Q = num.array([loc[0,0],loc[0,1],loc[0,2]])
        except IndexError as e:
            Q = num.array([loc[0],loc[1],loc[2]])        
    
        v = Q - mean
        dist = num.dot(v,normal)
        projected_point = Q - dist * normal
        
        locs_proj[i,0] = projected_point[0]
        locs_proj[i,1] = projected_point[1]
        locs_proj[i,2] = projected_point[2]

    return locs_proj

def pca_analysis(cluster_catalog, mt_ref_catalog, lat0, lon0, min_mag = 1, process_clusters='all'):

    clusters = num.sort(list(set([ev.extras['cluster_number'] for ev in cluster_catalog])))

    a2 = get_var_scaling(2)
    a3 = get_var_scaling(3)
    
    if process_clusters == 'all':
        cluster_list = clusters[1:]
    else:
        cluster_list = process_clusters

    for cl_nr in cluster_list:
        print('------- Cluster %i ---------' % cl_nr)
    
        cat_cluster = [ev for ev in cluster_catalog if ev.extras['cluster_number']==cl_nr and ev.magnitude >= min_mag]

        cluster_color = cat_cluster[0].extras['color']

        ev_ref = [ev for ev in mt_ref_catalog if ev.extras['cluster_number']==cl_nr][0]

        mt = moment_tensor.MomentTensor(strike=ev_ref.moment_tensor.strike1, 
                                        dip=ev_ref.moment_tensor.dip1, 
                                        rake=ev_ref.moment_tensor.rake1, 
                                        scalar_moment=ev_ref.moment_tensor.moment)

        lats = num.array([ev.lat for ev in cat_cluster])
        lons = num.array([ev.lon for ev in cat_cluster])
        depths = num.array([-num.abs(ev.depth) for ev in cat_cluster])

        mean_lats = num.mean(lats)
        mean_lons = num.mean(lons)

        north, east = orthodrome.latlon_to_ne_numpy(mean_lats,mean_lons,lats,lons)
        data_3d = num.stack((east, north, depths)).T

        cat_len = len(cat_cluster)
        
        mags = [ev.magnitude+0.3 for ev in cat_cluster]

        cuml_mag = moment_tensor.moment_to_magnitude(num.sum([moment_tensor.magnitude_to_moment(m) for m in mags]))

        A = Arup_from_mw(cuml_mag)

        # #assuming a fixed L/W ratio of 2
        # L = num.sqrt(A)
        # W = 0.5*L

        # assuming square-shaped fault
        L = num.sqrt(A)

        if cat_len < 5:
            continue

        #Standardize event locations (removing mean and scaling to unit variance)   
        mean_vec = num.mean(data_3d, axis=0)
        std_vec = num.std(data_3d, axis=0)
    
        #linear scaling
        X_std3 = data_3d / num.linalg.norm(data_3d)
        X_std2 = data_3d[:,:2] / num.linalg.norm(data_3d[:,:2])
    
        lin3 = num.linalg.norm(data_3d)
        lin2 = num.linalg.norm(data_3d[:,:2])
    
        pca3 = PCA(n_components=3)
        pca3.fit(X_std3)
    
        pca2 = PCA(n_components=2)
        pca2.fit(X_std2)
    
        #####SETUP SUBPLOTS####
        fig = plt.figure(figsize = (15,5))
        ax1 = fig.add_subplot(131)
        ax2 = fig.add_subplot(132, projection='3d')
        ax3 = fig.add_subplot(133, projection='3d')

        ax1.set_aspect('equal')

        ax1.set_title('Nodal planes projected to surface\n2D PCA fit')
        ax2.set_title('3D PCA fit')
        ax3.set_title('Projection onto DC planes')

        # Create cubic bounding box to simulate equal aspect ratio
        x_max = max(data_3d[:,0])
        x_min = min(data_3d[:,0])
        y_max = max(data_3d[:,1])
        y_min = min(data_3d[:,1])
        z_max = max(data_3d[:,2])
        z_min = min(data_3d[:,2])
    
        # Comment or uncomment following lines for fake bounding box:
        max_range = num.array([x_max-x_min, y_max-y_min, z_max-z_min]).max()
        Xb = 0.5*max_range*num.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(x_max+x_min)
        Yb = 0.5*max_range*num.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(y_max+y_min)
        Zb = 0.5*max_range*num.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(z_max+z_min)
                
        for ax3D in [ax2,ax3]:
            for xb, yb, zb in zip(Xb, Yb, Zb):
                ax3D.plot([xb], [yb], [zb], 'w')
            ax3D.set_aspect('equal')


        #####INITIALIZE NODAL PLANES AND PCA ELLIPSES/ELLIPSOIDS####

        #ellipse
        cords = num.zeros((500,3))
        t = num.linspace(0,2*num.pi,500)

        cords[:,0] = a3*num.sqrt(pca3.explained_variance_[0])*num.sin(t)*lin3
        cords[:,1] = a3*num.sqrt(pca3.explained_variance_[1])*num.cos(t)*lin3

        #rectangular
        cords_plane = num.zeros([5,3])
        cords_plane[:,1] = num.array([0.5,0.5,-0.5,-0.5,0.5]) * L*1000
        cords_plane[:,0] = num.array([0.5,-0.5,-0.5,0.5,0.5]) * L*1000

        ####ROTATIONS#####
        # rotate prepared ellipse in 3D according eigenvectors of PCA
        rot_elli = (pca3.components_.T@cords.T).T + mean_vec

        # rotate fault(nodal) planes of moment tensor 
        # with convention corrections
        r2d = 180. / num.pi
        d2r = 1. / r2d

        strike1_rad = (-mt.strike1)*d2r
        dip1_rad = (mt.dip1)*d2r

        strike2_rad = (-mt.strike2)*d2r
        dip2_rad = (mt.dip2)*d2r

        # rotate in dip and strike translate back to mean
        plane1 = rot_y(cords_plane, dip1_rad)
        plane_mt1 = rot_z(plane1, strike1_rad) + mean_vec
    
        plane2 = rot_y(cords_plane, dip2_rad)
        plane_mt2 = rot_z(plane2, strike2_rad) + mean_vec

        #####ADD DATA TO SUBPLOTS####
        ####2D###

        # clustered events
        ax1.scatter(data_3d[:,0],data_3d[:,1],alpha=0.7, c=cluster_color, edgecolors='k')
        
        for length, vector in zip(pca2.explained_variance_, pca2.components_):
            # eigenvectors with length corresponding to 95% covariance of 2d ellipse
            v = vector * a2 * num.sqrt(length) * lin2

            ax1.plot([mean_vec[0]-v[0],mean_vec[0]+v[0]],
                     [mean_vec[1]-v[1],mean_vec[1]+v[1]], c='k', alpha =0.5, ls=':')
        
        ####3D#####

        #clustered events
        ax2.scatter(data_3d[:,0],data_3d[:,1], data_3d[:,2],alpha=0.7, c=cluster_color, edgecolor='k')

        for ax3D in [ax2,ax3]:
            ax3D.plot(rot_elli[:,0],rot_elli[:,1],rot_elli[:,2], alpha=0.8, c='k')
                
            #add half axes of ellipse
            for length, vector in zip(pca3.explained_variance_, pca3.components_):
                #eigenvectors with length corresponding to 95% covariance of 3d_ellipsoid
                v = vector * a3 * num.sqrt(length)*lin3
                ax3D.plot([mean_vec[0],mean_vec[0]+v[0]],[mean_vec[1],mean_vec[1]+v[1]],[mean_vec[2],mean_vec[2]+v[2]], c='k')
    

        n_faults = num.zeros((2,3))
        for i_el, plane in enumerate([plane_mt1,plane_mt2]):  
            # 2D fault plane projections
            x = num.array(plane.T[0]).flatten()
            y = num.array(plane.T[1]).flatten()


            ax1.fill(x,y, c='grey', alpha =0.5)
            ax1.plot([x[1],x[2]],[y[1],y[2]], lw=2, c='grey')

            # 3D fault planes
            z = num.array(plane.T[2]).flatten()
            ax3.plot(plane[:,0],plane[:,1],plane[:,2],c='grey')
            
      
            n_f = num.cross(plane.T[:,2]-plane.T[:,1], plane.T[:,0]-plane.T[:,1])
            n_faults[i_el] =n_f/num.linalg.norm(n_f)

        n1 = n_faults[1]
        n2 = n_faults[0]
        
        xlims = ax1.get_xlim()
        ylims = ax1.get_ylim()

        pos = (xlims[0]+0.1*(xlims[1]-xlims[0]),ylims[0]+0.9*(ylims[1]-ylims[0]))

        beachball.plot_beachball_mpl(
           mt, ax1,
           beachball_type='dc',
           size=20,
           linewidth=1, position=pos ,color_t=cluster_color)


        locs_proj1 = project_to_plane(data_3d,mean_vec,n1)
        locs_proj2 = project_to_plane(data_3d,mean_vec,n2)

        for loc_proj, col in zip([locs_proj1, locs_proj2], ['b','r']): 
            ax3.scatter(loc_proj[:,0],loc_proj[:,1], loc_proj[:,2], alpha=0.6, c=col)


        fig.suptitle('Cluster %i' % cl_nr)

        plt.show()

#cat = model.load_events('/home/user/clusty/clustering_result_catalog.yaml', format='yaml')
#mts = model.load_events(filename='/home/user/clusty/representative_mt_catalog.yaml', format='yaml')

#pca_analysis(cat,mts,lon0=37.5,lat0=20)