from pyrocko import model
import plotly.graph_objects as go
import numpy as num

def plotly_3d(cat_path, station_path):
    cat = model.load_events(cat_path, format='yaml')
    stations = model.load_stations(station_path)
    cluster_list = num.array([ev.extras['cluster_number'] for ev in cat])
    clusters = set(cluster_list)
    
    fig = go.Figure()

    lon_sta = num.array([sta.lon for sta in stations])
    lat_sta = num.array([sta.lat for sta in stations])
    sta_names = num.array([sta.station for sta in stations])
    lon = num.array([ev.lon for ev in cat])
    lat = num.array([ev.lat for ev in cat])
    col = num.array([ev.extras['color'] for ev in cat])
    mag = num.array([2**ev.magnitude/3 for ev in cat])
    depth = num.array([-ev.depth for ev in cat])
    ev_names = num.array([ev.name for ev in cat])
    unclustered_mask = cluster_list != -1
    
    trace2_dummy = {
      "name": "setosa", 
      "type": "mesh3d", 
      "x": [], 
      "y": [], 
      "z": [], 
      "color": "#6fff00", 
      "opacity": 0.4, 
      "alphahull": -1
    }
    
    
    # fig.add_trace(go.Scatter3d(
    #               x=lon[~unclustered_mask],
    #               y=lat[~unclustered_mask],
    #               z=depth[~unclustered_mask],
    #               mode='markers',
    #               marker=dict(
    #                   size=mag[~unclustered_mask],
    #                   color=col[~unclustered_mask],
    #                   opacity=0.2)))

    # fig.add_trace(go.Scatter3d(
    #               x=lon[unclustered_mask],
    #               y=lat[unclustered_mask],
    #               z=depth[unclustered_mask],
    #               mode='markers',
    #               text=ev_names,
    #               marker=dict(
    #                   size=mag[unclustered_mask],
    #                   color=col[unclustered_mask],
    #                   opacity=0.2))
    #               )

    # fig.add_trace(go.Scatter3d(
    #               x=lon_sta,
    #               y=lat_sta,
    #               z=num.zeros_like(lon_sta),
    #               mode='markers',
    #               text=sta_names,
    #               marker=dict(
    #                   symbol='square',
    #                   size=2,
    #                   color='#000000',
    #                   opacity=0.5))
    #              )
    for c in clusters:
        print(c)
        if c != -1:
            c_mask = cluster_list == c
            lon_ = lon[c_mask]
            lat_ = lat[c_mask]
            depth_ = depth[c_mask]

            lon_mean = num.mean(lon_)
            lat_mean = num.mean(lat_)
            depth_mean = num.mean(depth_)

            lon_std = num.std(lon_)
            lat_std = num.std(lat_)
            depth_std = num.std(depth_)

            lon_limit1 = num.mean(lon_) - num.std(lon_)*2
            lat_limit1 = num.mean(lat_) - num.std(lat_)*2
            depth_limit1 = num.mean(depth_) - num.std(depth_)*2

            lon_limit2 = num.mean(lon_) + num.std(lon_)*2
            lat_limit2 = num.mean(lat_) + num.std(lat_)*2
            depth_limit2 = num.mean(depth_) + num.std(depth_)*2

            lon_clean =[]
            lat_clean = []
            depth_clean = []

            for lo,la,de in zip(lon_,lat_,depth_):
                if lon_limit1 <= lo <=  lon_limit2 and lat_limit1 <= la <=  lat_limit2:#and depth_limit1 <= de <=  depth_limit2:
                  lon_clean.append(lo)
                  lat_clean.append(la)
                  depth_clean.append(de)
                  print(lo,la,de)
            fig.add_trace(go.Mesh3d(
                          x=lon_clean,
                          y=lat_clean,
                          z=depth_clean,
                          alphahull=-1,
                          color=col[c_mask][0],
                          opacity=0.2))

    camera = dict(
    up=dict(x=0, y=0, z=1),
    center=dict(x=-0.4, y=-0.3, z=0.3),
    eye=dict(x=0, y=-1.5, z=1.1))

    fig.update_layout(scene_camera=camera,
                      scene = dict(
                        xaxis=dict(zeroline=False,
                                    title= 'longitude (degree)'),
                        yaxis=dict(zeroline=True,
                                    title= 'latitude (degree)'),
                        zaxis=dict(zeroline=False,
                                    title= 'depth (m)'),
                        aspectratio=dict(x=1, y=1, z=0.2)),
                      showlegend=False)
    fig.show()

    # for idx, clust in enumerate(clusters):

    #   dict_scatter = trace1_dummy.copy()
    #   dict_hull  = trace2_dummy.copy()

    #   clust_str = '%i' %clust

    #   x = [ev.lon for ev in cat if ev.extras['cluster_number'] == clust]
    #   y = [ev.lat for ev in cat if ev.extras['cluster_number'] == clust]
    #   z = [-ev.depth for ev in cat if ev.extras['cluster_number'] == clust]

    #   dict_scatter['x'] = x
    #   dict_scatter['y'] = y
    #   dict_scatter['z'] = z

    #   dict_scatter["name"] = 'Cluster %i' % int(clust)
    #   dict_scatter['text'] = [int(clust) for i in range(len(x))]

    #     dict_hull['x'] = x
    #     dict_hull['y'] = y
    #     dict_hull['z'] = z

    #     dict_hull["color"] = col_dict_plotly[clust_str]
    #     dict_hull["name"] = 'Cluster %i' % int(clust)
    #     fig.add_trace(dict_hull)

    #   fig.add_trace(dict_scatter)


def plot_merged(cat_path):

    cat_merged = model.load_events(cat_path, format='yaml')
    
    fig = go.Figure()

    lon = [ev.lon for ev in cat_merged if ev.extras['cluster_number'] != -1]
    lat = [ev.lat for ev in cat_merged if  ev.extras['cluster_number'] != -1]
    names = [ev.name for ev in cat_merged if ev.extras['cluster_number'] != -1]
    mag = [2**ev.magnitude/2 for ev in cat_merged if ev.extras['cluster_number'] != -1]
    col = [ev.extras['color'] for ev in cat_merged if ev.extras['cluster_number'] != -1]

    for ev in cat_merged:
        if ev.extras['cluster_number'] != -1:
            first = num.where(num.array(ev.extras['origins']) == True)[0][0]

    # fig.update_layout(
    #    geo = dict(
    #        showland = True,
    #        landcolor = "rgb(212, 212, 212)",
    #        subunitcolor = "rgb(255, 255, 255)",
    #        countrycolor = "rgb(255, 255, 255)",
    #        showlakes = True,
    #        lakecolor = "rgb(255, 255, 255)",
    #        showsubunits = True,
    #        showcountries = True,
    #        resolution = 50,
    #        projection = dict(
    #            type = 'conic conformal',
    #            rotation_lon = num.mean(lon)
    #        ),
    #        lonaxis = dict(
    #            showgrid = True,
    #            gridwidth = 0.5,
    #            range= [min(lon),max(lon)],
    #            dtick = 5
    #        ),
    #        lataxis = dict (
    #            showgrid = True,
    #            gridwidth = 0.5,
    #            range= [min(lat)


    fig.add_trace(go.Scattermapbox(
                    lon = lon,
                    lat = lat,
                    text =names,
                    # mode = 'markers',
                    marker = dict(
                             size = mag,
                             opacity = 0.8,
                             symbol = 'circle', # size and color only availabel for circle (Default)
                             color = col),
                    ))

    fig.update_layout(
        # geo = dict(
        #         lonaxis = dict(
        #             showgrid = True,
        #             gridwidth = 0.5,
        #             range= [min(lon),max(lon)],
        #             dtick = 5),
        #         lataxis = dict(
        #             showgrid = True,
        #             gridwidth = 0.5,
        #             range= [min(lat),max(lat)],
        #             dtick = 5),
        #         showland = True,
        #         landcolor = "rgb(212, 212, 212)",
        #         showcountries = True,
        #         resolution = 50),
        mapbox= dict(
                zoom = 10,
                style = "open-street-map",
                center = dict(lon=num.mean(lon),
                              lat=num.mean(lat)))
            )

    fig.show()

